//
//  TabBarController.m
//  JOTUN
//
//  Created by volive solutions on 2/7/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "TabBarController.h"
#import "PrefixHeader.pch"
@interface TabBarController (){
    
    AppDelegate * tabBarAppDelegate;
    NSObject*objectUserDefaults;
    
}

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    tabBarAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
   // Home*home=[[Home alloc]init];
   
    
 
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];

   
    
    if ([tabBarAppDelegate.selectTabItem isEqualToString:@"0"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setSelectedIndex:0];
           // [[sharedclass sharedInstance]alertforMessage:home];
            
        });
        
    
        
            } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"1"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   [self setSelectedIndex:1];
                   // [[sharedclass sharedInstance]alertforMessage:self];
                    
                });
                
        
        
        
    } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"2"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setSelectedIndex:2];
            //[[sharedclass sharedInstance]alertforMessage:home];
            
        });
        
        
        
    }else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"3"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
                [self setSelectedIndex:3];
                    
            
            //[[sharedclass sharedInstance]alertforMessage:home];
            
        });
 
    }
    else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"4"]) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self setSelectedIndex:4];
            //[[sharedclass sharedInstance]alertforMessage:home];
            
        });
        
        
        
    }
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
