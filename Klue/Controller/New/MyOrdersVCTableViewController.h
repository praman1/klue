//
//  MyOrdersVCTableViewController.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyOrdersVCTableViewController : UITableViewController
@property (weak, nonatomic) IBOutlet UITableView *ordersTableView;
@property (nonatomic, retain) NSString* comForm;
@end
