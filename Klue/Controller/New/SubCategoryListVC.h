//
//  SubCategoryListVC.h
//  Klue
//
//  Created by volivesolutions on 27/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SubCategoryListVC : UIViewController
@property NSString *catIdString;
@property NSString *catNameString;
@property NSString*checkStr;
@end
