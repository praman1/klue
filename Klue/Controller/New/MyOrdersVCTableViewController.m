//
//  MyOrdersVCTableViewController.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "MyOrdersVCTableViewController.h"
#import "PrefixHeader.pch"
#import "HomeTableVC.h"
@interface MyOrdersVCTableViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    
    
    TableViewCell *orderCell;
    NSString *languageString;
    NSString *currencyString;
    
    NSMutableArray *dataCountArray;
    
    NSMutableArray *quantityArray;
    NSMutableArray *subTotalArray;
    NSMutableArray *placedDateArray;
    NSMutableArray *orderIdArray;
    NSMutableArray *delliveryDateArray;
    NSMutableArray *currencyTypeArray;
    NSMutableArray *orderStatusArray;
    
    UIRefreshControl *refreshControl;
}

- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;


@end

@implementation MyOrdersVCTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    //refreshControl.backgroundColor = [UIColor blackColor];
    refreshControl.tintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    [_ordersTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    //self.tabBarController.tabBar.hidden = FALSE;
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
   // self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
//

}

-(void)refreshTable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [refreshControl endRefreshing];
    });
    
    [self myOrdersServicecall];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self myOrdersServicecall];
}

#pragma  mark GetWhishlistServicecall
-(void)myOrdersServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/orders
    
 
    NSMutableDictionary * myOrdersPostDictionary = [[NSMutableDictionary alloc]init];
    
    [myOrdersPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [myOrdersPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [myOrdersPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    //[myOrdersPostDictionary setObject:@"12" forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"orders" withPostDict:myOrdersPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"My Orders Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];

                    dataCountArray = [NSMutableArray new];
                    subTotalArray = [NSMutableArray new];
                    placedDateArray = [NSMutableArray new];
                    orderIdArray = [NSMutableArray new];
                    quantityArray = [NSMutableArray new];
                    delliveryDateArray = [NSMutableArray new];
                    currencyTypeArray = [NSMutableArray new];
                    orderStatusArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
                    if (dataCountArray.count > 0) {
                        for (int i =0; dataCountArray.count > i; i++) {
//                            "message": "Order will be delivered by",
//                            "data": [
//                                     {
//                                         "order_id": "5169602",
//                                         "prod_qty": "20",
//                                         "sub_total": "7979",
//                                         "order_date": "2018-05-24 00:37:40",
//                                         "estimate_date": "2018-05-28 00:00:00"
//                                     }
//                                     ]
  
                            [subTotalArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"sub_total"]]];
                            [currencyTypeArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"currency_type"]]];
                            [placedDateArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_date"]]];
                            [orderIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_id"]]];
                            [delliveryDateArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"estimate_date"]]];
                            [quantityArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"no_of_items"]]];
                            [orderStatusArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_status"]]];
                            
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_ordersTableView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return dataCountArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    orderCell = [_ordersTableView dequeueReusableCellWithIdentifier:@"cell"];
    if(orderCell == nil)
    {
        
        orderCell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
    [orderCell.orderView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [orderCell.orderView.layer setShadowOffset:CGSizeMake(0, 0)];
    [orderCell.orderView.layer setShadowRadius:3.0];
    [orderCell.orderView.layer setShadowOpacity:0.3];
    orderCell.orderView.layer.masksToBounds = NO;
 
    orderCell.NoOfItemsInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"NO.OF ITEMS"];
    orderCell.totalInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"TOTAL"];
    orderCell.noOfItemsValueLabel.text = [NSString stringWithFormat:@"%@",[quantityArray objectAtIndex:indexPath.row]];
    
   // if ([currencyString isEqualToString:@"SAR"]) {
        
         orderCell.totalValueLabel.text = [NSString stringWithFormat:@"%@ %@",[subTotalArray objectAtIndex:indexPath.row],[currencyTypeArray objectAtIndex:indexPath.row]];
    //}
    
//    else if ([currencyString isEqualToString:@"KWD"]) {
//
//         orderCell.totalValueLabel.text = [NSString stringWithFormat:@"%@ KWD",[subTotalArray objectAtIndex:indexPath.row]];
//    }
//    else if ([currencyString isEqualToString:@"AED"]) {
//
//         orderCell.totalValueLabel.text = [NSString stringWithFormat:@"%@ AED",[subTotalArray objectAtIndex:indexPath.row]];
//    }
//    else if ([currencyString isEqualToString:@"USD"]) {
//
//         orderCell.totalValueLabel.text = [NSString stringWithFormat:@"%@ USD",[subTotalArray objectAtIndex:indexPath.row]];
//    }
   
    orderCell.placedOnLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On"],[placedDateArray objectAtIndex:indexPath.row]];
    orderCell.orderIdLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order ID"],[orderIdArray objectAtIndex:indexPath.row]];
    orderCell.dateLabel.text = [NSString stringWithFormat:@"%@",[delliveryDateArray objectAtIndex:indexPath.row]];
    orderCell.orderStatusInfoLabel.text = [NSString stringWithFormat:@"%@:",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order wil be deliver by"]];
    orderCell.statusLabel.text = [NSString stringWithFormat:@"%@",[orderStatusArray objectAtIndex:indexPath.row]];

    return orderCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    OrderDetailVC *details = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailVC"];
    details.orderIdString = [orderIdArray objectAtIndex:indexPath.row];
    details.placedDateString = [placedDateArray objectAtIndex:indexPath.row];
    details.estimatedDateString = [delliveryDateArray objectAtIndex:indexPath.row];
    details.currencyType = [currencyTypeArray objectAtIndex:indexPath.row];
    details.statusString = [orderStatusArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:details animated:TRUE];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 175;
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewCell *cell = (TableViewCell*)[_ordersTableView cellForRowAtIndexPath:indexPath];
    cell.orderView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.orderView.layer.borderWidth = 2;
}

-(void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = (TableViewCell*)[_ordersTableView cellForRowAtIndexPath:indexPath];
    cell.orderView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.orderView.layer.borderWidth = 1;
}
- (IBAction)btnBack:(id)sender {
   
//    if ([_comForm isEqualToString:@"Menu"]) {
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:self];
//    }else {
//
//         [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
@end
