//
//  Menu2TableViewCell.h
//  Klue
//
//  Created by vishal bhatt on 19/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Menu2TableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblmName;
@property (strong, nonatomic) IBOutlet UIView *seprate;

@end
