//
//  FilterVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterVC : UIViewController
@property (weak, nonatomic) IBOutlet UISlider *priceChangeSlider;
- (IBAction)sliderChange:(id)sender;
@property NSString *subcatIdStr;

@end
