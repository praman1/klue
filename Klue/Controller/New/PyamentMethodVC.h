//
//  PyamentMethodVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <PayFortSDK/PayFortSDK.h>
#import <CommonCrypto/CommonCrypto.h>

@interface PyamentMethodVC : UIViewController<PayFortDelegate>
@property NSString *shippingMethodIdStr;
@property NSString *shippingAddressIdStr;
@property NSString *deliveryDateString,*deliveryTimeString;
@property NSString *orderTotal;
@property NSData *imageData;
@property NSString *messageString,*noteString,*cityNameStr;



@end
