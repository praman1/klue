//
//  FilterVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "FilterVC.h"
#import "PrefixHeader.pch"
#import "BrandsVC.h"

@interface FilterVC ()
{
    NSString *languageString;
    NSMutableArray *dataArray;
}
- (IBAction)btnBack:(id)sender;
- (IBAction)btnApply:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *applyBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *priceHintLabel;
@property (weak, nonatomic) IBOutlet UILabel *minimumPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *maximumPriceLabel;
- (IBAction)closeBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn_Outlet;


@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[sharedclass sharedInstance]shadowEffects:_applyBtn_Outlet];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Filters"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)brandBtn:(id)sender {
    
    BrandsVC * brand = [self.storyboard instantiateViewControllerWithIdentifier:@"BrandsVC"];
    
    //[self.navigationController pushViewController:brand animated:YES];
    
    [self presentViewController:brand animated:YES completion:nil];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark FiltersServicecall
-(void)filtersServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/filters
    //""API-KEY:985869547,lang:en,sub_cat_id,brand_id,low_price,high_price,user_id""
    
    
    
    NSMutableDictionary * filterPostDictionary = [[NSMutableDictionary alloc]init];
    
    [filterPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [filterPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [filterPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [filterPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"subCatId"]  forKey:@"sub_cat_id"];
    [filterPostDictionary setObject:_minimumPriceLabel.text forKey:@"low_price"];
    [filterPostDictionary setObject:_maximumPriceLabel.text forKey:@"high_price"];
    [filterPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"brandId"] forKey:@"brand_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"filters" withPostDict:filterPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Filters Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    NSDictionary *dataDict = dict;
                    
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"filterData" object:nil userInfo:dataDict];
                    
                    [self dismissViewControllerAnimated:TRUE completion:nil];

                    // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnApply:(id)sender {
    
    [self filtersServicecall];
}

- (IBAction)sliderChange:(id)sender {
    
    NSString * priceStr = [NSString stringWithFormat:@"%f",_priceChangeSlider.value];
    _maximumPriceLabel.text = [NSString stringWithFormat:@"%ld",(long)priceStr.integerValue];
    
    
}
- (IBAction)closeBtn_Action:(id)sender {
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
