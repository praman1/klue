//
//  TableViewCell.h
//  Klue
//
//  Created by volive solutions on 09/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "customCollectionView.h"

@interface TableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UILabel *shippingMethodLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingPriceLabel;

@property (weak, nonatomic) IBOutlet UIView *orderView;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;

@property (weak, nonatomic) IBOutlet UIButton *nextBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *placedOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *statusLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *klueLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *deleteBtn;
@property (weak, nonatomic) IBOutlet UILabel *quantityInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *notificationLabel;
@property (weak, nonatomic) IBOutlet UILabel *maiQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UIImageView *catImageView;
@property (weak, nonatomic) IBOutlet UIButton *quantityPlusBtn_Outlet;

@property (weak, nonatomic) IBOutlet UILabel *NoOfItemsInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *noOfItemsValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;

@property (weak, nonatomic) IBOutlet UIImageView *itemImageView;
@property (weak, nonatomic) IBOutlet UILabel *itemNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemBrandLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemOrderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *itemDeliveryDateLabel;
@property (weak, nonatomic) IBOutlet UIView *itemView;

@property (weak, nonatomic) IBOutlet UIButton *quantityMinusBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *catNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UIView *addressView;
@property (weak, nonatomic) IBOutlet UIButton *deleteAddressBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *editAddressBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *radioBtn_Outlet;
//@property (weak, nonatomic) IBOutlet UICollectionView *giftsCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *giftsTypeNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewMoreBtn_Outlet;
@property (strong, nonatomic) NSIndexPath *parentIndexpath;
@property (weak, nonatomic) IBOutlet customCollectionView *giftsCollectionView;


// brandcell

@property (weak, nonatomic) IBOutlet UILabel *brandNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *brandImg;



@end
