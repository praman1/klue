//
//  sharedclass.m
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import "sharedclass.h"
#import "HeaderAppConstant.h"
@implementation sharedclass
{
    // SCLAlertView *alert ;
}

static sharedclass * sharedClassObj = nil;

+(sharedclass *)sharedInstance {
    
    if (sharedClassObj == nil) {
        sharedClassObj = [[super allocWithZone:NULL]init];
    }
    
    return sharedClassObj;
}

+(id)allocWithZone:(struct _NSZone *)zone
{
    return sharedClassObj;
}

- (id)init
{
    self = [super init];
    
    if (self != nil)
    {
        
        
    }
    return self;
}

#pragma mark Reachability Checking
-(BOOL)isNetworkAvalible {
    
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [Reachability reachabilityWithHostName:@"http://www.google.co.in/"];
    NetworkStatus remoteHostStatus = [reachability currentReachabilityStatus];
    
    if (remoteHostStatus == NotReachable) {
        return NO;
    }
    else {
        return YES;
    }
}
#pragma mark - textfieldAsLine
-(void)textfieldAsLine:(UITextField *)myTextfield lineColor:(UIColor *)lineColor myView:(UIView *)view{
    CALayer *border = [CALayer layer];
    CGFloat borderWidth = 1.0;
    border.borderColor = lineColor.CGColor;
    border.frame =CGRectMake(0, myTextfield.frame.size.height - borderWidth, view.frame.size.width, myTextfield.frame.size.height);
    border.borderWidth = borderWidth;
    [myTextfield.layer addSublayer:border];
    myTextfield.layer.masksToBounds = YES;
    
}

-(void)shadowEffects:(UIView *)myView{
    
    [myView.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [myView.layer setShadowOffset:CGSizeMake(0,0)];
    [myView.layer setShadowRadius:3.0];
    [myView.layer setShadowOpacity:0.3];
    myView.layer.masksToBounds = NO;
}

//-(void)alertforMessage:(UIViewController *)ve :(NSString *)imgname :(NSString *)text :(NSString *)buttonTitle{
//    alert = [[SCLAlertView alloc] init];
//
//    UIColor *color = [UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
//
//    [alert showCustom:ve image:[UIImage imageNamed:imgname] color:color title:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Message"] subTitle:text closeButtonTitle:buttonTitle duration:0.0f];
//}
//-(void)alertforMessage:(UIViewController *)ve{
//    //Receiving information that SCLAlertView is dismissed
//    [alert alertIsDismissed:^{
//        NSLog(@"SCLAlertView dismissed!");
//    }];
//}

-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(KlueGet)completionHandler
{

    NSString * urlString;

    urlString = [self modifyURLString:parameterString valuesDictionary:postDictionary];

    NSLog(@"%@ URL string :%@",parameterString,urlString);

    NSMutableURLRequest * urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:urlString]];

    NSURLSessionConfiguration * urlSessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession * urlSession = [NSURLSession sessionWithConfiguration:urlSessionConfiguration];

    NSURLSessionDataTask * dataTask = [urlSession dataTaskWithRequest:urlRequest completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        if (data != nil) {

            NSData * _Nullable  dataFromJson = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];

            if (dataFromJson != nil) {

                completionHandler ( dataFromJson );

            }else
            {
                [self showprogressfor:@"Server problem,please try after sometime"];
                [SVProgressHUD dismiss];

            }

        }
        else
        {
            [self showprogressfor:@"Server problem,please try after sometime"];
            [SVProgressHUD dismiss];

        }



    }];

    [dataTask resume];

}

-(NSString *)modifyURLString: (NSString *) urlString
            valuesDictionary: (NSDictionary *)valuesFromUser

{
    
    NSString * finalURLString;
    
    if ([urlString isEqualToString:@"getprofile?"]) {

        //http://volivesolutions.com/Klue/services/getprofile?API-KEY=985869547&lang=en&user_id=35
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&user_id=%@",Base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"user_id"]];
       

    }
    else if ([urlString isEqualToString:@"banner_cat_products?"]) {
    //http://volivesolutions.com/Klue/services/banner_cat_products?API-KEY=985869547&lang=en&user_id=3

        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&user_id=%@",Base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"user_id"]];

    }
    else if ([urlString isEqualToString:@"product_info?"]) {
        //http://volivesolutions.com/Klue/services/product_info?API-KEY=985869547&lang=en&prod_id=10&user_id=3
        
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&prod_id=%@&user_id=%@",Base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"prod_id"],[valuesFromUser objectForKey:@"user_id"]];
        
    }
    else if ([urlString isEqualToString:@"sub_categories?"]) {
        //http://volivesolutions.com/Klue/services/sub_categories?API-KEY=985869547&lang=en&cat_id=6
        
        finalURLString = [NSString stringWithFormat:@"%@%@API-KEY=%@&lang=%@&cat_id=%@",Base_URL,urlString,[valuesFromUser objectForKey:@"API-KEY"],[valuesFromUser objectForKey:@"lang"],[valuesFromUser objectForKey:@"cat_id"]];
        
    }

    
    finalURLString = [finalURLString stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
    
    return finalURLString;
}


#pragma mark - urlPerameterforPost
-(void)urlPerameterforPost:(NSString* )paramStr withPostDict:(NSDictionary* )postDict andReturnwith:(KluePost)completionHandler {

    NSString *UrlStr;

    if ([paramStr isEqualToString:@"login"]) {

        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];

    }
    else if ([paramStr isEqualToString:@"register"]) {

        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];

    }
    else if ([paramStr isEqualToString:@"change_password"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"products_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"addcart"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"getcart"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"delete_cart"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"add_wishlist"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"getwishlist"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"delete_favourite"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"shipping_addr"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"get_shipping_addr"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"del_shipping_addr"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"hot_products_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"orders"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"order_details"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"gift_products"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"addcart_dec"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"countries_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"cities_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"shipping_methods"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"booking_orders"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"forgot_password"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"filters"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }else if ([paramStr isEqualToString:@"brands_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"sort_products"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"help_center"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"gift_addcart"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    else if ([paramStr isEqualToString:@"cod_list"]) {
        
        UrlStr=[NSString stringWithFormat:@"%@%@",Base_URL,paramStr];
        
    }
    

    NSMutableURLRequest*urlRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:UrlStr]];

    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";

    [urlRequest setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [urlRequest setHTTPShouldHandleCookies:NO];
    [urlRequest setTimeoutInterval:30];
    [urlRequest setHTTPMethod:@"POST"];

    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [urlRequest addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [urlRequest setValue:contentType forHTTPHeaderField: @"Content-Type"];

    // post body
    NSMutableData *body = [NSMutableData data];

    // add params (all params are strings)
    for (NSString *param in postDict) {

        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [postDict objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [urlRequest setHTTPBody:body];

    }

    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [urlRequest setValue:postLength forHTTPHeaderField:@"Content-Length"];

    NSURLSession *session=[NSURLSession sharedSession];

    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData*  _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {

        NSError *err;

        if (!error) {
            NSDictionary *serverDatadict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:&err];
                if (data != nil) {

                if ([paramStr isEqualToString:@"login"]) {

                    completionHandler (serverDatadict, nil, true, @"Success");
                }

                else if ([paramStr isEqualToString:@"register"]) {

                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"change_password"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"products_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"addcart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"getcart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"delete_cart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"add_wishlist"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"getwishlist"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"delete_favourite"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"shipping_addr"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"get_shipping_addr"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"del_shipping_addr"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"hot_products_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"orders"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"order_details"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"gift_products"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"addcart_dec"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"countries_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"cities_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"shipping_methods"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"booking_orders"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"forgot_password"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"filters"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"brands_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"sort_products"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"help_center"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"gift_addcart"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }
                else if ([paramStr isEqualToString:@"cod_list"]) {
                    
                    completionHandler (serverDatadict, nil, true, @"Success");
                }

                else {

                    completionHandler (nil, nil, false, @"Failed");
                }

            }

            else {

                completionHandler (nil, nil, false, @"Failed");
            }

        }

        else {


            NSLog(@"Error is %@", error.debugDescription);

            completionHandler (nil, nil, false, @"Unable to fetch.");

        }

    }];

    [postDataTask resume];

}

#pragma mark - showAlertWithTitle
-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

{
//    if (message == nil || [message isKindOfClass:[NSNull class]]) {
//        
//        title =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sorry!"] ;
//        message =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Server problem,please try after some time"] ;
//        
//    }
    UIAlertController * suggestionsAlert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction * okAction = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
       //completionBlock ();
        
    }];
    
    [suggestionsAlert addAction:okAction];
    
    [viewController presentViewController:suggestionsAlert animated:YES completion:nil];
}

-(void)showprogressfor:(NSString *)str {
    
    [SVProgressHUD showWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    
}

-(void)showprogressforSucsess:(NSString *)str {
    
    [SVProgressHUD setSuccessImage:[UIImage imageNamed:@"SuccessTick3.png"]];
    [SVProgressHUD showSuccessWithStatus:str];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeGradient];
    [SVProgressHUD dismissWithDelay:1.0];
    
}

#pragma mark - GET
-(void)GETList:(NSString *)post completion :(void (^)(NSDictionary *, NSError *))completion{
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:[post stringByReplacingOccurrencesOfString:@" " withString:@""]]];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    
    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        NSLog(@" res :%@",response);
        
        if ([data length] == 0) {
            
            
        }else{
            //NSLog(@"-data-%@",data);
            NSString *myString = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            _json = [NSJSONSerialization JSONObjectWithData:data
                                                    options:kNilOptions
                                                      error:nil];
            //NSLog(@"json : %@",_json);
            NSLog(@"myString : %@",myString);
            
        }
        completion(_json, error);
    }]resume];
    
}

//#pragma mark - POST
//-(void)postMethodForServicesRequest:(NSString *)serviceType :(NSString *)post completion:(void (^)(NSDictionary *, NSError *))completion{
//    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
//    NSString *postLength = [NSString stringWithFormat:@"%d", (int)[postData length]];
//    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
//    [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",base_URL,serviceType]]];
//    NSLog(@"---%@",[NSString stringWithFormat:@"%@%@",base_URL, serviceType]);
//    [request setHTTPMethod:@"POST"];
//    //[request setTimeoutInterval: 100];
//    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
//    [request setHTTPBody:postData];
//    
//    // code here
//    NSURLSession *session = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
//    [[session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        _json = [NSJSONSerialization JSONObjectWithData:data
//                                                options:kNilOptions
//                                                  error:nil];
//        NSLog(@"_json :%@",_json);
//        
//        completion(_json, error);
//    }]resume];
//}

#pragma mark - languageSelectedStringForKey
-(NSString*)languageSelectedStringForKey:(NSString*) key
{
    
    NSBundle *path;
    
    NSString *  selectedLanguage = [[NSUserDefaults standardUserDefaults] objectForKey:@"language"];
    
    if([selectedLanguage isEqualToString:ENGLSIH_LANGUAGE])
        
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
    
    else if ([selectedLanguage isEqualToString:ARABIC_LANGUAGE])
    {
        
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"ar" ofType:@"lproj"]];
    }
    else{
        path = [NSBundle bundleWithPath:[[NSBundle mainBundle] pathForResource:@"en" ofType:@"lproj"]];
        
    }
    
    //NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    NSString *str = [NSString stringWithFormat:NSLocalizedStringFromTableInBundle(key, @"LocalizedStrings", path, nil)];
    // NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    return str;

}

-(void)showalrtfor:(NSString* )str inViewCtrl:(UIViewController* )vc
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] message:str                                                                               preferredStyle:UIAlertControllerStyleAlert];
    
    [vc presentViewController:alertController animated:YES completion:nil];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        
        [alertController dismissViewControllerAnimated:YES completion:^{
            // do something ?
        }];
        
    });
}
@end
