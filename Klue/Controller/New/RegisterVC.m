//
//  RegisterVC.m
//  Klue
//
//  Created by vishal bhatt on 15/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "RegisterVC.h"
#import "PrefixHeader.pch"


@interface RegisterVC ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>
{
    NSString *tokenString;
    NSString *languageString;
    NSString *  selected_Type;
    NSMutableArray *countryCodesArray;
    NSMutableArray *countryNamesArray;
    NSMutableArray *countryDataArrayCount;
    UIPickerView *countryPickerView;
    UIToolbar *toolbar1;
}
- (IBAction)btnBack:(id)sender;
- (IBAction)btnSignin:(id)sender;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *nameTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *emailTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *passwordTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *countryCodeTF;



@property (weak, nonatomic) IBOutlet UIScrollView *signUpScrollView;
@property (weak, nonatomic) IBOutlet UIButton *createAccountBtn_Outlet;
- (IBAction)createAccountBTN:(id)sender;


@end

@implementation RegisterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self countryCodesServicecall];
    [self pickerView];
    _countryCodeTF.delegate = self;
   // countryPickerView.delegate = self;
    //countryPickerView.dataSource = self;
    countryPickerView = [[UIPickerView alloc] init];
    [[sharedclass sharedInstance]shadowEffects:_createAccountBtn_Outlet];
    
    // Do any additional setup after loading the view.
}

#pragma mark Textfield Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTF) {

        [_signUpScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _emailTF) {

        [_signUpScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _passwordTF) {

        [_signUpScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _mobileNumberTF) {
        
        [_signUpScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{

    [_nameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];

    
    [_signUpScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTF) {
        
        [_emailTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_mobileNumberTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _mobileNumberTF) {
        
        [_mobileNumberTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}

#pragma mark signUpServiceCall

-(void)signUpServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
 
   //http://volivesolutions.com/Klue/Api/register
    // NSString *url = [NSString stringWithFormat:@"%@/registration",base_URL];
 
    NSMutableDictionary * registerPostDictionary = [[NSMutableDictionary alloc]init];
    
    
   // fname,email,mobile,password,device_type,lang,device_token
    
    [registerPostDictionary setObject:_nameTF.text forKey:@"fname"];
    [registerPostDictionary setObject:_emailTF.text forKey:@"email"];
    [registerPostDictionary setObject:_passwordTF.text forKey:@"password"];
    [registerPostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
   // [registerPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"device_token"];
    [registerPostDictionary setObject:@"123" forKey:@"device_token"];
    [registerPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [registerPostDictionary setObject:Device_Type forKey:@"device_type"];
    [registerPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];

    
    [[sharedclass sharedInstance]urlPerameterforPost:@"register" withPostDict:registerPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Data From Server %@", dict);
            [SVProgressHUD dismiss];
            
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                if ([status isEqualToString:@"1"])
                {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        
                    });
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        if ([_checkString isEqualToString:@"fromLogin"]) {
                            [self.navigationController popViewControllerAnimated:TRUE];
                        }else{
                            LoginVC * login=[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                            [self.navigationController pushViewController:login animated:YES];
                        }

                    }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
                    
                }
            });
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[dict objectForKey:@"message"] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark CountryCodesServicecall
-(void)countryCodesServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/countries_list
    //"API-KEY:985869547,lang:en ,sub_cat_id,low_price,high_price,new_arrival"
    
    
    
    NSMutableDictionary * countryCodesPostDictionary = [[NSMutableDictionary alloc]init];
    
    [countryCodesPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [countryCodesPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
   
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"countries_list" withPostDict:countryCodesPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Country Codes Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];
                    countryCodesArray = [NSMutableArray new];
                    countryNamesArray = [NSMutableArray new];
                    countryDataArrayCount = [NSMutableArray new];
                    countryDataArrayCount = [dict objectForKey:@"data"];
                    if (countryDataArrayCount.count > 0) {
                        for (int i = 0; countryDataArrayCount.count > i; i++) {
                            [countryCodesArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"numcode"]];
                            [countryNamesArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"country_name"]];
                        }
                    }
                    [self pickerView];
                    countryPickerView.delegate = self;
                    countryPickerView.dataSource = self;
                    
                }
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(void)pickerView
{
    _countryCodeTF.inputView = countryPickerView;
    
    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor whiteColor];
    
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [_countryCodeTF setInputAccessoryView:toolbar1];
    
}
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryCodesArray count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryCodesArray[row];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == countryPickerView) {
        return countryCodesArray[row];
    }
    
    return nil;
    
}

-(void)show
{
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    if(selected_Type.length == 0)
    {
        _countryCodeTF.text = [countryCodesArray objectAtIndex:0];
        
    }  else
    {
        _countryCodeTF.text = selected_Type;
    }
    
    NSLog(@"myTextValue *** %@",_countryCodeTF.text);
    
    
    [_countryCodeTF resignFirstResponder];
    
}
-(void)cancel
{
    [_countryCodeTF resignFirstResponder];
    
}

- (IBAction)btnBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnSignin:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)createAccountBTN:(id)sender {
    [self signUpServiceCall];
}
@end
