//
//  HotPicksListVC.m
//  Klue
//
//  Created by volivesolutions on 10/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "TopPicksListVC.h"
#import "PrefixHeader.pch"

@interface TopPicksListVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    NSString *languageString;
    CollectionViewCell *cell;
    
    NSString *currencyString;
    NSMutableArray *dataCountArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productBrandNameArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    
    NSMutableArray *productRegular_price_sarArray;
    NSMutableArray *productPrice_sarArray;
    
    NSMutableArray *productRegular_price_usdArray;
    NSMutableArray *productPrice_usdArray;
    
    NSMutableArray *productRegular_price_kwdArray;
    NSMutableArray *productPrice_kwdArray;
    
    NSMutableArray *productRegular_price_aedArray;
    NSMutableArray *productPrice_aedArray;
    
    NSMutableArray *isWhistListCheckArray;
    NSString *whishlistString;
     NSObject*objectUserDefaults;
}
@property (weak, nonatomic) IBOutlet UICollectionView *topPicksCollectionView;
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;

@end

@implementation TopPicksListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageRefresh) name:@"refreshMenu1" object:nil];
    
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    [self topPicksProductsServicecall];
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Top Picks"];
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    // Do any additional setup after loading the view.
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    [self topPicksProductsServicecall];
//    self.tabBarController.tabBar.hidden = FALSE;
//}
//
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = TRUE;
//}
-(void)languageRefresh
{
    [self viewDidLoad];
}
#pragma mark HotPicksProductsServicecall
-(void)topPicksProductsServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/hot_products_list
    
    //API-KEY:985869547,lang:en,user_id
    
    NSMutableDictionary * topPicksPostDictionary = [[NSMutableDictionary alloc]init];
    
    [topPicksPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [topPicksPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [topPicksPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"hot_products_list" withPostDict:topPicksPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    productIdArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    productBrandNameArray = [NSMutableArray new];
                    
                    productRegular_price_sarArray = [NSMutableArray new];
                    productPrice_sarArray = [NSMutableArray new];
                    
                    productRegular_price_usdArray = [NSMutableArray new];
                    productPrice_usdArray = [NSMutableArray new];
                    
                    productRegular_price_kwdArray = [NSMutableArray new];
                    productPrice_kwdArray = [NSMutableArray new];
                    
                    productRegular_price_aedArray = [NSMutableArray new];
                    productPrice_aedArray = [NSMutableArray new];
                    
                    isWhistListCheckArray = [NSMutableArray new];
                    dataCountArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
                    
                    if (dataCountArray.count > 0) {
                        for (int i = 0; dataCountArray.count > i; i++) {
                            [productIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
                            [productImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [productBrandNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                            
                            [productPrice_sarArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            [productRegular_price_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            [productRegular_price_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            [productRegular_price_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];
                            [ productRegular_price_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
                            
                            
                            [isWhistListCheckArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"is_wish_list"]];
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_topPicksCollectionView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}



#pragma mark - Table view data source
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _topPicksCollectionView)
    {
        CGFloat width = (CGFloat)(_topPicksCollectionView.frame.size.width/2);
        return CGSizeMake(width-8, 220);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return productNameArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == _topPicksCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        [[sharedclass sharedInstance]shadowEffects:cell.cellView];
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
        cell.brandNameLabel.text = [productBrandNameArray objectAtIndex:indexPath.row];
        
        
        NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                      NSStrikethroughColorAttributeName:[UIColor blackColor]};
        NSAttributedString * attr;
        if ([currencyString isEqualToString:@"SAR"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",[productRegular_price_sarArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
        }
        
        else if ([currencyString isEqualToString:@"KWD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",[productRegular_price_kwdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"AED"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",[productRegular_price_aedArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"USD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",[productRegular_price_usdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
        }
        
        [cell.favouriteBtn addTarget:self action:@selector(addOrRemoveWhishlist:) forControlEvents:UIControlEventTouchUpInside];
        whishlistString = [NSString stringWithFormat:@"%@",[isWhistListCheckArray objectAtIndex:indexPath.row]];
        
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            
        }
        return cell;
    }
    return NULL;
    
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
 
    if(collectionView == _topPicksCollectionView){
        
        ProductScreenVC *productInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductScreenVC"];
        productInfo.productIdString = [NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:productInfo animated:YES];
        
    }
}

-(void)addOrRemoveWhishlist:(id)sender
{
    
    if (objectUserDefaults != nil) {
        CGPoint position = [sender convertPoint:CGPointZero toView:_topPicksCollectionView];
        NSIndexPath *indexPath = [_topPicksCollectionView indexPathForItemAtPoint:position];
        
        CollectionViewCell *cell = (CollectionViewCell*)[_topPicksCollectionView cellForItemAtIndexPath:indexPath];
        whishlistString = [NSString stringWithFormat:@"%@",[isWhistListCheckArray objectAtIndex:indexPath.row]];
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            [self addWhishlistServicecall:sender];
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            [self removeWhishlistServicecall:sender];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
    
    
}

#pragma  mark AddWhishlistServicecall
-(void)addWhishlistServicecall:(id)sender
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/add_wishlist
    //API-KEY:985869547,lang,user_id,prod_id
    CGPoint position = [sender convertPoint:CGPointZero toView:_topPicksCollectionView];
    NSIndexPath *indexPath = [_topPicksCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_wishlist" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];
                    
                    
                    [self topPicksProductsServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


#pragma mark RemoveWhishlistServicecall
-(void)removeWhishlistServicecall:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/delete_favourite
    //API-KEY:985869547,lang,user_id,prod_id
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_topPicksCollectionView];
    NSIndexPath *indexPath = [_topPicksCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * removeWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [removeWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [removeWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:removeWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    // [SVProgressHUD dismiss];
                    
                    [self topPicksProductsServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

- (IBAction)backBtn_Action:(id)sender {
    
//    if ([_comForm isEqualToString:@"Menu"]) {
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:nil];
//
//    }else{
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:nil];
//        self.tabBarController.selectedIndex = 0;
//        // [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
@end
