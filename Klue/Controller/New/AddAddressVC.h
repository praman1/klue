//
//  AddAddressVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HomeTableVC.h"
@interface AddAddressVC : UIViewController
@property (nonatomic, retain) NSString* comForm;
@property NSString *shippingAddressId;
@property NSString *nameString,*emailIdString,*mobileNumberString,*addressString,*houseNoString,*landMarkString,*cityNameString;
@property NSString *addressCheckString;


@end
