//
//  CatProductsListVC.h
//  Klue
//
//  Created by volivesolutions on 12/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CatProductsListVC : UIViewController
@property NSString *subCatIdString;
@property NSString *subCatNameString;
@end
