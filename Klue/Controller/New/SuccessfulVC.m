//
//  SuccessfulVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "SuccessfulVC.h"
#import "HomeTableVC.h"
@interface SuccessfulVC ()
    {
        NSString *languageString;
        NSString *currencyString;
    }

- (IBAction)continueShopping:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *successTickImageview;
@property (weak, nonatomic) IBOutlet UILabel *paymentSuccessInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentTypeInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentTypeValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionDateInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *transactionDateValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueShoppingBtn_Outlet;


@end

@implementation SuccessfulVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    self.navigationItem.hidesBackButton = YES;
    self.navigationController.navigationItem.hidesBackButton = true;
   // self.navigationController.navigationItem.
    //self.navigationController.navigationBar isUserInteractionEnabled = false;
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Payment Successful"];
    [_continueShoppingBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Continue Shopping"] forState:UIControlStateNormal];
    _paymentSuccessInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Payment Successful"];
    _amountInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Amount"];
    _orderIdInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order ID"];
    _paymentTypeInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Payment Type"];
    _transactionDateInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Transaction Date"];
    _paymentInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Your payment has been processed! Details of transactions are included below"];
    
            if ([currencyString isEqualToString:@"SAR"]) {
                _amountValueLabel.text = [NSString stringWithFormat:@"%@",_totalAmountString];
            }
            else if ([currencyString isEqualToString:@"KWD"]) {
                _amountValueLabel.text = [NSString stringWithFormat:@"%@KWD",_totalAmountString];
            }
            else if ([currencyString isEqualToString:@"AED"]) {
                _amountValueLabel.text = [NSString stringWithFormat:@"%@AED",_totalAmountString];
            }
            else if ([currencyString isEqualToString:@"USD"]) {
                _amountValueLabel.text = [NSString stringWithFormat:@"%@USD",_totalAmountString];
            }
    
    _orderIdValueLabel.text = [NSString stringWithFormat:@"%@",_orderIdString];
    _transactionDateValueLabel.text = [NSString stringWithFormat:@"%@",_transactionDateString];
    _paymentTypeValueLabel.text = [NSString stringWithFormat:@"%@",_paymentTypeString];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //self.navigationController.navigationBar.hidden = NO;
dispatch_async(dispatch_get_main_queue(), ^{
    [UIView animateWithDuration:0 animations:^{
        
        _successTickImageview.transform = CGAffineTransformMakeScale(0.01, 0.01);
        
    }completion:^(BOOL finished){
        [UIView animateWithDuration:0.4 animations:^{
            
            _successTickImageview.transform = CGAffineTransformIdentity;
            
        }completion:^(BOOL finished){
            
        }];
        
    }];
    
});
}

-(void)viewWillDisappear:(BOOL)animated
    {
       // self.navigationController.navigationBar.hidden = YES;
    }


- (IBAction)continueShopping:(id)sender {
//    HomeTableVC *home = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeTableVC"];
//    [self showViewController:home sender:nil];
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                             bundle: nil];
    SWRevealViewController *home = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Side"];
    // self.window.rootViewController = home;
    [self presentViewController:home animated:YES completion:nil];

}

@end
