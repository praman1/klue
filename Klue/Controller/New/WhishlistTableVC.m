//
//  WhishlistTableVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "WhishlistTableVC.h"
#import "PrefixHeader.pch"
@interface WhishlistTableVC ()<UITableViewDelegate,UITableViewDataSource>
{
    TableViewCell *cell;
    NSString *currencyString;
    
    NSMutableArray *dataCountArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    NSMutableArray *brandNameArray;
    
    NSMutableArray *productPrice_sarArray;
    NSMutableArray *productPrice_usdArray;
    NSMutableArray *productPrice_kwdArray;
    NSMutableArray *productPrice_aedArray;
    
    NSString *languageString;
    NSObject*objectUserDefaults;
    
    UIRefreshControl *refreshControl;
    
    
    
}
@property (strong, nonatomic) IBOutlet UIView *card1;
@property (weak, nonatomic) IBOutlet UITableView *whishListTableView;

- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;

@end

@implementation WhishlistTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageRefresh) name:@"refreshMenu1" object:nil];
    
    refreshControl = [[UIRefreshControl alloc]init];
    refreshControl.tintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    [_whishListTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    
    if (objectUserDefaults != nil) {
        [self customDesign];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

- (void)refreshTable {
    dispatch_async(dispatch_get_main_queue(), ^{
        [refreshControl endRefreshing];
    });
    
    [self getWhishlistServicecall];
    
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //[self viewDidLoad];
   // [self getWhishlistServicecall];
 
}
-(void)languageRefresh
{
    [self viewDidLoad];
}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = TRUE;
//}
-(void)customDesign
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Whishlist"];
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Categories"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Top Picks"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Whishlist"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:4] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Account"]];
    [self getWhishlistServicecall];
}



#pragma  mark GetWhishlistServicecall
-(void)getWhishlistServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/getwishlist

    //API-KEY:985869547,lang,user_id

//    CGPoint position = [sender convertPoint:CGPointZero toView:_catCollectionView];
//    NSIndexPath *indexPath = [_catCollectionView indexPathForItemAtPoint:position];
//
    NSMutableDictionary * getWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [getWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [getWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [getWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"getwishlist" withPostDict:getWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Get Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    dataCountArray = [NSMutableArray new];
                    productIdArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    brandNameArray = [NSMutableArray new];
                    productPrice_sarArray = [NSMutableArray new];
                    productPrice_usdArray = [NSMutableArray new];
                    productPrice_kwdArray = [NSMutableArray new];
                    productPrice_aedArray = [NSMutableArray new];
                    
//                    "prod_id"
//                    "brand_name
//                    "pname"
//                    "prod_image"
//                    "price_sar"
                    dataCountArray = [dict objectForKey:@"data"];
                    if (dataCountArray.count > 0) {
                        for (int i =0; dataCountArray.count > i; i++) {
                            [productIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
                            [productImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [brandNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                            
                            [productPrice_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];

                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_whishListTableView reloadData];
                        [refreshControl endRefreshing];
                    });
                   
                    
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}




#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return productNameArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.contentView];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]] completed:nil];
    cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
    cell.brandLabel.text = [brandNameArray objectAtIndex:indexPath.row];
    
    if ([currencyString isEqualToString:@"SAR"]) {
        
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
    }
    
    else if ([currencyString isEqualToString:@"KWD"]) {
        
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
    }
    else if ([currencyString isEqualToString:@"AED"]) {
        
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
    }
    else if ([currencyString isEqualToString:@"USD"]) {
        
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
    }
    
    [cell.deleteBtn addTarget:self action:@selector(deleteWhishlist:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

-(void)deleteWhishlist:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    CGPoint position = [sender convertPoint:CGPointZero toView:_whishListTableView];
    NSIndexPath *indexPath = [_whishListTableView indexPathForRowAtPoint:position];
   // API-KEY:985869547,lang,user_id,prod_id
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/delete_favourite


    NSMutableDictionary * deleteWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [deleteWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [deleteWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [deleteWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [deleteWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:deleteWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Delete Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    [productIdArray removeObjectAtIndex:indexPath.row];
                    [productNameArray removeObjectAtIndex:indexPath.row];
                    [productImageArray removeObjectAtIndex:indexPath.row];
                    [brandNameArray removeObjectAtIndex:indexPath.row];
                    if ([currencyString isEqualToString:@"SAR"]) {
                        
                        [productPrice_sarArray removeObjectAtIndex:indexPath.row];
                    }
                    
                    else if ([currencyString isEqualToString:@"KWD"]) {
                        
                        [productPrice_kwdArray removeObjectAtIndex:indexPath.row];
                    }
                    else if ([currencyString isEqualToString:@"AED"]) {
                        
                         [productPrice_aedArray removeObjectAtIndex:indexPath.row];
                    }
                    else if ([currencyString isEqualToString:@"USD"]) {
                        
                        [productPrice_usdArray removeObjectAtIndex:indexPath.row];
                    }
                    
                    
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_whishListTableView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];

    
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBack:(id)sender {
//    if ([_comForm isEqualToString:@"Menu"]) {
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:nil];
//
//    }else{
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:nil];
//        self.tabBarController.selectedIndex = 0;
//       // [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
@end
