//
//  ProductScreenVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ProductScreenVC.h"

@interface ProductScreenVC ()
{
    NSString *productIdStr;
    NSString *isWhishlistString;
    NSString *languageString;
    NSString *productPriceString;
    NSString *currencyString;
    int value;
    NSObject*objectUserDefaults;
}
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UIButton *favouriteBtn_Outlet;
- (IBAction)favouriteBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *detailsInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *addToCartBtn_Outlet;
- (IBAction)addToCartBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
- (IBAction)plusBtn_Action:(id)sender;
- (IBAction)minusBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityInfoLabel;
@property (weak, nonatomic) IBOutlet UIButton *minusBtn_Outlet;

@end

@implementation ProductScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    value = 1;
    _quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    [self customDesign];
    
    [[sharedclass sharedInstance]shadowEffects:_addToCartBtn_Outlet];
    
    if ([_quantityLabel.text isEqualToString:@"1"]) {
        _minusBtn_Outlet.userInteractionEnabled = NO;
    }


    
    // Do any additional setup after loading the view.
}
-(void)customDesign
{
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Product Info"];
    _detailsInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Details"];
    _quantityInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Details"];
    [self productsInfoServicecall];
}

#pragma mark ProductsInfoServicecall
-(void)productsInfoServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    // http://volivesolutions.com/Klue/services/product_info?API-KEY=985869547&lang=en&prod_id=10&user_id=3
    NSMutableDictionary * productInfoPostDictionary = [[NSMutableDictionary alloc]init];
    
    [productInfoPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [productInfoPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [productInfoPostDictionary setObject:[NSString stringWithFormat:@"%@",_productIdString] forKey:@"prod_id"];
    [productInfoPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"product_info?" withPostDict:productInfoPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Product Info Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                NSString *imageString = [[dataDictionary objectForKey:@"data"]objectForKey:@"prod_image"];
                [_productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,imageString]] completed:nil];
                _productNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"pname"];
                _descriptionLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"description"];
                _brandNameLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"brand_name"];
                
               // price_usd
//                regular_price_usd
//                price_kwd
//                regular_price_kwd
//                price_sar
//                regular_price_sar
//                price_aed
//                regular_price_aed
                NSString *regular_price_sar = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"regular_price_sar"]];
                NSString *regular_price_kwd = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"regular_price_kwd"]];
                NSString *regular_price_usd = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"regular_price_usd"]];
                NSString *regular_price_aed = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"regular_price_aed"]];
                
                NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                              NSStrikethroughColorAttributeName:[UIColor blackColor]};
                NSAttributedString * attr ;
                
                if ([currencyString isEqualToString:@"SAR"]) {
                    
                     attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",regular_price_sar] attributes:attribtues];
                    
                    _originalPriceLabel.attributedText = attr;
                    _discountPriceLabel.text = [NSString stringWithFormat:@"%@ SAR",[[dataDictionary objectForKey:@"data"]objectForKey:@"price_sar"]];
                }
                else if ([currencyString isEqualToString:@"KWD"]) {
                    attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",regular_price_kwd] attributes:attribtues];
                     _originalPriceLabel.attributedText = attr;
                    _discountPriceLabel.text = [NSString stringWithFormat:@"%@ KWD",[[dataDictionary objectForKey:@"data"]objectForKey:@"price_kwd"]];
                }
                else if ([currencyString isEqualToString:@"AED"]) {
                    attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",regular_price_aed] attributes:attribtues];
                     _originalPriceLabel.attributedText = attr;
                    _discountPriceLabel.text = [NSString stringWithFormat:@"%@ AED",[[dataDictionary objectForKey:@"data"]objectForKey:@"price_aed"]];
                }
                else if ([currencyString isEqualToString:@"USD"]) {
                    attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",regular_price_usd] attributes:attribtues];
                     _originalPriceLabel.attributedText = attr;
                    _discountPriceLabel.text = [NSString stringWithFormat:@"%@ USD",[[dataDictionary objectForKey:@"data"]objectForKey:@"price_usd"]];
                }
                
                
               // productPriceString = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"price_sar"]];
                
                productIdStr = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"prod_id"]];
                
               
                isWhishlistString = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"is_wish_list"]];
                
                if ([isWhishlistString isEqualToString:@"0"]) {
                    
                    [_favouriteBtn_Outlet setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
                
                }
                else{
                    [_favouriteBtn_Outlet setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
                   
                }
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

#pragma mark AddCartServicecall
-(void)addCartServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    //API-KEY=985869547,lang,prod_id,user_id,quantity,currency_type

    //http://volivesolutions.com/Klue/services/addcart
    NSMutableDictionary * addCartPostDictionary = [[NSMutableDictionary alloc]init];

    [addCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",productIdStr] forKey:@"prod_id"];
    [addCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",_quantityLabel.text] forKey:@"quantity"];
  //  [addCartPostDictionary setObject:currencyString ? currencyString :@"" forKey:@"currency_type"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"addcart" withPostDict:addCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Cart Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(void)addWhishlistServicecall
{

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id


    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];

    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",productIdStr] forKey:@"prod_id"];

    [[sharedclass sharedInstance]urlPerameterforPost:@"add_wishlist" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{

                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];


                    [self productsInfoServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }

                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
            });

        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark RemoveWhishlistServicecall
-(void)removeWhishlistServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id



    NSMutableDictionary * removeWhishlistPostDictionary = [[NSMutableDictionary alloc]init];

    [removeWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [removeWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",productIdStr] forKey:@"prod_id"];

    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:removeWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{

                if ([status isEqualToString:@"1"])
                {
                    // [SVProgressHUD dismiss];

                    [self productsInfoServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }

                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
            });

        }
        else{
            // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)favouriteBtn_Action:(id)sender {
    if (objectUserDefaults != nil) {
        if ([isWhishlistString isEqualToString:@"0"]) {
            [self addWhishlistServicecall];
        }
        else
        {
            [self removeWhishlistServicecall];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                    LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                    [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                             handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
}

- (IBAction)addToCartBtn_Action:(id)sender {
    if (objectUserDefaults != nil) {
    //if (value > 0) {
        [self addCartServicecall];
//    }
//    else{
//        NSLog(@"Please Select quantity");
//        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Quantity is required"] onViewController:self completion:nil];
//    }
    
    }
else
{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                    
                                                                       message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                handler:^(UIAlertAction * _Nonnull action) {
                            [SVProgressHUD dismiss];
            LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
            [self presentViewController:login animated:TRUE completion:nil];
                                                         }];
        UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                             handler:nil];
        
        [alert addAction:okButton];
        [alert addAction:cancelButton];
        
        [self presentViewController:alert animated:YES completion:nil];
    });
    
}
}
- (IBAction)plusBtn_Action:(id)sender {
    value ++;
    _quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    
    if (value > 1) {
        _minusBtn_Outlet.userInteractionEnabled = YES;
    }
    
}

- (IBAction)minusBtn_Action:(id)sender {
    value --;
    _quantityLabel.text = [NSString stringWithFormat:@"%d",value];
    
//    if ([_quantityLabel.text isEqualToString:@"1"]) {
//        _minusBtn_Outlet.userInteractionEnabled = NO;
//    }
    if (value == 1) {

        _minusBtn_Outlet.userInteractionEnabled = NO;
    }
}
@end
