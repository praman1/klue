//
//  MoreVC.m
//  Klue
//
//  Created by vishal bhatt on 19/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "MoreVC.h"
#import "WhishlistTableVC.h"
@interface MoreVC ()
- (IBAction)btnTapped:(id)sender;
- (IBAction)btnHome:(id)sender;

@property (weak, nonatomic) IBOutlet UIView *moreView;

@end

@implementation MoreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [_moreView.layer setShadowColor:[[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]CGColor]];
    [_moreView.layer setShadowOffset:CGSizeMake(5, 5)];
    [_moreView.layer setShadowRadius:10.0];
    [_moreView.layer setShadowOpacity:2.0];
    _moreView.layer.masksToBounds = NO;
    
    [_homeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"] forState:UIControlStateNormal];
    [_myOrdersBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"My Orders"] forState:UIControlStateNormal];
    [_whishListBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Whishlist"] forState:UIControlStateNormal];
    [_notificationbtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Notifications"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}



- (IBAction)btnTapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnHome:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
}

@end
