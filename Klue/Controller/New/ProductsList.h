//
//  ProductsList.h
//  Klue
//
//  Created by volive solutions on 05/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProductsList : UIViewController
@property NSMutableArray *array;
@property NSString *subCatIdString;
@end
