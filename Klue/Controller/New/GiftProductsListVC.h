//
//  GiftProductsListVC.h
//  Klue
//
//  Created by volivesolutions on 15/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GiftProductsListVC : UIViewController
@property NSString *totalString;
-(void)plusBtn:(UIButton*)sender collection:(customCollectionView*)collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeightConstrint;
-(void)setCollectionData:(NSArray *)collectData;
//-(void)setCollectionData:(NSArray *)collectData parentIndexPath:(NSIndexPath *)parent;
-(void)parentIndexPath:(NSIndexPath *)parent;
@property NSArray*coolArry;
@property NSIndexPath*tableIndexpath;
@property (weak, nonatomic) IBOutlet UILabel *totalAmount_lbl;

@end
