//
//  SubCategoryListVC.m
//  Klue
//
//  Created by volivesolutions on 27/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "SubCategoryListVC.h"
#import "PrefixHeader.pch"

@interface SubCategoryListVC ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    
    TableViewCell *cell;
    NSString *languageString;
    NSMutableArray *subCatDataArrayCount;
    NSMutableArray *subCatIdArray;
    NSMutableArray *subCatNameArray;
    NSMutableArray *subCatImageArray;
    NSString *sideCatIdStr,*sideCatNameStr;
    AppDelegate *delegate;
   
    NSArray * searchResultsArray;
    BOOL isupdateSearching;
    
    NSMutableArray *searchArray;
   
}
@property (weak, nonatomic) IBOutlet UISearchBar *catListSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *subCatListTableView;
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeight;


@end

@implementation SubCategoryListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // searchResultsArray = [NSMutableArray new];
    
     searchArray = [NSMutableArray new];
    _searchBarHeight.constant = 0;
    _catListSearchBar.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Search by Name"];
    delegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    sideCatIdStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"catId"];
    sideCatNameStr = [[NSUserDefaults standardUserDefaults]objectForKey:@"catName"];
 
   // [self subCatListServicecall];
  
    if ([delegate.sideMenuCheck isEqualToString:@"fromSide"]) {
        if ([sideCatIdStr isEqualToString:@"9"]) {
            _searchBarHeight.constant = 38;
            
        }
        self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:sideCatNameStr];
    }else{
        if ([_catIdString isEqualToString:@"9"]) {
            _searchBarHeight.constant = 38;
           
        }
        self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:_catNameString];
    }
   // self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:_catNameString];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    if (![self.checkStr isEqualToString:@"BACK"]) {
        
        SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            
            [_backBtn_Outlet setTarget: self.revealViewController];
            [_backBtn_Outlet setAction: @selector( revealToggle: )];
            // [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
            
        }
        
    }else
    {
        [_backBtn_Outlet setTarget: self];
        [_backBtn_Outlet setAction: @selector(backBtnAction)];
      // [self.navigationController popViewControllerAnimated:true];
        
      //  [_backBtn_Outlet setAction: @selector(backBtnAction)];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
   
    [self subCatListServicecall];
}


-(void)backBtnAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}

-(void)subCatListServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    // http://volivesolutions.com/Klue/services/sub_categories?API-KEY=985869547&lang=en&cat_id=7
    NSMutableDictionary * subCatPostDictionary = [[NSMutableDictionary alloc]init];
    
    [subCatPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [subCatPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    if ([delegate.sideMenuCheck isEqualToString:@"fromSide"]) {
        [subCatPostDictionary setObject:sideCatIdStr ? sideCatIdStr : @"" forKey:@"cat_id"];
    }else{
        [subCatPostDictionary setObject:_catIdString ? _catIdString : @"" forKey:@"cat_id"];
    }
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"sub_categories?" withPostDict:subCatPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            subCatNameArray = [NSMutableArray new];
            subCatIdArray = [NSMutableArray new];
            subCatImageArray = [NSMutableArray new];
            subCatDataArrayCount = [NSMutableArray new];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                subCatDataArrayCount = [dataDictionary objectForKey:@"data"];
                
                if (subCatDataArrayCount.count > 0) {
                    
                    for (int i = 0; subCatDataArrayCount.count > i; i++) {
                       
                        [subCatIdArray addObject: [[[dataDictionary objectForKey:@"data"]objectAtIndex:i] objectForKey:@"sub_cat_id"]];
                        [subCatNameArray addObject: [[[dataDictionary objectForKey:@"data"]objectAtIndex:i] objectForKey:@"sub_cat_name"]];
                        [subCatImageArray addObject: [[[dataDictionary objectForKey:@"data"]objectAtIndex:i] objectForKey:@"sub_cat_image"]];
                        
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_subCatListTableView reloadData];
                    
                });
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (isupdateSearching == NO) {
        
         return subCatNameArray.count;
        
    } else {
        
        return searchResultsArray.count;
        
    }
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_subCatListTableView dequeueReusableCellWithIdentifier:@"cell"];

    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
     if (isupdateSearching == NO) {
         
    [[sharedclass sharedInstance]shadowEffects:cell.orderView];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[subCatImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
    cell.productNameLabel.text = [subCatNameArray objectAtIndex:indexPath.row];
    
     }else
    {
        
        [[sharedclass sharedInstance]shadowEffects:cell.orderView];
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"sub_cat_image"]]]  placeholderImage:[UIImage imageNamed:@"men"]];
        
         cell.productNameLabel.text = [NSString stringWithFormat:@"%@",[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"sub_cat_name"]];
        
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   // if (isupdateSearching == NO) {
    CatProductsListVC *catProducts = [self.storyboard instantiateViewControllerWithIdentifier:@"CatProductsListVC"];
    catProducts.subCatIdString = [subCatIdArray objectAtIndex:indexPath.row];
    catProducts.subCatNameString = [subCatNameArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:catProducts animated:TRUE];
    [_subCatListTableView deselectRowAtIndexPath:indexPath animated:TRUE];
//    }
//    else{
//        CatProductsListVC *catProducts = [self.storyboard instantiateViewControllerWithIdentifier:@"CatProductsListVC"];
//        catProducts.subCatIdString = [NSString stringWithFormat:@"%@",[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"sub_cat_id"]];
//        catProducts.subCatNameString = [NSString stringWithFormat:@"%@",[[searchResultsArray objectAtIndex:indexPath.row] objectForKey:@"sub_cat_name"]];
//        [self.navigationController pushViewController:catProducts animated:TRUE];
//        [_subCatListTableView deselectRowAtIndexPath:indexPath animated:TRUE];
//    }
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewCell *cell = (TableViewCell*)[_subCatListTableView cellForRowAtIndexPath:indexPath];
    cell.orderView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.orderView.layer.borderWidth = 2;
}

-(void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = (TableViewCell*)[_subCatListTableView cellForRowAtIndexPath:indexPath];
    cell.orderView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.orderView.layer.borderWidth = 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

#pragma mark - SearchBarDelegate

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar;
{
    [_catListSearchBar resignFirstResponder];
    isupdateSearching = NO;
    
}

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text
{
    if (text.length > 0)
    {
        isupdateSearching = YES;
    }
    else
    {
        isupdateSearching = NO;
    }
    return YES;
}


- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar
{
    if (searchBar.text.length>0)
    {
        isupdateSearching = YES;
    }
    else
    {
        isupdateSearching = NO;
    }
    return YES;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
   
    if ([searchText isEqualToString:@""]) {
        
        isupdateSearching = NO;
        
        [_catListSearchBar resignFirstResponder];
        [_subCatListTableView reloadData];
        
    } else {
        
        isupdateSearching = YES;
        
        NSPredicate * namePredicate = [NSPredicate predicateWithFormat:@"(sub_cat_name CONTAINS[c] %@)", searchText];

        searchResultsArray = [subCatDataArrayCount filteredArrayUsingPredicate:namePredicate];
      
        NSLog(@"SearchResultsArray Is %@",searchResultsArray);
        
        [_subCatListTableView reloadData];
        
    }
    
}

- (BOOL)searchBar:(UISearchBar* )searchBar shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString* )string  {
    
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:ACCEPTABLE_CHARACTERS] invertedSet];
    
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    
    return [string isEqualToString:filtered];
    
}

- (IBAction)backBtn_Action:(id)sender {
 
}
@end
