//
//  AddressListVC.m
//  Klue
//
//  Created by volivesolutions on 08/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "AddressListVC.h"
#import "PrefixHeader.pch"

@interface AddressListVC ()<UITableViewDelegate,UITableViewDataSource>
{
    TableViewCell *cell;
    NSMutableArray *addressDataArrayCount;
    NSMutableArray *shippingAddressIdArray;
    NSMutableArray *finalAddressArray;
    
    NSString *languageString;
    NSString * addressStr;
    
    NSMutableArray *nameArray;
    NSMutableArray *emailIdArray;
    NSMutableArray *mobileNumberArray;
    NSMutableArray *addressArray;
    NSMutableArray *houseNoArray;
    NSMutableArray *landMarkArray;
    NSMutableArray *cityNameArray;

}
@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
- (IBAction)addNewAddressBTN_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addNewAddressBtn_Outlet;
- (IBAction)backBtn_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;
@end

@implementation AddressListVC

- (void)viewDidLoad {
    [super viewDidLoad];
   
    [[sharedclass sharedInstance]shadowEffects:_addNewAddressBtn_Outlet];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    [_addNewAddressBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Add New Address"] forState:UIControlStateNormal];

    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
//    UIBarButtonItem *flipButton = [[UIBarButtonItem alloc]
//                                   initWithImage:[UIImage imageNamed:@"back_arrow"] style:UIBarButtonItemStyleBordered target:self action:@selector(back)];
//    self.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
//    self.navigationItem.leftBarButtonItem = flipButton;
   
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"My Address"];
    
    
    [[sharedclass sharedInstance]shadowEffects:_addNewAddressBtn_Outlet];
    

    
    [self getShippingAddressServicecall];
    // Do any additional setup after loading the view.
}
-(void)viewWillAppear:(BOOL)animated
{
    [self getShippingAddressServicecall];
}

-(void)back
{
    NSLog(@"Back Button Pressed");
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//}

#pragma mark GetShippingAddressServicecall
-(void)getShippingAddressServicecall
{
//http://volivesolutions.com/Klue/services/get_shipping_addr
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    //API-KEY:985869547,lang:en,user_id

    NSMutableDictionary * getAddressPostDictionary = [[NSMutableDictionary alloc]init];

    [getAddressPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [getAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [getAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];


    [[sharedclass sharedInstance]urlPerameterforPost:@"get_shipping_addr" withPostDict:getAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Get Address Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{

                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    
                    addressDataArrayCount = [NSMutableArray new];
                    finalAddressArray = [NSMutableArray new];
                    shippingAddressIdArray = [NSMutableArray new];
                    nameArray = [NSMutableArray new];
                    emailIdArray = [NSMutableArray new];
                    mobileNumberArray = [NSMutableArray new];
                    addressArray = [NSMutableArray new];
                    landMarkArray = [NSMutableArray new];
                    cityNameArray = [NSMutableArray new];
                    
                    addressDataArrayCount = [dict objectForKey:@"data"];
                    
                    if (addressDataArrayCount.count > 0) {
                        for (int i =0; addressDataArrayCount.count > i; i++) {
                            
                            [shippingAddressIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"shipping_id"]]];
                            
                            [nameArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"username"]]];
                            [emailIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"email"]]];
                            [mobileNumberArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"mobile"]]];
                            [addressArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"address"]]];
                            //[houseNoArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"]]];
                            [cityNameArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"]]];
                            [landMarkArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"landmark"]]];
 
                            
                 addressStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"username"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"email"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"mobile"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"address"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"landmark"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"country"]];

                            [finalAddressArray addObject:addressStr];
                        }
                        NSLog(@"Final Address Array is %@",finalAddressArray);
                        
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_addressTableView reloadData];
                    });


                }

                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
            });

        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];

}

#pragma mark DeleteAddressServicecall
-(void)deleteAddressServicecall:(id)sender
{
//http://volivesolutions.com/Klue/services/del_shipping_addr
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_addressTableView];
    NSIndexPath *indexPath = [_addressTableView indexPathForRowAtPoint:position];
    //API-KEY:985869547,lang:en,shipping_id,user_id
    NSMutableDictionary * deleteAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [deleteAddressPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [deleteAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [deleteAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",[shippingAddressIdArray objectAtIndex:indexPath.row]] forKey:@"shipping_id"];
    [deleteAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"del_shipping_addr" withPostDict:deleteAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Get Address Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    [self getShippingAddressServicecall];
 
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];


}


#pragma mark TableviewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return addressDataArrayCount.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_addressTableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.addressView];
    [cell.editAddressBtn_Outlet addTarget:self action:@selector(editAddress:) forControlEvents:UIControlEventTouchUpInside];
    [cell.deleteAddressBtn_Outlet addTarget:self action:@selector(deleteAddressServicecall:) forControlEvents:UIControlEventTouchUpInside];
    cell.addressLabel.text = [NSString stringWithFormat:@"%@",[finalAddressArray objectAtIndex:indexPath.row]];
    
    return cell;
}

-(void)editAddress:(id)sender
{
    CGPoint position = [sender convertPoint:CGPointZero toView:_addressTableView];
    NSIndexPath *indexPath = [_addressTableView indexPathForRowAtPoint:position];
    
    AddAddressVC *addAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAddressVC"];
    addAddress.addressCheckString = @"fromAddressList";
    addAddress.shippingAddressId = [shippingAddressIdArray objectAtIndex:indexPath.row];
    addAddress.nameString = [nameArray objectAtIndex:indexPath.row];
    addAddress.emailIdString = [emailIdArray objectAtIndex:indexPath.row];
    addAddress.mobileNumberString = [mobileNumberArray objectAtIndex:indexPath.row];
    addAddress.addressString = [addressArray objectAtIndex:indexPath.row];
    addAddress.cityNameString = [cityNameArray objectAtIndex:indexPath.row];
   // addAddress.houseNoString = [shippingAddressIdArray objectAtIndex:indexPath.row];
    addAddress.landMarkString = [landMarkArray objectAtIndex:indexPath.row];

    [self.navigationController pushViewController:addAddress animated:TRUE];
    
}

- (IBAction)addNewAddressBTN_Action:(id)sender {
    
    AddAddressVC *address = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAddressVC"];
   
    [self.navigationController pushViewController:address animated:TRUE];
}
- (IBAction)backBtn_Action:(id)sender {
    
//    if ([_comForm isEqualToString:@"Menu"]) {
////        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
////        [self showViewController:home sender:self];
//
//    }
//    else {
//
//        [self dismissViewControllerAnimated:YES completion:nil];
//    }
}
@end
