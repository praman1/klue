//
//  PyamentMethodVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "PyamentMethodVC.h"
#import "PrefixHeader.pch"

@interface PyamentMethodVC ()
{
    NSString *languageString;
    NSString *currencyString;
    NSString *codPriceString;
    NSString *orderIdString;
    NSString *totalAmountString;
    NSString *paymentTypeString;
    NSString *orderDateString;
    
    //NSData *imageData;
    NSString *fileConstant;
    NSString *paymentCheckString;
    NSString *paymentMethodString;
    
    NSString *sdk_token;
    PayFortController *payFort;
    
    NSString *userNameString;
    NSString *emailIdString;
    NSString *mobileNoString;
}

- (IBAction)sadaadBtn_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *sadaadBtn_Outlet;

@property (weak, nonatomic) IBOutlet UILabel *paymentDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *cardNumberLabel;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *cardNumberTF;
@property (weak, nonatomic) IBOutlet UILabel *cardExpiryLabel;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *cardExpiryTF;
@property (weak, nonatomic) IBOutlet UILabel *CVVLabel;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *CVVTF;
@property (weak, nonatomic) IBOutlet UILabel *cardHolderNameLabel;
@property (weak, nonatomic) IBOutlet UIView *cardHolderNameTF;

@property (strong, nonatomic) IBOutlet UIView *card1;
@property (strong, nonatomic) IBOutlet UIView *card2;
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *payBtn_Outlet;
- (IBAction)payBtn_Action:(id)sender;
- (IBAction)onlineBtn_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *onlineBtn_Outlet;

@property (weak, nonatomic) IBOutlet UILabel *hintLabel;

@property (weak, nonatomic) IBOutlet UIButton *CODBtn_Outlet;
- (IBAction)CODBTN_Action:(id)sender;
@end

@implementation PyamentMethodVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Select Payment Method"];
    _hintLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"*Charges Apply For Payment Method COD"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    //if ([currencyString isEqualToString:@"SAR"]) {
        [_payBtn_Outlet setTitle:[NSString stringWithFormat:@"%@ %@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pay"],_orderTotal] forState:UIControlStateNormal];
//    }
//    else if ([currencyString isEqualToString:@"KWD"]) {
//        [_payBtn_Outlet setTitle:[NSString stringWithFormat:@"%@ %@KWD",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pay"],_orderTotal] forState:UIControlStateNormal];
//    }
//    else if ([currencyString isEqualToString:@"AED"]) {
//        [_payBtn_Outlet setTitle:[NSString stringWithFormat:@"%@ %@AED",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pay"],_orderTotal] forState:UIControlStateNormal];
//    }
//    else if ([currencyString isEqualToString:@"USD"]) {
//        [_payBtn_Outlet setTitle:[NSString stringWithFormat:@"%@ %@USD",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pay"],_orderTotal] forState:UIControlStateNormal];
//    }
    
//    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"fname"] forKey:@"userName"];
//
//    NSLog(@"User Name Is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]);
//
//    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"email"] forKey:@"emailId"];
//    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"mobile"] forKey:@"mobileNo"];
    
    userNameString = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    emailIdString = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
    mobileNoString = [[NSUserDefaults standardUserDefaults]objectForKey:@"mobileNo"];
    
    NSLog(@"User Name String Is :%@",userNameString);
    NSLog(@"EmailId String Is :%@",emailIdString);
    NSLog(@"MobileNo String Is :%@",mobileNoString);
    
    [_card1.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_card1.layer setShadowOffset:CGSizeMake(0, 0)];
    [_card1.layer setShadowRadius:3.0];
    [_card1.layer setShadowOpacity:0.3];
    _card1.layer.masksToBounds = NO;
    
    
    [_card2.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_card2.layer setShadowOffset:CGSizeMake(0, 0)];
    [_card2.layer setShadowRadius:3.0];
    [_card2.layer setShadowOpacity:0.3];
    _card2.layer.masksToBounds = NO;
    _imageData = [[NSUserDefaults standardUserDefaults]objectForKey:@"imageData"];
    
    [_onlineBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Online"] forState:UIControlStateNormal];
    [_sadaadBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"SADAAD"] forState:UIControlStateNormal];
    [_CODBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"COD"] forState:UIControlStateNormal];
    
}

-(void)viewWillAppear:(BOOL)animated
{
   // self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
   // self.navigationController.navigationBar.hidden = YES;
}

#pragma mark BookOrdersServicecall
-(void)bookOrdersServicecall
{
    //http://volivesolutions.com/Klue/services/booking_orders
    
    // API-KEY:985869547,lang:en,user_id,shipping_id,currency_type
    //"API-KEY:985869547,lang,user_id,shipping_id,currency_type,message note,payment_type:1,cod_price,delivery_date,delivery_time:2pm,file_upload"
    NSString *url = [NSString stringWithFormat:@"%@booking_orders",Base_URL];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary * bookOrderPostDictionary = [[NSMutableDictionary alloc]init];
    
    [bookOrderPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [bookOrderPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_shippingAddressIdStr ? _shippingAddressIdStr :@""] forKey:@"shipping_id"];
     [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",currencyString ? currencyString :@""] forKey:@"currency_type"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_messageString ? _messageString :@""] forKey:@"message"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_noteString ? _noteString :@""] forKey:@"note"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",paymentCheckString ? paymentCheckString :@""] forKey:@"payment_type"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",codPriceString ? codPriceString :@""] forKey:@"cod_price"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_deliveryDateString ? _deliveryDateString :@""] forKey:@"delivery_date"];
    [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_deliveryTimeString ? _deliveryTimeString :@""] forKey:@"delivery_time"];
   // [bookOrderPostDictionary setObject:[NSString stringWithFormat:@"%@",_imageData ? _imageData :@""] forKey:@"file_upload"];
    
    
    fileConstant = @"file_upload";
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in bookOrderPostDictionary) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [bookOrderPostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
   // _profileImageView.image = [self scaleImage:profileImage toSize:CGSizeMake(200.0,200.0)];
    
    
    //For Image Uploading
   // NSData *imageData = UIImageJPEGRepresentation(profileImage,1.0);
    if (_imageData) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:_imageData];
        [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    NSLog(@"Body Is %@",body);
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
        
        NSDictionary *statusDict = [[NSDictionary alloc]init];
        statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        NSLog(@"Images from server %@", statusDict);
        //
        NSString * status  = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
       // _profileImageView.image = profileImage;
        
        // message1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    //[[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                   
//                    "order_date" = "2018-06-14 06:48:28";
//                    "order_id" = 52752908;
//                    "payment_type" = COD;
//
//                    "total_amount" = "679.98";

                    orderIdString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"order_id"]];
                    totalAmountString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"total_amount"]];
                    orderDateString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"order_date"]];
                    paymentTypeString = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"payment_type"]];
                    
                    if ([paymentCheckString isEqualToString:@"1"]) {
                        [self createPayFortEnvironment];
                        
                    }else{
                        SuccessfulVC *success = [self.storyboard instantiateViewControllerWithIdentifier:@"SuccessfulVC"];
                        success.totalAmountString = [NSString stringWithFormat:@"%@",totalAmountString];
                        success.orderIdString = [NSString stringWithFormat:@"%@",orderIdString];
                        success.paymentTypeString = [NSString stringWithFormat:@"%@",paymentTypeString];
                        success.transactionDateString = [NSString stringWithFormat:@"%@",orderDateString];
                        //[self presentViewController:success animated:TRUE completion:nil];
                        [self.navigationController pushViewController:success animated:TRUE];
                    }
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[statusDict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
 
        }
        else
        {
            [SVProgressHUD dismiss];
            [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:nil];
            
        }
        
    }]
     
     resume];

}

#pragma  mark createPayFortEnvironment
-(void)createPayFortEnvironment{
    payFort = [[PayFortController  alloc]initWithEnviroment:KPayFortEnviromentSandBox];
    
    NSMutableString *post = [NSMutableString string];
    [post appendFormat:@"ssadsafsdaccess_code=%@", @"1ntKrEDL2LHgWxiSpCtN"];
    [post appendFormat:@"device_id=%@",  [payFort getUDID]];
    [post appendFormat:@"language=%@", @"en"];
    [post appendFormat:@"merchant_identifier=%@",@"YvmgwYWW"];
    [post appendFormat:@"service_command=%@", @"SDK_TOKENssadsafsd"];
    
    
    NSDictionary* tmp = @{ @"service_command": @"SDK_TOKEN",
                           
                           @"merchant_identifier":@"YvmgwYWW",
                           @"access_code": @"1ntKrEDL2LHgWxiSpCtN",
                           @"signature": [self sha1Encode:post],
                           @"language": @"en",
                           @"device_id": [payFort getUDID ] };
    NSError *error;
    NSData *postdata = [NSJSONSerialization dataWithJSONObject:tmp options:0 error:&error];
    NSString *BaseDomain = @"https://sbpaymentservices.payfort.com/FortAPI/paymentApi";
    NSString *urlString = [NSString stringWithFormat:@"%@",BaseDomain];
    //NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%ld",[postdata length]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] init];
    [request setURL:[NSURL URLWithString:urlString]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    //[request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postdata];
    //NSLog(@"signal mname %@", signalNameFunction);
    NSLog(@"url string %@",urlString);
    NSLog(@"url post %@",tmp);
    // NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request  delegate:self];
    //    NSURLConnection *connection = [NSURLConnection connectionWithRequest:request delegate:self];
    //    if(connection){
    //        webDataglobal = [NSMutableData data];
    //    }
    //    else{
    //        NSLog(@"The Connection is null");
    //    }
    
    
    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error)
                                  {
                                      if (!error)
                                      {
                                          NSError *error = nil;
                                          id object = [NSJSONSerialization
                                                       JSONObjectWithData:data
                                                       options:0
                                                       error:&error];
                                          //webDataglobal = [NSMutableData data];
                                          NSLog(@"object %@",object);
                                          
                                          if(error) {
                                              NSLog(@"Error %@",error);
                                              return;
                                          }
                                          self->sdk_token = object[@"sdk_token"];
                                          //[self startTheAction:input callback:callback];
                                          dispatch_async(dispatch_get_main_queue(), ^{
                                              
                                              [self launchPayFort];
                                              
                                          });
                                      }
                                      else
                                      {
                                          NSLog(@"Error: %@", error.localizedDescription);
                                      }
                                  }];
    [task resume];
}

#pragma mark Signature For PayFort
- (NSString*)sha1Encode:(NSString*)input {
    const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
    NSData *data = [NSData dataWithBytes:cstr length:input.length];
    
    uint8_t digest[CC_SHA256_DIGEST_LENGTH];
    
    CC_SHA256(data.bytes, (int)data.length, digest);
    
    NSMutableString* output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
    
    for(int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x", digest[i]];
    
    return output;
}

#pragma mark launchPayFort
-(void) launchPayFort{
    
    NSString *referenceId =  [NSString stringWithFormat:@"%lld", [@(floor([[NSDate date] timeIntervalSince1970] * 1000)) longLongValue]];
    
    NSMutableDictionary *request = [[NSMutableDictionary alloc]init];
    [request setValue:@"PURCHASE" forKey:@"command"];
    [request setValue:sdk_token forKey:@"sdk_token"];
    [request setValue:referenceId forKey:@"merchant_reference"];
    [request setValue:userNameString forKey:@"customer_name"];
    [request setValue:emailIdString forKey:@"customer_email"];
    [request setValue:mobileNoString forKey:@"phone_number"];
    [request setValue:@"" forKey:@"payment_option"];
    [request setValue:languageString forKey:@"language"];
    [request setValue:@"SAR" forKey:@"currency"];
    [request setValue:@"200" forKey:@"amount"];
    [request setValue:@"ECOMMERCE" forKey:@"eci"]; // ssadsafsd
    [request setValue:@"iPhone" forKey:@"order_description"];
    
    //[PayFort setPayFortCustomViewNib:@"PayFortView2"];
    payFort.IsShowResponsePage = YES;
    //    [payFort callPayFortWithRequest:request currentViewController:self
    //                            Success:^(NSDictionary *requestDic, NSDictionary *responeDic) {
    //                                NSLog(@"Success");
    //                                NSLog(@"responeDic=%@",responeDic);
    //                                callback(@[@"",responeDic, @true]);
    //                            }
    //                           Canceled:^(NSDictionary *requestDic, NSDictionary *responeDic) {
    //                               NSLog(@"Canceled");
    //                               NSLog(@"responeDic=%@",responeDic);
    //                               callback(@[RCTMakeError(@"Canceled", nil, nil)]);
    //                           }
    //                              Faild:^(NSDictionary *requestDic, NSDictionary *responeDic, NSString *message) {
    //                                  NSLog(@"Faild");
    //                                  NSLog(@"responeDic=%@",responeDic);
    //                                  callback(@[RCTMakeError(@"Faild", nil, nil)]);
    //                              }];
    
    
    
    [payFort callPayFortWithRequest:request currentViewController:self
                            Success:^(NSDictionary *requestDic, NSDictionary *responeDic) {
                                NSLog(@"Success");
                                NSLog(@"requestDic=%@",requestDic);
                                NSLog(@"responeDic=%@",responeDic);
                                /*
                                amount = 1;
                                "authorization_code" = 616172;
                                "card_holder_name" = naresh;
                                "card_number" = "400555******0001";
                                command = PURCHASE;
                                currency = SAR;
                                "customer_email" = "harishios2@gmail.com";
                                "customer_ip" = "183.82.113.60";
                                "customer_name" = Harishkumar;
                                eci = ECOMMERCE;
                                "expiry_date" = 2105;
                                "fort_id" = 154096848500039785;
                                language = en;
                                "merchant_reference" = 1540969275468;
                                "order_description" = iPhone;
                                "payment_option" = VISA;
                                "phone_number" = " 919533662221";
                                "response_code" = 14000;
                                "response_message" = Success;
                                "sdk_token" = 796B4979F16B6422E053321E320A3156;
                                status = 14;
                                "token_name" = 796828A4175F6424E053321E320A8FFD;
                                 */
                                NSString *amount = [responeDic objectForKey:@"amount"];
                                NSString *currency = [responeDic objectForKey:@"currency"];
                            
    SuccessfulVC *success = [self.storyboard instantiateViewControllerWithIdentifier:@"SuccessfulVC"];
    success.totalAmountString = [NSString stringWithFormat:@"%@%@",amount,currency];
    success.orderIdString = [NSString stringWithFormat:@"%@",orderIdString];
    success.paymentTypeString = [NSString stringWithFormat:@"%@",paymentTypeString];
    success.transactionDateString = [NSString stringWithFormat:@"%@",orderDateString];
                                //[self presentViewController:success animated:TRUE completion:nil];
    [self.navigationController pushViewController:success animated:TRUE];
                            }
                           Canceled:^(NSDictionary *requestDic, NSDictionary *responeDic) {
                               NSLog(@"Canceled");
                               NSLog(@"requestDic=%@",requestDic);
                               NSLog(@"responeDic=%@",responeDic);
                               
                           }
                              Faild:^(NSDictionary *requestDic, NSDictionary *responeDic, NSString *message) {
                                  NSLog(@"Faild");
                                  NSLog(@"requestDic=%@",requestDic);
                                  NSLog(@"responeDic=%@",responeDic);
                                  NSLog(@"message=%@",message);
                                  
                              }];
}


#pragma  mark ShippingMethodsServicecall
-(void)CODChargesServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klueusa/services/cod_list
    
    // API-KEY:985869547,lang:en
    NSMutableDictionary * codChargesPostDictionary = [[NSMutableDictionary alloc]init];
    
    [codChargesPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [codChargesPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    
    NSLog(@"dict is %@",codChargesPostDictionary);
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"cod_list" withPostDict:codChargesPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"COD Charges Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
//                    cod_price_sar
//                    cod_price_usd
//                    cod_price_kwd
//                    cod_price_aed
//                    if ([_cityNameStr isEqualToString:@"Riyadh"]) {
//                        codPriceString = @"";
//                    }else
//                    {
                        if ([currencyString isEqualToString:@"SAR"]) {
                            codPriceString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"cod_price_sar"]];
                            
                        }
                        else if ([currencyString isEqualToString:@"KWD"]) {
                            codPriceString = [NSString stringWithFormat:@"%@ KWD",[[dict objectForKey:@"data"] objectForKey:@"cod_price_kwd"]];
                        }
                        else if ([currencyString isEqualToString:@"AED"]) {
                            codPriceString = [NSString stringWithFormat:@"%@ AED",[[dict objectForKey:@"data"] objectForKey:@"cod_price_aed"]];
                        }
                        else if ([currencyString isEqualToString:@"USD"]) {
                            codPriceString = [NSString stringWithFormat:@"%@ USD",[[dict objectForKey:@"data"] objectForKey:@"cod_price_usd"]];
                                                }
                    NSLog(@"COD Price Is %@",codPriceString);
                    //}
 
                    dispatch_async(dispatch_get_main_queue(), ^{
  
                        [SVProgressHUD dismiss];
                    });
                    
                    
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}



- (IBAction)CODBTN_Action:(id)sender;
{
    paymentCheckString = @"1";
    paymentMethodString = @"COD";
    _card1.hidden = TRUE;
    _paymentDetailsLabel.hidden = TRUE;
    [_CODBtn_Outlet setTitleColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_onlineBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_sadaadBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
    if ([_cityNameStr isEqualToString:@"Riyadh"]) {
        codPriceString = @"0";
    }else{
        [self CODChargesServicecall];
 }
    
}

- (IBAction)onlineBtn_Action:(id)sender {
    _card1.hidden = FALSE;
    _paymentDetailsLabel.hidden = FALSE;
     paymentCheckString = @"1";
    paymentMethodString = @"Online";
    [_onlineBtn_Outlet setTitleColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_CODBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_sadaadBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

- (IBAction)sadaadBtn_Action:(id)sender {
    _card1.hidden = FALSE;
    _paymentDetailsLabel.hidden = FALSE;
     paymentCheckString = @"1";
    paymentMethodString = @"SADAAD";
    [_sadaadBtn_Outlet setTitleColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] forState:UIControlStateNormal];
    [_CODBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [_onlineBtn_Outlet setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
}
- (IBAction)payBtn_Action:(id)sender;
{
    if ([paymentCheckString length] != 0) {
        [self bookOrdersServicecall];
    }
    else{
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Please Select Payment Method"] onViewController:self completion:nil];
    }
    
   // SuccessfulVC *successVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SuccessfulVC"];
    //[self.navigationController pushViewController:successVC animated:TRUE];
    
}




- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
