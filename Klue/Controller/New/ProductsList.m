//
//  ProductsList.m
//  Klue
//
//  Created by volive solutions on 05/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ProductsList.h"
#import "PrefixHeader.pch"

@interface ProductsList ()<UICollectionViewDataSource,UICollectionViewDelegate>
{
//    CAShapeLayer *maskLayer;
//    UIBezierPath *maskPath;
    CollectionViewCell *cell;
    NSString *languageString;
    NSString *whishlistString;
    NSString *currencyString;
    NSMutableArray *selectUnselect_Array;
    NSMutableArray *filterCheckingArr;
    
    NSMutableArray *productIdArray ;
    NSMutableArray *productImageArray;
    NSMutableArray *productNameArray;
    
    NSMutableArray *productRegular_price_sarArray;
    NSMutableArray *productPrice_sarArray;
    
    NSMutableArray *productRegular_price_usdArray;
    NSMutableArray *productPrice_usdArray;
    
    NSMutableArray *productRegular_price_kwdArray;
    NSMutableArray *productPrice_kwdArray;
    
    NSMutableArray *productRegular_price_aedArray;
    NSMutableArray *productPrice_aedArray;
    
    NSMutableArray *productDataArrayCount;
    NSMutableArray *productBrandNameArray;
    NSMutableArray *whishlistCheckArray;
    NSObject*objectUserDefaults;
    
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *stackHeight;

@property (weak, nonatomic) IBOutlet UICollectionView *productsListCollectionView;
- (IBAction)back:(id)sender;
- (IBAction)moreBarBTN:(id)sender;
- (IBAction)cartBarBTN:(id)sender;
- (IBAction)filtersBTN:(id)sender;
- (IBAction)sortBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *filtersBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn_outlet;

@end

@implementation ProductsList

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _viewHeight.constant = 0;
    _stackHeight.constant = 0;
   // maskLayer = [CAShapeLayer layer];
    [self myCustomDesign];
    // Do any additional setup after loading the view.
}

-(void)myCustomDesign{
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Products List"];
    selectUnselect_Array = [[NSMutableArray  alloc]initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
    filterCheckingArr=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0", nil];
    
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_filtersBtn_Outlet.bounds byRoundingCorners:( UIRectCornerBottomLeft  | UIRectCornerTopLeft ) cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.view.bounds;
    maskLayer1.path  = maskPath1.CGPath;
    
    _filtersBtn_Outlet.layer.mask = maskLayer1;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_sortBtn_outlet.bounds byRoundingCorners:( UIRectCornerBottomRight |UIRectCornerTopRight ) cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.view.bounds;
    maskLayer2.path  = maskPath2.CGPath;
    
    _sortBtn_outlet.layer.mask = maskLayer2;
     currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    [self productsListServicecall];

}

#pragma mark ProductsListServicecall
-(void)productsListServicecall
{
        languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
        
        // http://volivesolutions.com/Klue/services/banner_cat_products?API-KEY=985869547&lang=en&user_id=3
        NSMutableDictionary * productsPostDictionary = [[NSMutableDictionary alloc]init];
        
        [productsPostDictionary setObject:API_KEY forKey:@"API-KEY"];
        [productsPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
        [productsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
        
        [[sharedclass sharedInstance]fetchResponseforParameter:@"banner_cat_products?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
            
            NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
            
            NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
            // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
            NSLog(@"%@",dataFromJson);
            NSLog(@"Data From Server%@",dataDictionary);
            
            if ([status isEqualToString:@"1"])
            {
                productIdArray = [NSMutableArray new];
                productImageArray = [NSMutableArray new];
                productNameArray = [NSMutableArray new];
                productDataArrayCount = [NSMutableArray new];
                whishlistCheckArray = [NSMutableArray new];
                productBrandNameArray = [NSMutableArray new];
                
                productRegular_price_sarArray = [NSMutableArray new];
                productPrice_sarArray = [NSMutableArray new];
                
                productRegular_price_usdArray = [NSMutableArray new];
                productPrice_usdArray = [NSMutableArray new];
                
                productRegular_price_kwdArray = [NSMutableArray new];
                productPrice_kwdArray = [NSMutableArray new];
                
                productRegular_price_aedArray = [NSMutableArray new];
                productPrice_aedArray = [NSMutableArray new];
     
                
               
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    productDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"products"];

                    if (productDataArrayCount.count > 0) {
                        for (int i = 0; productDataArrayCount.count > i; i++) {
                            [productIdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"prod_id"]];
                            [productNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"pname"]];
                            [productBrandNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"brand_name"]];
                            [productImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"prod_image"]];
                            
                            [productPrice_sarArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            [productRegular_price_sarArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            [productRegular_price_usdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            [productRegular_price_kwdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_aed"]];
                            [ productRegular_price_aedArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
                            
                            [whishlistCheckArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"is_wish_list"]];
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_productsListCollectionView reloadData];
                       
                    });
                    
                });
            }else {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    
                    [SVProgressHUD dismiss];
                    
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
                });
            }
            
        }];
        

}

#pragma  mark AddWhishlistServicecall
-(void)addWhishlistServicecall:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id
    CGPoint position = [sender convertPoint:CGPointZero toView:_productsListCollectionView];
    NSIndexPath *indexPath = [_productsListCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_wishlist" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    [self productsListServicecall];
                    

                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark RemoveWhishlistServicecall
-(void)removeWhishlistServicecall:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_productsListCollectionView];
    NSIndexPath *indexPath = [_productsListCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * removeWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [removeWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [removeWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:removeWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    [self productsListServicecall];
 
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}



- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _productsListCollectionView)
    {
        CGFloat width = (CGFloat)(_productsListCollectionView.frame.size.width/2);
        return CGSizeMake(width-5, 220);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return productNameArray.count;
   
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == _productsListCollectionView) {
        //cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productsCell" forIndexPath:indexPath];
        CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"productsCell" forIndexPath:indexPath];
        
        [[sharedclass sharedInstance]shadowEffects:cell.productView];
 
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
         cell.brandNameLabel.text = [productBrandNameArray objectAtIndex:indexPath.row];
        
        NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                      NSStrikethroughColorAttributeName:[UIColor blackColor]};
        NSAttributedString * attr;
        if ([currencyString isEqualToString:@"SAR"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",[productRegular_price_sarArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
        }
        
        else if ([currencyString isEqualToString:@"KWD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",[productRegular_price_kwdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"AED"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",[productRegular_price_aedArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"USD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",[productRegular_price_usdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
        }
        [cell.favouriteBtn addTarget:self action:@selector(addOrRemove:) forControlEvents:UIControlEventTouchUpInside];
        
        whishlistString = [NSString stringWithFormat:@"%@",[whishlistCheckArray objectAtIndex:indexPath.row]];
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            
        }
        
        return cell;
        
    }
 
    return NULL;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == _productsListCollectionView){
        
        ProductScreenVC *productInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductScreenVC"];
        productInfo.productIdString = [NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:productInfo animated:YES];
    }
    
}

-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = (CollectionViewCell*)[_productsListCollectionView cellForItemAtIndexPath:indexPath];
    cell.productView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.productView.layer.borderWidth = 2;
}

-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[_productsListCollectionView cellForItemAtIndexPath:indexPath];
    cell.productView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.productView.layer.borderWidth = 2;
}


-(void)addOrRemove:(id)sender
{
    if (objectUserDefaults != nil) {
        CGPoint position = [sender convertPoint:CGPointZero toView:_productsListCollectionView];
        NSIndexPath *indexPath = [_productsListCollectionView indexPathForItemAtPoint:position];
        CollectionViewCell *cell = (CollectionViewCell*)[_productsListCollectionView cellForItemAtIndexPath:indexPath];
        
        whishlistString = [NSString stringWithFormat:@"%@",[whishlistCheckArray objectAtIndex:indexPath.row]];
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            [self addWhishlistServicecall:sender];
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            [self removeWhishlistServicecall:sender];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
}

#pragma mark SortServicecall
-(void)sortServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/sort_products
    //"API-KEY:985869547,lang:en ,sub_cat_id,low_price,high_price,new_arrival"
    
    
    
    NSMutableDictionary * sortPostDictionary = [[NSMutableDictionary alloc]init];
    
    [sortPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [sortPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [sortPostDictionary setObject:API_KEY forKey:@"sub_cat_id"];
    [sortPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"low_price"];
    [sortPostDictionary setObject:API_KEY forKey:@"high_price"];
    [sortPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"new_arrival"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"sort_products" withPostDict:sortPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Sort Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
 
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_productsListCollectionView reloadData];
                    });
                    
                    
                    
                    // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


-(void)filtersClicked
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sort"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    
 
    
    UIAlertAction *priceLH = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Price Low to High"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
//        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"1"];
//        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"0"];
        //[self filterProductsServiceCall:sub_cat_id];
        
    }];
    UIAlertAction *priceHL = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Price High to Low"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
//        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
//        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"1"];
        //[self filterProductsServiceCall:sub_cat_id];
        
        
    }];
    UIAlertAction *newArrival = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"New Arrivals"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
//        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
//        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"1"];
        //[self filterProductsServiceCall:sub_cat_id];
        
        
    }];
    //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
   
    [alertController addAction:priceHL];
    [alertController addAction:priceLH];
    [alertController addAction:newArrival];
    // [controller addAction:latest];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (IBAction)back:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)moreBarBTN:(id)sender {
}

- (IBAction)cartBarBTN:(id)sender {
    
    ShoppingCartVC *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartVC"];
    [self.navigationController pushViewController:cart animated:TRUE];
}

- (IBAction)filtersBTN:(id)sender {
    
    FilterVC *filters = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
    [self.navigationController pushViewController:filters animated:TRUE];
}

- (IBAction)sortBTN:(id)sender {
     //[self filtersClicked];
}



@end
