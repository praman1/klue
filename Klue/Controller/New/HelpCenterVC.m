//
//  HelpCenterVC.m
//  Klue
//
//  Created by volivesolutions on 30/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "HelpCenterVC.h"

@interface HelpCenterVC ()
{
    NSString *languageString;
    NSString *descriptionString;
    NSAttributedString *attrSting;
    NSString *addressString;
    NSString *emailIdString;
    NSString *phoneNoString;
    NSString *pincodeString;
}
@property (weak, nonatomic) IBOutlet UITextView *helpCenterTextView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *addressValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailIdValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNoValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *pincodeValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailIdInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactNoInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *pincodeInfoLabel;

@end

@implementation HelpCenterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _addressInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Address:"];
    _emailIdInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Email ID:"];
    _contactNoInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Contact No:"];
    _pincodeInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Pincode:"];
    
    [self helpCenterServicecall];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Help Center"];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    // Do any additional setup after loading the view.
}

#pragma  mark HelpCenterServicecall
-(void)helpCenterServicecall
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    //    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //http://volivesolutions.com/Klue/services/help_center
    //API-KEY:985869547,lang:en

    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"help_center" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"help_center Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];
//                    address
//                    email
//                    phone_no
//                    pincode
                    addressString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"address"]];
                    NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[addressString dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];

                    
                    _addressValueLabel.attributedText = attString;
                    _addressValueLabel.font = [UIFont fontWithName:@"Montserrat-Medium" size:17.0];
                    _emailIdValueLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"email"]];
                    _contactNoValueLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"phone_no"]];
                    _pincodeValueLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"]objectForKey:@"pincode"]];
                    
                    
                    //[[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}
@end
