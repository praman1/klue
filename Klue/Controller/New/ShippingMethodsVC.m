//
//  ShippingMethodsVC.m
//  Klue
//
//  Created by volivesolutions on 24/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ShippingMethodsVC.h"
#import "PrefixHeader.pch"

@interface ShippingMethodsVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSString *languageString;
    NSString *currencyString;
    NSMutableArray *dataCountArray;
    NSMutableArray *shippingPriceArray;
    NSMutableArray *shippingMethodIdArray;
    NSMutableArray *shipping_name_Array;
    TableViewCell *shippingCell;
}
@property (weak, nonatomic) IBOutlet UITableView *shippingMethodsTableview;
- (IBAction)backBtn_Action:(id)sender;

@end

@implementation ShippingMethodsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Shipping Methods"];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self shippingMethodsServicecall];
    // Do any additional setup after loading the view.
}


#pragma  mark ShippingMethodsServicecall
-(void)shippingMethodsServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/shipping_methods
    
    // API-KEY:985869547,lang:en,currency_type,user_id
    NSMutableDictionary * shippingMethodsPostDictionary = [[NSMutableDictionary alloc]init];
    
    [shippingMethodsPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [shippingMethodsPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [shippingMethodsPostDictionary setObject:[NSString stringWithFormat:@"%@",currencyString ? currencyString :@""] forKey:@"currency_type"];
    [shippingMethodsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"shipping_methods" withPostDict:shippingMethodsPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Order Details Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    
                    dataCountArray = [NSMutableArray new];
                    //    "shipping_name": "1 day delivery",
                    //    "price_sar": "400",
                    //    "shipping_price_usd": "",
                    //    "shipping_price_aed": "",
                    //    "shipping_price_kwd": ""
  
                    shipping_name_Array = [NSMutableArray new];
                    shippingMethodIdArray = [NSMutableArray new];
                    shippingPriceArray = [NSMutableArray new];
//                    quantityArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
                    
                    if (dataCountArray.count > 0) {
                        for (int i =0; dataCountArray.count > i; i++) {
                            
                            [shipping_name_Array addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"shipping_name"]];
                            [shippingPriceArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"shipping_price"]];
                            [shippingMethodIdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        }
                        
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_shippingMethodsTableview reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark - Table view data source
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return dataCountArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    shippingCell = [_shippingMethodsTableview dequeueReusableCellWithIdentifier:@"cell"];
    if(shippingCell == nil)
    {
        
        shippingCell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:shippingCell.cellView];
    
    NSAttributedString *attString = [[NSAttributedString alloc] initWithData:[[shipping_name_Array objectAtIndex:indexPath.row] dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)} documentAttributes:nil error:nil];
    shippingCell.shippingMethodLabel.attributedText = attString;
    shippingCell.shippingMethodLabel.font = [UIFont fontWithName:@"Montserrat-Medium" size:15.0];
    shippingCell.shippingPriceLabel.text = [NSString stringWithFormat:@"%@",[shippingPriceArray objectAtIndex:indexPath.row]];
    
//    shippingCell.textLabel.textColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
//    shippingCell.textLabel.textAlignment =NSTextAlignmentCenter;
//    shippingCell.textLabel.text = [shipping_name_Array objectAtIndex:indexPath.row];
    
    return shippingCell;
}


-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewCell *cell = (TableViewCell*)[_shippingMethodsTableview cellForRowAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.cellView.layer.borderWidth = 2;
}

-(void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = (TableViewCell*)[_shippingMethodsTableview cellForRowAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.cellView.layer.borderWidth = 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    PyamentMethodVC *payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PyamentMethodVC"];
    payment.shippingMethodIdStr = [NSString stringWithFormat:@"%@",[shippingMethodIdArray objectAtIndex:indexPath.row]];
    payment.orderTotal = [NSString stringWithFormat:@"%@",_total];
    payment.shippingAddressIdStr = [NSString stringWithFormat:@"%@",_shippingString];
    [self.navigationController pushViewController:payment animated:TRUE];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 65;
}
- (IBAction)backBtn_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
