//
//  CollectionViewCell.h
//  Klue
//
//  Created by volive solutions on 05/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ButtonIndex.h"

@interface CollectionViewCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *categoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *brandNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *discountAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *originalAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *favBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIView *cellView;
@property (weak, nonatomic) IBOutlet UIButton *favouriteBtn;
@property (weak, nonatomic) IBOutlet UIImageView *bannerImageView;
@property (weak, nonatomic) IBOutlet UIView *productView;

@property (weak, nonatomic) IBOutlet UIImageView *adImageView;

//cell

@property (weak, nonatomic) IBOutlet UILabel *categoryNameLbl;

@property (weak, nonatomic) IBOutlet UIImageView *categoryImg;

//@property (weak, nonatomic) IBOutlet UIButton *quantityPlusBtn_Outlet;
//@property (weak, nonatomic) IBOutlet UIButton *quantityMinusBtn_outlet;
@property (weak, nonatomic) IBOutlet UILabel *quantityNumberLabel;
@property (weak, nonatomic) IBOutlet ButtonIndex *quantityPlusBtn_Outlet;
@property (weak, nonatomic) IBOutlet ButtonIndex *quantityMinusBtn_outlet;


@end
