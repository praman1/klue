//
//  GiftProductsListVC.m
//  Klue
//
//  Created by volivesolutions on 15/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "GiftProductsListVC.h"
#import "PrefixHeader.pch"
#import "customCollectionView.h"
#import "ButtonIndex.h"

@interface GiftProductsListVC ()<UICollectionViewDelegate,UICollectionViewDataSource,UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImagePickerController *imagePicker;
    NSData *imageData;
    NSString *localFilePath;
    TableViewCell *tableCell;
    CollectionViewCell *collectionCell;
    
    NSMutableArray *productIdArray;
    NSMutableArray *productIdAllArray;
    NSMutableArray *productImageArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productOriginalPriceArray;
    NSMutableArray *productDiscountPriceArray;
    NSMutableArray *productBrandNameArray;
    NSMutableArray *isWhishlistCheckArray;
    NSMutableArray *dataCountArray;
    NSMutableArray *giftTypeNameArray;
    NSMutableArray *allKeysArray;
    NSMutableArray *firstArray;
    NSMutableArray *orderArray;
    NSMutableDictionary*keysDisctionary;
    NSString *languageString;
    NSString *currencyString;
    NSString *productIdString;
    
    NSInteger value;
    NSMutableArray *quantityArray;
    NSMutableArray *priceArray;
    
    NSMutableArray *quantityAllArray;
    NSMutableArray *priceAllArray;
    NSIndexPath* tableIndexPath;
    
    NSMutableArray *indexpathArr;
    NSMutableArray *indexpathAllArr;
    NSMutableArray *priceStsticArray;
    NSMutableArray *priceStsticAllArray;
    NSMutableArray *subCatArr;
    NSMutableArray *subCatAllArr;
    
    NSMutableDictionary *indexpathDict;
    NSInteger total;
}

@property (weak, nonatomic) IBOutlet UIButton *buyNowBtn_Outlet;
@property (strong, nonatomic) IBOutlet UILabel *subTotalInfoLabel;

- (IBAction)buyNowBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UITableView *giftTableView;
- (IBAction)backBtn_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIView *buyNowView;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *skipBtn_Outlet;
- (IBAction)skipBtn_Action:(id)sender;
- (IBAction)browseBtn_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *browseBtn_Outlet;
@property (strong, nonatomic) IBOutlet UILabel *messageOrCardLabel;
@property (strong, nonatomic) IBOutlet UILabel *dontForgetLabel;
@property (strong, nonatomic) IBOutlet UITextView *messageTextView;
@property (strong, nonatomic) IBOutlet UILabel *noteLabel;
@property (strong, nonatomic) IBOutlet UITextView *noteTextView;

@end

@implementation GiftProductsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    [[sharedclass sharedInstance]shadowEffects:_buyNowBtn_Outlet];
    [[sharedclass sharedInstance]shadowEffects:_buyNowView];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Gift Preparation"];
    [_browseBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Browse"] forState:UIControlStateNormal];
    [_buyNowBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Buy Now"] forState:UIControlStateNormal];
    _subTotalInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Sub Total"];
    _messageOrCardLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Message/Card"];
    _dontForgetLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"*Don't forget to enter your name"];
    _noteLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Note"];
    _noteLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Upload Image"];
    
    _skipBtn_Outlet.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"SKIP"];
  //  customCollectionView*collection;
    
    _messageTextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _messageTextView.layer.borderWidth = 1;
    _messageTextView.layer.cornerRadius = 5;
    
    _noteTextView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    _noteTextView.layer.borderWidth = 1;
    _noteTextView.layer.cornerRadius = 5;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(plusBtn:collection:)
                                                 name:@"GearButtonPressedNotification"
                                               object:nil];
    [self giftProductsServicecall];
    if (total  == 0) {
        if ([currencyString isEqualToString:@"SAR"]) {
            _totalAmount_lbl.text = @"0.0 SAR";
        }
        else if ([currencyString isEqualToString:@"KWD"]) {
            _totalAmount_lbl.text = @"0.0 KWD";
        }
        else if ([currencyString isEqualToString:@"AED"]) {
            _totalAmount_lbl.text = @"0.0 AED";
        }
        else if ([currencyString isEqualToString:@"USD"]) {
            _totalAmount_lbl.text = @"0.0 USD";
        }
        
    }
   // [[sharedclass sharedInstance]shadowEffects:collectionCell.cellView];
    // Do any additional setup after loading the view.
}

//-(void)viewWillAppear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = NO;
//    self.tabBarController.tabBar.hidden = FALSE;
//}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = TRUE;
//}

-(void)giftProductsServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/gift_products
    
    
    NSMutableDictionary * myOrdersPostDictionary = [[NSMutableDictionary alloc]init];
    
    [myOrdersPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [myOrdersPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"gift_products" withPostDict:myOrdersPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Gift Products Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
 
                   
                    productImageArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productOriginalPriceArray = [NSMutableArray new];
                    productDiscountPriceArray = [NSMutableArray new];
                    productBrandNameArray = [NSMutableArray new];
                    isWhishlistCheckArray = [NSMutableArray new];
                    giftTypeNameArray = [NSMutableArray new];
                    firstArray = [NSMutableArray new];
                    keysDisctionary = [NSMutableDictionary new];
                    
                    priceAllArray = [NSMutableArray new];
                    productIdAllArray = [NSMutableArray new];
                    quantityAllArray = [NSMutableArray new];
                    indexpathArr = [NSMutableArray new];
                    indexpathAllArr = [NSMutableArray new];
                    subCatArr = [NSMutableArray new];
                    subCatAllArr = [NSMutableArray new];
                    priceStsticAllArray = [NSMutableArray new];
                    indexpathDict = [NSMutableDictionary new];
                    NSMutableDictionary* dataDict = [dict objectForKey:@"data"];
                    allKeysArray = [[NSMutableArray alloc]initWithArray:dataDict.allKeys];
                    
                    for (int i=0; i<allKeysArray.count; i++) {
                        
                        [keysDisctionary setObject:[dataDict objectForKey:[NSString stringWithFormat:@"%d",i+1]] forKey:[NSNumber numberWithInt:i]];
                        
                    }
                    NSLog(@"keys dict %@",keysDisctionary);
                   
                    for (int j=0; j<allKeysArray.count; j++) {
                        
                        NSArray*dictCount=  [keysDisctionary objectForKey:[NSNumber numberWithInteger:j]];
                        [indexpathArr addObject:[NSNumber numberWithInteger:j]];
                        priceArray = [NSMutableArray new];
                        quantityArray = [NSMutableArray new];
                        priceStsticArray = [NSMutableArray new];
                         productIdArray = [NSMutableArray new];
                        
                        for (int k=0; k<dictCount.count; k++) {
                            
                            NSDictionary*dictData=[dictCount objectAtIndex:k];
                            
                            [quantityArray addObject:[NSNumber numberWithInt:0]];
                            [productIdArray addObject:[dictData objectForKey:@"prod_id"]];
                            
                            if ([currencyString isEqualToString:@"SAR"]) {
                                [priceStsticArray addObject:[dictData objectForKey:@"price_sar"]];
                                [priceArray addObject:[dictData objectForKey:@"price_sar"]];
                            }
                            else if ([currencyString isEqualToString:@"KWD"]) {
                                [priceStsticArray addObject:[dictData objectForKey:@"price_kwd"]];
                                [priceArray addObject:[dictData objectForKey:@"price_kwd"]];
                            }
                            else if ([currencyString isEqualToString:@"AED"]) {
                                [priceStsticArray addObject:[dictData objectForKey:@"price_aed"]];
                                [priceArray addObject:[dictData objectForKey:@"price_aed"]];
                            }
                            else if ([currencyString isEqualToString:@"USD"]) {
                                [priceStsticArray addObject:[dictData objectForKey:@"price_usd"]];
                                [priceArray addObject:[dictData objectForKey:@"price_usd"]];
                            }
                        }
                        
                        [priceAllArray addObject:priceArray];
                        [productIdAllArray addObject:productIdArray];
                        [quantityAllArray addObject:quantityArray];
                        [priceStsticAllArray addObject:priceStsticArray];
                        
                    }
                    
                    NSLog(@"all keys arra is %@",[keysDisctionary objectForKey:[NSNumber numberWithInt:4]]);
                    NSSortDescriptor *descriptor=[[NSSortDescriptor alloc] initWithKey:@"self" ascending:YES];
                    NSArray *descriptors=[NSArray arrayWithObject: descriptor];
                    NSArray *reverseOrder=[allKeysArray sortedArrayUsingDescriptors:descriptors];
                    orderArray = [NSMutableArray arrayWithArray:reverseOrder];
                    
                    if (orderArray.count > 0) {
                        
                        for (int i =0; orderArray.count > i; i++) {
                            
                            [firstArray addObject:[dataDict valueForKey:[orderArray objectAtIndex:i]]];
                           // [giftTypeNameArray addObject:[keysDisctionary objectForKey:@"sub_cat_name"]];
                            
                        }
                    }
                    NSLog(@"allKeysArray Is %@",orderArray);
                    NSLog(@"firstArray Is %@",firstArray);
                    

                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self loadViewHeightConstraint];
                        [_giftTableView reloadData];
                    });
                    
                    
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}
-(void)loadViewHeightConstraint{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.viewHeightConstrint.constant=(allKeysArray.count)* 210 + 107;
    });
    
}
#pragma mark QuantityIncrementServicecall
-(void) quantityIncrementServicecall:(id)sender
{

//    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
//    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
    // TableViewCell *cell = (TableViewCell*)[_shippingCartTableView cellForRowAtIndexPath:indexPath];

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];


    //API-KEY:985869547,lang:en,prod_id,user_id,quantity

    //http://volivesolutions.com/Klue/services/gift_addcart
    NSMutableDictionary * addCartPostDictionary = [[NSMutableDictionary alloc]init];

    [addCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",productIdString ? productIdString :@""] forKey:@"prod_id"];
    [addCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%ld",value+1] forKey:@"quantity"];

    [[sharedclass sharedInstance]urlPerameterforPost:@"gift_addcart" withPostDict:addCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"gift_addcart Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{

                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];

                }

                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
            });

        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark QuantityDecrementServicecall
-(void)quantityDecrementServicecall:(id)sender
{
//    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
//    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
//    TableViewCell *cell = (TableViewCell*)[_shippingCartTableView cellForRowAtIndexPath:indexPath];

    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    // API-KEY=985869547,lang,prod_id,user_id,quantity

    //http://volivesolutions.com/Klue/services/addcart_dec
    NSMutableDictionary * addCartPostDictionary = [[NSMutableDictionary alloc]init];

    [addCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",productIdString ? productIdString :@""] forKey:@"prod_id"];
    [addCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%ld",value-1] forKey:@"quantity"];

    [[sharedclass sharedInstance]urlPerameterforPost:@"addcart_dec" withPostDict:addCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Quantity Decrement Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{

                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];

                    //[[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];


                }

                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
            });

        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}
#pragma mark TableViewDelegates

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return orderArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:  (NSIndexPath *)indexPath
{
    
    
    tableCell = [_giftTableView dequeueReusableCellWithIdentifier:@"tableCell" forIndexPath:indexPath];
    
     NSArray *productNameArr  = [keysDisctionary objectForKey:[NSNumber numberWithInteger:indexPath.row]];
  //  NSLog(@"product names at %ld  arr %@",(long)indexPath.row,productNameArr);
    
    if (tableCell == nil)
    {
        tableCell = [[TableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableCell"];
    }
    tableCell.giftsTypeNameLabel.text = [NSString stringWithFormat:@"%ld.%@",indexPath.row+1,[[productNameArr objectAtIndex:0] valueForKey:@"sub_cat_name"]];
  
   
    // call collection view delegate method in table view cell.
    [tableCell.viewMoreBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"View More"] forState:UIControlStateNormal];
    tableCell.giftsCollectionView.delegate=self;
    tableCell.giftsCollectionView.dataSource=self;
    tableCell.giftsCollectionView.parentIndexpath = indexPath;
    
    //tableCell.giftsCollectionView.delegate1=self;
    tableCell.giftsCollectionView.tag = indexPath.row;
  //  [tableCell co]
    self.giftTableView.tag=indexPath.row;
    [self setCollectionData:productNameArr];
   // [self setCollectionData:productNameArr parentIndexPath:indexPath];
   // [self parentIndexPath:indexPath];
    
    return tableCell;
}

-(void)setCollectionData:(NSArray *)collectData{

    self.coolArry=collectData;

    [tableCell.giftsCollectionView reloadData];

}
-(void)setCollectionData:(NSArray *)collectData parentIndexPath:(NSIndexPath *)parent{
    self.coolArry=collectData;
    self.tableIndexpath=parent;
   
    [tableCell.giftsCollectionView reloadData];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 210;
}

#pragma mark CollectionViewDelegates
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
   // NSInteger tagIndex=self.giftTableView.tag;
    //  NSArray*collectionViewArray=[keysDisctionary objectForKey:[NSNumber numberWithInteger:tagIndex]];
   // NSLog(@"collection view array values %@",collectionViewArray);
  //  NSLog(@"collection view array values %ld",(long)tagIndex);
 //  NSArray *collectionViewArray=[keysDisctionary objectForKey:[NSNumber numberWithInteger:tagIndex]];
    
    return self.coolArry.count;

}

//- (NSIndexPath *)indexPathForRowAtPoint:(CGPoint)point
//
//{
//    return 0;
//}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   
  //   collectionCell = (CollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
      collectionCell = [tableCell.giftsCollectionView dequeueReusableCellWithReuseIdentifier:@"collectionCell" forIndexPath:indexPath];
    
   
    [[sharedclass sharedInstance]shadowEffects:collectionCell.cellView];

    
    collectionCell.quantityPlusBtn_Outlet.buttonRow=tableCell.giftsCollectionView.tag;
    collectionCell.quantityPlusBtn_Outlet.buttonIndexpath=indexPath;
    
    collectionCell.quantityMinusBtn_outlet.buttonRow=tableCell.giftsCollectionView.tag;
    collectionCell.quantityMinusBtn_outlet.buttonIndexpath=indexPath;

    NSArray*priceArr=[priceAllArray objectAtIndex:tableCell.giftsCollectionView.tag];
    NSArray*quantityarr=[quantityAllArray objectAtIndex:tableCell.giftsCollectionView.tag];
    
    NSDictionary *dictdata = [self.coolArry objectAtIndex:indexPath.row];
    
    collectionCell.quantityNumberLabel.text = [NSString stringWithFormat:@"%@",quantityarr[indexPath.row]];
    //collectionCell.brandNameLabel.text=[dictdata objectForKey:@"brand_name"];
    NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                  NSStrikethroughColorAttributeName:[UIColor blackColor]};
    NSAttributedString * attr;
    if ([currencyString isEqualToString:@"SAR"]) {
        
         attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",[dictdata objectForKey:@"regular_price_sar"]] attributes:attribtues];
        collectionCell.originalAmountLabel.attributedText = attr;
        collectionCell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR",priceArr[indexPath.row]];
    }
    else if ([currencyString isEqualToString:@"KWD"]) {
        attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",[dictdata objectForKey:@"regular_price_kwd"]] attributes:attribtues];
        collectionCell.originalAmountLabel.attributedText = attr;
        collectionCell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ KWD",[dictdata objectForKey:@"price_kwd"]];
    }
    else if ([currencyString isEqualToString:@"AED"]) {
        attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",[dictdata objectForKey:@"regular_price_aed"]] attributes:attribtues];
        collectionCell.originalAmountLabel.attributedText = attr;
        collectionCell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ AED",[dictdata objectForKey:@"price_aed"]];
    }
    else if ([currencyString isEqualToString:@"USD"]) {
        attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",[dictdata objectForKey:@"regular_price_usd"]] attributes:attribtues];
        collectionCell.originalAmountLabel.attributedText = attr;
        collectionCell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ USD",[dictdata objectForKey:@"price_usd"]];
    }
    
    collectionCell.brandNameLabel.text = [NSString stringWithFormat:@"%@",[dictdata objectForKey:@"brand_name"]];
    [collectionCell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[dictdata objectForKey:@"prod_image"]]] placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
    [collectionCell.quantityPlusBtn_Outlet addTarget:self action:@selector(plusBtn:) forControlEvents:UIControlEventTouchUpInside];
    [collectionCell.quantityMinusBtn_outlet addTarget:self action:@selector(minusQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
//    NSLog(@"price index %ld is =====%@",(long)indexPath.row,[NSString stringWithFormat:@"%@ SAR",[dictdata objectForKey:@"price_sar"]]);
//    NSLog(@"regular price index %ld is =====%@",(long)indexPath.row,[dictdata objectForKey:@"regular_price_sar"]);
//    NSLog(@"image data %ld is =====%@",(long)indexPath.row,[NSString stringWithFormat:@"%@%@",BASE_PATH,[dictdata objectForKey:@"prod_image"]]);
   // NSLog(@"dictdata is =====%@",[dictdata objectForKey:@"price_sar"]);
    
//    NSLog(@"object ibde xis ----%ld pname",[productNameArr count]);
    
  //NSLog(@"collection path row is ----%ld",collectionIndex);
    
    return collectionCell;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView
                   layout:(UICollectionViewLayout *)collectionViewLayout
minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 0;
}


- (void)collectionView:(customCollectionView *)collectionView
didSelectItemAtIndexPath:(NSIndexPath *)indexPath

{



    NSInteger tableindex = collectionView.parentIndexpath.row;

    NSLog(@"collection path row is ----%ld",tableindex);
    NSLog(@"collection path row is ----%ld",indexPath.row);

    //you can get all that index path number . Enjoy :)

}




-(void)plusBtn:(UIButton *)sender{

    
    ButtonIndex*button=(ButtonIndex*)sender;
    NSLog(@"index path row is %ld",button.buttonIndexpath.row);
    NSLog(@"parent index path row is %ld",button.buttonRow);

    
        NSMutableArray*priceArr=[[NSMutableArray alloc]initWithArray:[priceStsticAllArray objectAtIndex:button.buttonRow]];
        NSMutableArray*quantityarr=[[NSMutableArray alloc]initWithArray:[quantityAllArray objectAtIndex:button.buttonRow]];
        value=[[quantityarr objectAtIndex:button.buttonIndexpath.row] integerValue];
        NSInteger pricevalue=[[priceArr objectAtIndex:button.buttonIndexpath.row] integerValue] * (value+1);


        [quantityarr replaceObjectAtIndex:button.buttonIndexpath.row withObject:[NSNumber numberWithInteger:value+1]];
    
        NSMutableArray*priceDummyArr=[[NSMutableArray alloc]initWithArray:[priceAllArray objectAtIndex:button.buttonRow]];
    
        [priceDummyArr replaceObjectAtIndex:button.buttonIndexpath.row withObject:[NSNumber numberWithInteger:pricevalue]];
    
    NSInteger total=0;
   // total=total;

        [priceAllArray replaceObjectAtIndex:button.buttonRow withObject:priceDummyArr];
        [quantityAllArray replaceObjectAtIndex:button.buttonRow withObject:quantityarr];
    
    for (int i=0; i<priceAllArray.count; i++) {
        NSMutableArray*priceArr1=[[NSMutableArray alloc]initWithArray:[priceAllArray objectAtIndex:i]];
         NSMutableArray*quantArr1=[[NSMutableArray alloc]initWithArray:[quantityAllArray objectAtIndex:i]];
         for (int j=0; j<priceArr1.count; j++) {
            NSInteger value=[[quantArr1 objectAtIndex:j] integerValue];
             if (value>=1) {
                 total=total+[priceArr1[j] integerValue];
             }
         }
    }

      //  NSLog(@"quantity all array row is %@",quantityAllArray);
      //  NSLog(@"price all array row is %@",priceAllArray);
    NSLog(@"Total value is %ld",(long)total);

//
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([currencyString isEqualToString:@"SAR"]) {
                _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld SAR",(long)total];
            }
            else if ([currencyString isEqualToString:@"KWD"]) {
                _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld KWD",(long)total];
            }
            else if ([currencyString isEqualToString:@"AED"]) {
                _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld AED",(long)total];
            }
            else if ([currencyString isEqualToString:@"USD"]) {
                _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld USD",(long)total];
            }
            
                [self.giftTableView reloadData];
        });
     NSMutableArray*productIdArr=[[NSMutableArray alloc]initWithArray:[productIdAllArray objectAtIndex:button.buttonRow]];
    productIdString = [productIdArr objectAtIndex:button.buttonRow];
    NSLog(@"ProductIdString Is %@",productIdString);

    [self quantityIncrementServicecall:sender];

}


-(void)minusQuantity:(UIButton*)sender
{
    
    
    ButtonIndex*button=(ButtonIndex*)sender;
    NSLog(@"index path row is %ld",button.buttonIndexpath.row);
    NSLog(@"parent index path row is %ld",button.buttonRow);
    
    NSMutableArray*priceArr=[[NSMutableArray alloc]initWithArray:[priceStsticAllArray objectAtIndex:button.buttonRow]];
    NSMutableArray*quantityarr=[[NSMutableArray alloc]initWithArray:[quantityAllArray objectAtIndex:button.buttonRow]];
 
    NSInteger value=[[quantityarr objectAtIndex:button.buttonIndexpath.row] integerValue];
    
    if (value == 0) {
        
    }
    else{
        
    NSInteger pricevalue=[[priceArr objectAtIndex:button.buttonIndexpath.row] integerValue] * (value-1);
    
    
    [quantityarr replaceObjectAtIndex:button.buttonIndexpath.row withObject:[NSNumber numberWithInteger:value-1]];
    
    NSMutableArray*priceDummyArr=[[NSMutableArray alloc]initWithArray:[priceAllArray objectAtIndex:button.buttonRow]];
    
    [priceDummyArr replaceObjectAtIndex:button.buttonIndexpath.row withObject:[NSNumber numberWithInteger:pricevalue]];
    
    [priceAllArray replaceObjectAtIndex:button.buttonRow withObject:priceDummyArr];
    [quantityAllArray replaceObjectAtIndex:button.buttonRow withObject:quantityarr];
    
    NSLog(@"quantity all array row is %@",quantityAllArray);
    NSLog(@"price all array row is %@",priceAllArray);
        
     total=0;
        
        for (int i=0; i<priceAllArray.count; i++) {
            NSMutableArray*priceArr1=[[NSMutableArray alloc]initWithArray:[priceAllArray objectAtIndex:i]];
            NSMutableArray*quantArr1=[[NSMutableArray alloc]initWithArray:[quantityAllArray objectAtIndex:i]];
            for (int j=0; j<priceArr1.count; j++) {
                NSInteger value=[[quantArr1 objectAtIndex:j] integerValue];
                if (value>=1) {
                    total=total+[priceArr1[j] integerValue];
                }
            }
        }
        
        //  NSLog(@"quantity all array row is %@",quantityAllArray);
        //  NSLog(@"price all array row is %@",priceAllArray);
        NSLog(@"Total value is %ld",(long)total);
   
    dispatch_async(dispatch_get_main_queue(), ^{
        if ([currencyString isEqualToString:@"SAR"]) {
            _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld SAR",(long)total];
        }
        else if ([currencyString isEqualToString:@"KWD"]) {
            _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld KWD",(long)total];
        }
        else if ([currencyString isEqualToString:@"AED"]) {
            _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld AED",(long)total];
        }
        else if ([currencyString isEqualToString:@"USD"]) {
            _totalAmount_lbl.text=[NSString stringWithFormat:@"%ld USD",(long)total];
        }
        
        [self.giftTableView reloadData];
    });
        [self quantityDecrementServicecall:sender];
    }

    
}


- (IBAction)backBtn_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)buyNowBtn_Action:(id)sender {
    
    OrderSummaryVC *summery = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderSummaryVC"];
    summery.subTotalString = [NSString stringWithFormat:@"%@",_totalString];
    summery.image = localFilePath;
    summery.messageString = [NSString stringWithFormat:@"%@",_messageTextView.text];
    summery.noteString = [NSString stringWithFormat:@"%@",_noteTextView.text];
    [self.navigationController pushViewController:summery animated:TRUE];

}
- (IBAction)skipBtn_Action:(id)sender {
    OrderSummaryVC *summery = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderSummaryVC"];
    summery.subTotalString = [NSString stringWithFormat:@"%@",_totalString];
    [self.navigationController pushViewController:summery animated:TRUE];
}


- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage* )image editingInfo:(NSDictionary *)editingInfo {
    
    if (picker == imagePicker) {
        
        imageData = UIImageJPEGRepresentation(image,1.0);
        [[NSUserDefaults standardUserDefaults]setObject:imageData forKey:@"imageData"];
//        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//        NSString *documentsDirectory = [paths objectAtIndex:0];
//        localFilePath = [documentsDirectory stringByAppendingPathComponent:@"png"];
//        [imageData writeToFile:localFilePath atomically:YES];
//        NSLog(@"localFilePath.%@",localFilePath);
        NSLog(@"Browse Image Data Is %@",imageData);
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Image uploaded succefully"] onViewController:self completion:nil];
//        profileImage = image;
//
//        _profileImageView.image=profileImage;
        //[self networkCheck];
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(UIImage *)scaleImage:(UIImage* )image toSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}


- (IBAction)browseBtn_Action:(id)sender {
    
    UIAlertController * view=   [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pick the image"] message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"From"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* PhotoLibrary = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Photo Library"]
                                                           style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                                               
                                                               imagePicker = [[UIImagePickerController alloc] init];
                                                               imagePicker.delegate = self;
                                                               [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                                               [self presentViewController:imagePicker animated:YES completion:NULL];
                                                               [view dismissViewControllerAnimated:YES completion:nil];    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {  }];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Camera"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action) {
        
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:imagePicker animated:YES completion:NULL];
        [view dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [view addAction:PhotoLibrary];
    [view addAction:camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}
@end
