//
//  NoInternetVCViewController.m
//  Klue
//
//  Created by volivesolutions on 02/07/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "NoInternetVCViewController.h"

@interface NoInternetVCViewController ()
@property (weak, nonatomic) IBOutlet UIButton *okBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIImageView *noInternetImageview;
@property (weak, nonatomic) IBOutlet UILabel *noInternetInfoLabel;

- (IBAction)okBtn_Action:(id)sender;
@end

@implementation NoInternetVCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _noInternetInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Oops, your internet connection seems to be off"];
    [_okBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Retry"] forState:UIControlStateNormal];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)okBtn_Action:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
