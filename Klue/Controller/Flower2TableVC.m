//
//  Flower2TableVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "Flower2TableVC.h"
#import "PrefixHeader.pch"

@interface Flower2TableVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    CollectionViewCell *cell;
    
}

@property (weak, nonatomic) IBOutlet UICollectionView *catCollectionView;

- (IBAction)btnBack:(id)sender;
- (IBAction)moreBTN:(id)sender;
- (IBAction)cartBTN:(id)sender;

@end

@implementation Flower2TableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
    
 
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _catCollectionView)
    {
        CGFloat width = (CGFloat)(_catCollectionView.frame.size.width/2);
        return CGSizeMake(width-8, 220);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return 10;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{

    if (collectionView == _catCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
        
        
        
        
        //cell.categoriesImageview.layer.cornerRadius = cell.categoriesImageview.frame.size.width/2;
        
        //cell.categoriesImageview.layer.borderColor = [UIColor colorWithRed:<#(CGFloat)#> green:<#(CGFloat)#> blue:<#(CGFloat)#> alpha:<#(CGFloat)#>]
        
        //        cell.categoriesImageview.clipsToBounds = YES;
        //        cell.categoriesImageview.layer.borderColor = [[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]CGColor];
        //        cell.categoriesImageview.layer.cornerRadius=cell.categoriesImageview.frame.size.width/2;
        //        [cell.categoriesImageview sd_setImageWithURL:[NSURL URLWithString:[_categoriesImagesArray objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        //        cell.categoryNameLabel.text = [_categoriesNamesArray objectAtIndex:indexPath.row];
        
        return cell;
        
    }
    
    return NULL;
    
}

//-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
//{
//    if(collectionView == _catCollectionView){
//
//        ProductScreenVC *productInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductScreenVC"];
//
//        // [productInfo productInfoServiceCall:[productIdArr objectAtIndex:indexPath.row]];
//
//        //[self serviceCall:[self.sub_cat_idArr objectAtIndex:indexPath.row]];
//
//        [self.navigationController pushViewController:productInfo animated:YES];
//    }
//
//}

- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)moreBTN:(id)sender {
}

- (IBAction)cartBTN:(id)sender {
}
@end
