//
//  HomeTableVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealViewController.h"
#import "MenuVC.h"
@interface HomeTableVC : UITableViewController
@property (strong, nonatomic) IBOutlet UIBarButtonItem *menuButton;

@end
