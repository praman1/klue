//
//  ChangePasswordVC.m
//  Klue
//
//  Created by volivesolutions on 20/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ChangePasswordVC.h"
#import "PrefixHeader.pch"

@interface ChangePasswordVC ()
{
    NSString *tokenString;
    NSString *languageString;
}
@property (weak, nonatomic) IBOutlet UILabel *oldPasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTF;
@property (weak, nonatomic) IBOutlet UILabel *recentPasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *recentPasswordTF;
@property (weak, nonatomic) IBOutlet UILabel *conformPasswordLabel;
@property (weak, nonatomic) IBOutlet UITextField *conformPasswordTF;
@property (weak, nonatomic) IBOutlet UIScrollView *changePasswordScrollView;
- (IBAction)saveBTN_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn_Outlet;
- (IBAction)backBtn_Action:(id)sender;

@end

@implementation ChangePasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)customDesign
{
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Password"];
    _oldPasswordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Old Password"];
    _recentPasswordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"New Password"];
    _conformPasswordLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Conform Password"];
    [_saveBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Save"] forState:UIControlStateNormal];
}

-(void)changePasswordServicecall
{
        languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    //API-KEY:985869547,lang,user_id,old_pass,new_pass,con_pass
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/change_password

    NSMutableDictionary * changePasswordPostDictionary = [[NSMutableDictionary alloc]init];
    
    [changePasswordPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [changePasswordPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [changePasswordPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [changePasswordPostDictionary setObject:_oldPasswordTF.text forKey:@"old_pass"];
    [changePasswordPostDictionary setObject:_recentPasswordTF.text forKey:@"new_pass"];
    [changePasswordPostDictionary setObject:_conformPasswordTF.text forKey:@"con_pass"];
    // [loginPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
    
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"change_password" withPostDict:changePasswordPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Dat From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        [self.navigationController popViewControllerAnimated:TRUE];
                    }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

- (IBAction)saveBTN_Action:(id)sender {
    [self changePasswordServicecall];
}

//if ([_comForm isEqualToString:@"Menu"]) {
//    Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//    [self showViewController:home sender:self];
//}else {
//    [self.navigationController popViewControllerAnimated:YES];
//}
- (IBAction)backBtn_Action:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

@end
