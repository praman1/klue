//
//  OnboardingVC.m
//  Klue
//
//  Created by vishal bhatt on 15/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "OnboardingVC.h"
#import "PrefixHeader.pch"

@interface OnboardingVC ()
{
    UIAlertController *alertController;
    NSString *languageString;
}
@property (strong, nonatomic) IBOutlet UIButton *btnClose;
@property (strong, nonatomic) IBOutlet UIView *btnView;
@property (strong, nonatomic) IBOutlet UIButton *btnSignin;
@property (strong, nonatomic) IBOutlet UIButton *btnRegister;

@property (strong, nonatomic) IBOutlet UIView *pop;
- (IBAction)btnSelectcity:(id)sender;

- (IBAction)btnCloseMenu:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *changeLanguageBtn_Outlet;
- (IBAction)changeLanguageBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn_Outlet;
- (IBAction)signInBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *registerBtn_Outlet;
- (IBAction)registerBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *browseAsGuestBtn_Outlet;
-(IBAction)browseAsGuestBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *easiestWayLabel;

@property (weak, nonatomic) IBOutlet UIButton *selectCityBtn;

@end

@implementation OnboardingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //languageString = [NSString stringWithFormat:@"1"];
    
    //[[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
    
    [self customDesign];
   
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData) name:@"data" object:nil];
    // Do any additional setup after loading the view.

    
}

-(void)reloadData{
    
    [_selectCityBtn setTitle:[[NSUserDefaults standardUserDefaults]objectForKey:@"cityName"] forState:UIControlStateNormal];
   // [self.presentingViewController.presentingViewController dismissModalViewControllerAnimated:YES];

}

-(void)customDesign{
    
    
    [_changeLanguageBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"] forState:UIControlStateNormal];
    [_browseAsGuestBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Browse as Guest"] forState:UIControlStateNormal];
    [_signInBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign In"] forState:UIControlStateNormal];
    [_registerBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Register"] forState:UIControlStateNormal];
    [_registerBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Register"] forState:UIControlStateNormal];
    [_selectCityBtn setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Select City"] forState:UIControlStateNormal];
    _easiestWayLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Easiest way to Shop Online"];

//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
//    [self.view addGestureRecognizer:tap];

    _changeLanguageBtn_Outlet.layer.borderColor = [UIColor colorWithRed:180.0/255.0 green:155.0/255.0 blue:108.0/255.0 alpha:1.0].CGColor;
    
    _btnView.layer.borderColor = [UIColor colorWithRed:180.0/255.0 green:155.0/255.0 blue:108.0/255.0 alpha:1.0].CGColor;
    
    _btnSignin.layer.borderColor = [UIColor colorWithRed:180.0/255.0 green:155.0/255.0 blue:108.0/255.0 alpha:1.0].CGColor;
    
    _btnRegister.layer.borderColor = [UIColor colorWithRed:180.0/255.0 green:155.0/255.0 blue:108.0/255.0 alpha:1.0].CGColor;
}

//-(void)dismiss{
//    
//    [self dismissPopUpViewController];
//}

- (IBAction)btnSelectcity:(id)sender {
    
    CityListVC *cityList = [self.storyboard instantiateViewControllerWithIdentifier:@"CityListVC"];
    cityList.view.frame = CGRectMake(0, 0, 250.0f, 250.0f);
    //[cityList setModalPresentationStyle:UIModalPresentationOverCurrentContext];
    [self presentViewController:cityList animated:NO completion:nil];
    
 
}

- (IBAction)btnCloseMenu:(id)sender {
    [UIView animateWithDuration:0.25 delay:0 usingSpringWithDamping:0 initialSpringVelocity:0 options:UIViewAnimationOptionOverrideInheritedOptions animations:^{
        self.pop.transform = CGAffineTransformMakeScale(1.3, 1.3);
    } completion:^(BOOL finished) {
        self.pop.hidden = YES;
        self.btnClose.hidden = YES;
    }];
}

-(void)changeLanguage
{
    alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *english = [UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"en"];

        [[NSUserDefaults standardUserDefaults]setObject:languageString forKey:@"language"];
        //[[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
        [self customDesign];

    }];
    UIAlertAction *arabic = [UIAlertAction actionWithTitle:@"العربية" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"ar"];
        
        [[NSUserDefaults standardUserDefaults]setObject:languageString forKey:@"language"];
       // [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
        [self customDesign];


    }];


    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];

    [alertController addAction:english];
    [alertController addAction:arabic];
    [alertController addAction:cancel];

    [self presentViewController:alertController animated:YES completion:nil];

}

- (IBAction)changeLanguageBTN:(id)sender {
    
    [self changeLanguage];
    
}
- (IBAction)signInBTN:(id)sender {
    
    LoginVC *login =[self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
    [self.navigationController pushViewController:login animated:TRUE];
}

- (IBAction)registerBTN:(id)sender {
    
    RegisterVC *registerVC =[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    [self.navigationController pushViewController:registerVC animated:TRUE];
}

- (IBAction)browseAsGuestBTN:(id)sender {
    
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                             bundle: nil];
//    SWRevealViewController *reveal = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Side"];
//    [self.navigationController presentViewController:reveal animated:TRUE completion:nil];
    
    SWRevealViewController*reveal=[self.storyboard instantiateViewControllerWithIdentifier:@"Side"];
    
    [self presentViewController:reveal animated:YES completion:nil];
    
}
@end
