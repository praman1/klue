//
//  MenuVC.m
//  Klue
//
//  Created by vishal bhatt on 19/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "MenuVC.h"
#import "Home.h"
#import "SubCategoryTableVC.h"
#import "FlowerTableVC.h"
#import "MyOrdersVCTableViewController.h"
#import "AddAddressVC.h"
#import "WhishlistTableVC.h"
#import "PrefixHeader.pch"
@interface MenuVC ()
{
    MenuTableViewCell *cell1;
    Menu2TableViewCell *cell2;
    NSString *catIdString,*catNameString;
    AppDelegate *delegate;
    NSObject*objectUserDefaults;
    NSString *currencyString;
   
    
}
@property (weak, nonatomic) IBOutlet UIButton *homeBtn_Outlet;
- (IBAction)homeBtn_Action:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *myOrdersNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myAddressNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *myWhishListNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *homeNameLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

@implementation MenuVC {
    
    NSString *languageString;
    NSMutableArray *menuName;
    NSMutableArray *menuName2;
    NSMutableArray *categoryIdArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *catDataArrayCount;
   
    
}

- (void)viewDidLoad {
    [super viewDidLoad];

    [self menuServiceCall];
    _lineView.hidden = NO;
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    delegate = (AppDelegate*)[[UIApplication sharedApplication]delegate];
    // Do any additional setup after loading the view.
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.height/2;
  
    _myOrdersNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"My Orders"];
    _myAddressNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"My Addressses"];
    _myWhishListNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"My Whishlist"];
    _homeNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"];
    //[_homeBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Home"] forState:UIControlStateNormal];
    
    if ([languageString isEqualToString:@"ar"]) {
         menuName2 = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Help Center"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Logout"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Currency"], nil];
    }
    else{
        menuName2 = [[NSMutableArray alloc]initWithObjects:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Help Center"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Logout"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"],[[sharedclass sharedInstance]languageSelectedStringForKey:@"Currency"], nil];
    }
    
    currencyString = @"SAR";
    [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
 
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadData) name:@"refreshData" object:nil];
}



-(void)reloadData
{
    _userNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
    [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"]]] placeholderImage:[UIImage imageNamed:@"menu profile"]];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    //self.navigationController.navigationBar.hidden = NO;
    dispatch_async(dispatch_get_main_queue(), ^{
        _userNameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"userName"];
        [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[[NSUserDefaults standardUserDefaults]objectForKey:@"profileImage"]]] placeholderImage:[UIImage imageNamed:@""]];
        
        
        //[self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
        
        [UIView animateWithDuration:0 animations:^{
            
            _profileImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
            _profileImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
    });
}

-(void)viewWillDisappear:(BOOL)animated
{
    //self.navigationController.navigationBar.hidden = YES;
}



-(void)menuServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    // http://volivesolutions.com/Klue/services/banner_cat_products?API-KEY=985869547&lang=en&user_id=3
    NSMutableDictionary * menuPostDictionary = [[NSMutableDictionary alloc]init];
    
    [menuPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [menuPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [menuPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"banner_cat_products?" withPostDict:menuPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            catDataArrayCount = [NSMutableArray new];
            categoryIdArray = [NSMutableArray new];
            categoryNameArray = [NSMutableArray new];
            menuName = [NSMutableArray new];

            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
               
                catDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"categories"];
 
                if (catDataArrayCount.count > 0) {
                    for (int i = 0; catDataArrayCount.count > i; i++) {
                        [categoryIdArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cat_id"]];
                        [categoryNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cname"]];
                        [menuName addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cname"]];
                       
                    }
                }

                dispatch_async(dispatch_get_main_queue(), ^{
                    [_tableView reloadData];
                });
                
            });
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];
    
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView {
    
    return 2;
}

#pragma mark TableviewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return [menuName count];
    }else {
        return [menuName2 count];
    }
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    //UITableView *cell;
    
    
    if(indexPath.section == 0) {
        cell1 = [tableView dequeueReusableCellWithIdentifier:@"cell"];
        if (cell1 == nil) {
            cell1 = [[MenuTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
        }
        cell1.lblmenu.text = [menuName objectAtIndex:indexPath.row];
    
        return cell1;
    }else {
        cell2 = [tableView dequeueReusableCellWithIdentifier:@"cell1"];
        
//        if (indexPath.row == 0) {
//            cell2.seprate.hidden = NO;
//        }
        if (cell2 == nil) {
            cell2 = [[Menu2TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell1"];
        }
        cell2.lblmName.text = [menuName2 objectAtIndex:indexPath.row];
        
        return cell2;
    }

    //return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath;{
    NSLog(@"test");
    [tableView deselectRowAtIndexPath:indexPath animated:YES];

    
    //SWRevealViewController *revealController = self.revealViewController;
    if (indexPath.section == 0) {
        delegate.sideMenuCheck = @"fromSide";
        catIdString = [categoryIdArray objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults]setObject:catIdString forKey:@"catId"];
       
        catNameString = [categoryNameArray objectAtIndex:indexPath.row];
        [[NSUserDefaults standardUserDefaults]setObject:catNameString forKey:@"catName"];
        
        [self performSegueWithIdentifier:@"sideMenuPush" sender:self];
        

    }else{
        if (indexPath.row == 0) {
            
            [self performSegueWithIdentifier:@"pushToHelpCenter" sender:self];
            
        }
        else if (indexPath.row == 1){
            
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Logout"]
                                         message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Are you sure to Logout?"]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Yes"]
                                        style:UIAlertActionStyleDestructive
                                        handler:^(UIAlertAction * action) {
                                            
                                            
                                            UIViewController * gotoMainNav = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavVC"];
                                            [[UIApplication sharedApplication] delegate].window.rootViewController = gotoMainNav;
                                            [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                                            [[NSUserDefaults standardUserDefaults]removeObjectForKey:@"userId"];
                                            
                                            
                                        }];
            
            UIAlertAction* cancel = [UIAlertAction
                                     actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"]
                                     style:UIAlertActionStyleDefault
                                     handler:^(UIAlertAction * action) {
                                         //Handle no, thanks button
                                     }];
            
            
            
            //Add your buttons to alert controller
            [alert addAction:cancel];
            [alert addAction:yesButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            
        }
        else if (indexPath.row == 2){
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Language"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
            
            //***Small
            UIAlertAction *english = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"English"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"en" forKey:@"language"];
                
                [self viewDidLoad];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu" object:nil];
                
                // appDelegate.langStr =@"en";
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu1" object:nil];
                
                
            }];
            
            
            UIAlertAction *arabic = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"عربى"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                [[NSUserDefaults standardUserDefaults]setObject:@"ar" forKey:@"language"];
                [self viewDidLoad];
                
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu" object:nil];
                
                //appDelegate.langStr =@"ar";
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshMenu1" object:nil];
                
            }];
            //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:english];
            [alertController addAction:arabic];
            
            [alertController addAction:cancel];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
        else if (indexPath.row == 3){
            
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Choose Currency"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
            
            //***Small
            UIAlertAction *SAR = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"SAR"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                currencyString = @"SAR";
                [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"currencyUpdate" object:nil];
                
            }];
            
            
            UIAlertAction *KWD = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"KWD"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                currencyString = @"KWD";
                [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"currencyUpdate" object:nil];
                
            }];
            UIAlertAction *AED = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"AED"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                
                currencyString = @"AED";
                [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"currencyUpdate" object:nil];
                
            }];
            UIAlertAction *USD = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"USD"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                currencyString = @"USD";
                [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"currencyUpdate" object:nil];
                
                
            }];
            //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
            UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
            
            [alertController addAction:SAR];
            [alertController addAction:KWD];
            [alertController addAction:AED];
            [alertController addAction:USD];
            
            [alertController addAction:cancel];
            
            [self presentViewController:alertController animated:YES completion:nil];
            
        }

    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

- (IBAction)myOrders:(id)sender {
    
    if (objectUserDefaults != nil) {
        [self performSegueWithIdentifier:@"sw_PushToOrders" sender:self];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
//    SWRevealViewController *revealController = self.revealViewController;
//
//            MyOrdersVCTableViewController *myorder = [self.storyboard instantiateViewControllerWithIdentifier:@"MyOrdersVCTableViewController"];
//            myorder.comForm = @"Menu";
//            UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:myorder];
//            [revealController pushFrontViewController:nav animated:YES];
}

- (IBAction)myAddress:(id)sender {
    
    if (objectUserDefaults != nil) {
        [self performSegueWithIdentifier:@"sw_PushToAddress" sender:self];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
//    SWRevealViewController *revealController = self.revealViewController;
//
//    AddressListVC *addressList = [self.storyboard instantiateViewControllerWithIdentifier:@"AddressListVC"];
//    addressList.comForm = @"Menu";
//    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:addressList];
//    [revealController pushFrontViewController:nav animated:YES];
//
//    SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"Side"];                        self.navigationController.navigationBar.hidden = YES;
//    [self.navigationController pushViewController:reveal animated:TRUE];
    
    
    //[self.navigationController pushViewController:addressList animated:TRUE];
    
}

- (IBAction)mywhishlist:(id)sender {
    if (objectUserDefaults != nil) {
        [self performSegueWithIdentifier:@"sw_PushToWhishlist" sender:self];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
//    SWRevealViewController *revealController = self.revealViewController;
//
//    WhishlistTableVC *wish = [self.storyboard instantiateViewControllerWithIdentifier:@"WhishlistTableVC"];
//    wish.comForm = @"Menu";
//    self.tabBarController.selectedIndex = 3;
//    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:wish];
//    [revealController pushFrontViewController:nav animated:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)homeBtn_Action:(id)sender {
    
   // menuAppDelegate.selectTabItem = @"0";
    Home * home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self.navigationController pushViewController:home animated:YES];
}
@end
