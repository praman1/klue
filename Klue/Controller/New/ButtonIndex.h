//
//  ButtonIndex.h
//  Klue
//
//  Created by volivesolutions on 02/06/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ButtonIndex : UIButton

@property (nonatomic, strong) NSIndexPath* buttonIndexpath;
@property (nonatomic, strong) NSIndexPath* buttonParentIndexpath;
@property NSInteger buttonRow;
@end
