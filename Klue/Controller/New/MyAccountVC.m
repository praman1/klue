//
//  MyAccountVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "MyAccountVC.h"
#import "HomeTableVC.h"
#import "PrefixHeader.pch"
@interface MyAccountVC ()<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    UIImage *profileImage;
    UIImagePickerController *imagePicker;
    NSString *fileParameterConstant;
    NSString *languageString;
    NSObject*objectUserDefaults;
}
@property (weak, nonatomic) IBOutlet UIButton *profileImageAddBtn_Outleet;
@property (weak, nonatomic) IBOutlet UIScrollView *profileScrollview;

- (IBAction)profileImageAddBtn_Action:(id)sender;

- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailAddressLabel;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *emailIdTF;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UIButton *saveBtn_Outlet;
- (IBAction)saveBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordBtn_Outlet;
- (IBAction)changePasswordBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;

@end

@implementation MyAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageRefresh) name:@"refreshMenu1" object:nil];
    
    
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    if (objectUserDefaults != nil) {
        [self customDesign];
    }
    else
    {
        _profileScrollview.hidden = TRUE;
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

}

-(void)languageRefresh
{
    [self viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated
{

        [self viewDidLoad];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [UIView animateWithDuration:0 animations:^{
            
            _profileImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            [UIView animateWithDuration:0.4 animations:^{
                
                _profileImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
            }];
            
        }];
        
    });
    //self.navigationController.navigationBar.hidden = NO;
    //self.tabBarController.tabBar.hidden = FALSE;
}

//-(void)viewWillDisappear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = TRUE;
//}
-(void)customDesign
{
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBtn_Outlet setTarget: self.revealViewController];
        [_backBtn_Outlet setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    
    _profileImageView.layer.cornerRadius = _profileImageView.frame.size.width/2;
    _profileImageAddBtn_Outleet.layer.cornerRadius = _profileImageAddBtn_Outleet.frame.size.width/2;
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"My Profile"];
    _infoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"INFO"];
    _emailAddressLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"EMAIL ADDRESS"];
    _mobileNumberLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"MOBILE NUMBER"];
    [_saveBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Save"] forState:UIControlStateNormal];
    [_changePasswordBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Change Password"] forState:UIControlStateNormal];
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Categories"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Top Picks"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Whishlist"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:4] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Account"]];
    [self getProfileServiceCall];
    
}

-(void)getProfileServiceCall
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];

    // http://voliveafrica.com/togo/api/services/profile?API-KEY=225143&user_id=134721528
    NSMutableDictionary * profilePostDictionary = [[NSMutableDictionary alloc]init];
    
    [profilePostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [profilePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [profilePostDictionary setObject:[NSString stringWithFormat:@"%@",languageString] forKey:@"lang"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"getprofile?" withPostDict:profilePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _userNameLabel.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"fname"]];
                _emailIdLabel.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"email"]];
                _emailIdTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"] valueForKey:@"email"]];
                _mobileNumberTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"mobile"]];
                //NSString *basePath = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"base_path"]];
                NSString *image = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]valueForKey:@"image"]];
                
                [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,image]] placeholderImage:[UIImage imageNamed:@""]];
                
                [[NSUserDefaults standardUserDefaults]setObject:_userNameLabel.text forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults]setObject:image forKey:@"profileImage"];
                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"fname"] forKey:@"userName"];
                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"email"] forKey:@"emailId"];
                [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}







-(void)updateProfileServiceCall{
 
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

 [SVProgressHUD setForegroundColor:[UIColor colorWithRed:235.0/255.0 green:28.0/255.0 blue:36.0/255.0 alpha:1.0]];
 [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
 [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading Wait..."]];

 
    // API-KEY:985869547,lang,user_id,fname,email,mobile,profile_image

 
 //http://volivesolutions.com/Klue/services/update_profile

NSString *url = [NSString stringWithFormat:@"%@update_profile",Base_URL];


NSMutableDictionary * updateProfilePostDictionary = [[NSMutableDictionary alloc]init];


[updateProfilePostDictionary setObject:API_KEY forKey:@"API-KEY"];
[updateProfilePostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
[updateProfilePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
[updateProfilePostDictionary setObject:_userNameLabel.text forKey:@"fname"];
[updateProfilePostDictionary setObject:_emailIdTF.text forKey:@"email"];
[updateProfilePostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
// [updateProfilePostDictionary setObject:_profileImageView.image forKey:@"file"];

     fileParameterConstant = @"profile_image";

NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];

NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";

// string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ

[request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
[request setHTTPShouldHandleCookies:NO];
[request setTimeoutInterval:30];
[request setHTTPMethod:@"POST"];

// set Content-Type in HTTP header
NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
[request setValue:contentType forHTTPHeaderField: @"Content-Type"];

// post body
NSMutableData *body = [NSMutableData data];

// add params (all params are strings)
for (NSString *param in updateProfilePostDictionary) {
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"%@\r\n", [updateProfilePostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
}
_profileImageView.image = [self scaleImage:profileImage toSize:CGSizeMake(200.0,200.0)];


//For Image Uploading
NSData *imageData = UIImageJPEGRepresentation(profileImage,1.0);
if (imageData) {
    [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"; filename=\"image.jpg\"\r\n", fileParameterConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: image/jpeg\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:imageData];
    [body appendData:[[NSString stringWithFormat:@"\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"--%@--\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPBody:body];
}
// set the content-length
NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
[request setValue:postLength forHTTPHeaderField:@"Content-Length"];

NSURLSession *session = [NSURLSession sharedSession];
NSLog(@"The request is %@",request);

[[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
    
    NSDictionary *statusDict = [[NSDictionary alloc]init];
    statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
    NSLog(@"Images from server %@", statusDict);
    //
    NSString * status  = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
    _profileImageView.image = profileImage;
    
    // message1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
    
    if ([status isEqualToString:@"1"])
    {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
            [_profileImageView sd_setImageWithURL:[NSURL URLWithString:[statusDict objectForKey:@"image"]] placeholderImage:[UIImage imageNamed:@"Coffee Shop"]];
            [[NSUserDefaults standardUserDefaults]setObject:_emailIdTF.text forKey:@"emailId"];
            [[NSUserDefaults standardUserDefaults]setObject:_userNameLabel.text forKey:@"userName"];
            [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
            
            
            
        });
        
        [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:nil];
        
        
        [self getProfileServiceCall];
    }
    else
    {
        [SVProgressHUD dismiss];
        [[sharedclass sharedInstance] showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:nil];
        
    }
    
}]
 
 resume];


}



- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingImage:(UIImage* )image editingInfo:(NSDictionary *)editingInfo {
    
    if (picker == imagePicker) {
        
        profileImage = image;
        
        _profileImageView.image=profileImage;
        //[self networkCheck];
        
    }
    [picker dismissViewControllerAnimated:YES completion:nil];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(UIImage *)scaleImage:(UIImage* )image toSize:(CGSize)newSize {
    
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}




- (IBAction)profileImageAddBtn_Action:(id)sender {
    
    UIAlertController * view=   [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Pick the image"] message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"From"] preferredStyle:UIAlertControllerStyleActionSheet];
    
    UIAlertAction* PhotoLibrary = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Photo Library"]
                                    style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                                       
                                       imagePicker = [[UIImagePickerController alloc] init];
                                       imagePicker.delegate = self;
                                       [imagePicker setSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
                                       [self presentViewController:imagePicker animated:YES completion:NULL];
                                       [view dismissViewControllerAnimated:YES completion:nil];    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:^(UIAlertAction*  action) {  }];
    
    UIAlertAction *camera = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Camera"] style:UIAlertActionStyleDefault handler:^(UIAlertAction*  action) {
        
        imagePicker = [[UIImagePickerController alloc] init];
        imagePicker.delegate = self;
        [imagePicker setSourceType:UIImagePickerControllerSourceTypeCamera];
        [self presentViewController:imagePicker animated:YES completion:NULL];
        [view dismissViewControllerAnimated:YES completion:nil];
        
    }];
    
    [view addAction:PhotoLibrary];
    [view addAction:camera];
    [view addAction:cancel];
    [self presentViewController:view animated:YES completion:nil];
}

- (IBAction)btnBack:(id)sender {
    Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
    [self showViewController:home sender:self];
    self.tabBarController.selectedIndex = 0;
}
- (IBAction)saveBtn_Action:(id)sender {
    
    [self updateProfileServiceCall];
    
    
}
- (IBAction)changePasswordBtn_Action:(id)sender {
    ChangePasswordVC *change = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangePasswordVC"];
    [self.navigationController pushViewController:change animated:TRUE];
}
@end
