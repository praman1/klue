//
//  ShippingMethodsViewController.h
//  Klue
//
//  Created by volivesolutions on 07/06/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ShippingMethodsViewController : UIViewController
@property NSString *shippingString;
@property NSString *total;
//@property NSData *imageData;
@property NSString *messageTextString,*notetextString,*imageData,*cityNameString;

@end
