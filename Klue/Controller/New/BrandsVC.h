//
//  BrandsVC.h
//  Klue
//
//  Created by volivesolutions on 25/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandsVC : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *brandTV;

@end
