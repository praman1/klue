//
//  SuccessfulVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuccessfulVC : UIViewController
@property NSString *totalAmountString,*orderIdString,*paymentTypeString,*transactionDateString;

@end
