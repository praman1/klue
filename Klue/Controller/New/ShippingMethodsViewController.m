//
//  ShippingMethodsViewController.m
//  Klue
//
//  Created by volivesolutions on 07/06/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ShippingMethodsViewController.h"
#import <FSCalendar/FSCalendar.h>

@interface ShippingMethodsViewController ()<FSCalendarDelegate,FSCalendarDelegateAppearance>
{
    NSString *languageString;
    NSString *currencyString;
    NSString *weekdayString;
    NSString *beforeAndAfterTimeString;
    
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;
- (IBAction)backBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *deliveryPriceInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *deliveryDateInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *beforeTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *afterTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *beforeTimeCheckBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *afterTimeCheckBtn_Outlet;
- (IBAction)continueBtn_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *continueBtn_Outlet;

- (IBAction)beforeTimeBtn_Action:(id)sender;
- (IBAction)afterTimeBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *deliveryPriceValueLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLbl;

@property (strong, nonatomic) IBOutlet FSCalendar *deliverCalender;

@property NSString * dateStr;
@property (strong, nonatomic) NSDateFormatter *dateFormatter5;

@end

@implementation ShippingMethodsViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    [self shippingMethodsServicecall];
    
    // _deliverCalender.allowsMultipleSelection=YES;
   
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   
    [[sharedclass sharedInstance]shadowEffects:_continueBtn_Outlet];
    _deliveryPriceInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Delivery Price:"];
    _deliveryDateInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Delivery Date:"];
    _beforeTimeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Delivery Before 2pm"];
    _afterTimeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Delivery After 3pm"];
    [_continueBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Continue"] forState:UIControlStateNormal];
  
    // Do any additional setup after loading the view.
}
    
-(void)viewWillAppear:(BOOL)animated
    {
       
        //self.navigationController.navigationBar.hidden = NO;
    }
    
-(void)viewWillDisappear:(BOOL)animated
    {
        //self.navigationController.navigationBar.hidden = YES;
        
    }
#pragma  mark ShippingMethodsServicecall
-(void)shippingMethodsServicecall
{
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/shipping_methods
    
    // API-KEY:985869547,lang:en,currency_type,user_id
    NSMutableDictionary * shippingMethodsPostDictionary = [[NSMutableDictionary alloc]init];
    
    [shippingMethodsPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [shippingMethodsPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [shippingMethodsPostDictionary setObject:[NSString stringWithFormat:@"%@",currencyString ? currencyString :@""] forKey:@"currency_type"];
   // [shippingMethodsPostDictionary setObject:@"SAR" forKey:@"currency_type"];
    
    [shippingMethodsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    NSLog(@"dict is %@",shippingMethodsPostDictionary);
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"shipping_methods" withPostDict:shippingMethodsPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Order Details Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                   

//                    "delivery_price": "150SAR",
//                    "delivery_date": "2018-06-14"
//                    if ([currencyString isEqualToString:@"SAR"]) {
//                        _deliveryPriceValueLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"delivery_price"]];
//                    }
//                    else if ([currencyString isEqualToString:@"KWD"]) {
//                        _deliveryPriceValueLabel.text = [NSString stringWithFormat:@"%@ KWD",[[dict objectForKey:@"data"] objectForKey:@"delivery_price"]];
//                    }
//                    else if ([currencyString isEqualToString:@"AED"]) {
//                        _deliveryPriceValueLabel.text = [NSString stringWithFormat:@"%@ AED",[[dict objectForKey:@"data"] objectForKey:@"delivery_price"]];
//                    }
//                    else if ([currencyString isEqualToString:@"USD"]) {
//                        _deliveryPriceValueLabel.text = [NSString stringWithFormat:@"%@ USD",[[dict objectForKey:@"data"] objectForKey:@"delivery_price"]];
//                    }
  
                   
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
        _deliveryPriceValueLabel.text = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"delivery_price"]];
        _dateStr = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"delivery_date"]];
        //weekdayString = [NSString stringWithFormat:@"%@",[[dict objectForKey:@"data"] objectForKey:@"delivery_day"]];
                        
                        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
                        [dateFormat setDateFormat:@"yyyy-MM-dd"];
                        NSDate *date = [dateFormat dateFromString:_dateStr];
                       
                        NSCalendar * calendar1 = [NSCalendar currentCalendar];
                        NSDateComponents * dateComponents = [calendar1 components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
                        
                       // NSLog(@"Day in week: %ld", (long)dateComponents.weekday); // Day in week: 3
                       //NSLog(@"Day in month: %ld", (long)dateComponents.day);    // Day in month: 25
                        NSLog(@"Day name: %@", calendar1.shortWeekdaySymbols[dateComponents.weekday - 1]); // Day name: Tue (iOS 8.0 +)
                        weekdayString = calendar1.shortWeekdaySymbols[dateComponents.weekday - 1];
                        
                        if ([weekdayString isEqualToString:@"Fri"])
                        {
                            _afterTimeLabel.hidden = TRUE;
                            _afterTimeCheckBtn_Outlet.hidden = TRUE;
                        }else{
                            _afterTimeLabel.hidden = FALSE;
                            _afterTimeCheckBtn_Outlet.hidden = FALSE;
                        }
                        
                        _dateLbl.text = _dateStr;
                        [_deliverCalender reloadData];
                        
                        [SVProgressHUD dismiss];
                    });
                   
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}



- (IBAction)continueBtn_Action:(id)sender {
    if (beforeAndAfterTimeString.length > 0) {
        PyamentMethodVC *payment = [self.storyboard instantiateViewControllerWithIdentifier:@"PyamentMethodVC"];
        //payment.shippingMethodIdStr = [NSString stringWithFormat:@"%@",[shippingMethodIdArray objectAtIndex:indexPath.row]];
        payment.deliveryTimeString = [NSString stringWithFormat:@"%@",beforeAndAfterTimeString];
        payment.deliveryDateString = [NSString stringWithFormat:@"%@",_dateLbl.text];
        payment.orderTotal = [NSString stringWithFormat:@"%@",_total];
        payment.imageData = _imageData;
        payment.messageString = _messageTextString;
        payment.noteString = _notetextString;
        payment.shippingAddressIdStr = [NSString stringWithFormat:@"%@",_shippingString];
        payment.cityNameStr = _cityNameString;
        [self.navigationController pushViewController:payment animated:TRUE];
    }
    else{
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:@"Please select delivery time"] onViewController:self completion:nil];
    }
    
    
}

- (IBAction)beforeTimeBtn_Action:(id)sender {
    
    beforeAndAfterTimeString = @"2pm";
    [_beforeTimeCheckBtn_Outlet setImage:[UIImage imageNamed:@"radio_check"] forState:UIControlStateNormal];
    [_afterTimeCheckBtn_Outlet setImage:[UIImage imageNamed:@"radio_uncheck"] forState:UIControlStateNormal];
    
    
}

- (IBAction)afterTimeBtn_Action:(id)sender {
    beforeAndAfterTimeString = @"3pm";
    [_afterTimeCheckBtn_Outlet setImage:[UIImage imageNamed:@"radio_check"] forState:UIControlStateNormal];
    [_beforeTimeCheckBtn_Outlet setImage:[UIImage imageNamed:@"radio_uncheck"] forState:UIControlStateNormal];
    
}

- (IBAction)backBtn_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (BOOL)calendar:(FSCalendar *)calendar shouldSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition
{
    
    BOOL shouldSelect = _dateStr;
    
    
    return shouldSelect;
}

- (void)calendar:(FSCalendar *)calendar didSelectDate:(NSDate *)date atMonthPosition:(FSCalendarMonthPosition)monthPosition;
{
   

    _dateStr = [self.dateFormatter5 stringFromDate:date];
    _dateLbl.text = _dateStr;
    
    NSCalendar * calendar1 = [NSCalendar currentCalendar];
    NSDateComponents * dateComponents = [calendar1 components: NSCalendarUnitDay | NSCalendarUnitWeekday fromDate: date];
    
    //NSLog(@"Day in week: %ld", (long)dateComponents.weekday); // Day in week: 3
    //NSLog(@"Day in month: %ld", (long)dateComponents.day);    // Day in month: 25
    NSLog(@"Day name: %@", calendar1.shortWeekdaySymbols[dateComponents.weekday - 1]); // Day name: Tue (iOS 8.0 +)
    weekdayString = calendar1.shortWeekdaySymbols[dateComponents.weekday - 1];
    if ([weekdayString isEqualToString:@"Fri"])
    {
        _afterTimeLabel.hidden = TRUE;
        _afterTimeCheckBtn_Outlet.hidden = TRUE;
    }
    else{
        _afterTimeLabel.hidden = FALSE;
        _afterTimeCheckBtn_Outlet.hidden = FALSE;
    }
    
}


- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        
        self.dateFormatter5 = [[NSDateFormatter alloc] init];
        self.dateFormatter5.dateFormat = @"yyyy-MM-dd";
        
    }
    return self;
}

- (NSInteger)calendar:(FSCalendar *)calendar numberOfEventsForDate:(NSDate *)date
{
    if ([_dateStr containsString:[self.dateFormatter5 stringFromDate:date]]) {
    
        return 1;
    
    }
    return 0;
}
- (NSArray<UIColor *> *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance eventSelectionColorsForDate:(nonnull NSDate *)date
{
  
    //calendar.gregorian.component(.Weekday, fromDate: date) == 4 ? UIColor.blueColor() : UIColor.blackColor()
    
   
    
    if ([_dateStr containsString:[self.dateFormatter5 stringFromDate:date]]) {
        
        return @[[UIColor redColor]];
    }
    return nil;
}
- (nullable UIColor *)calendar:(FSCalendar *)calendar appearance:(FSCalendarAppearance *)appearance fillDefaultColorForDate:(NSDate *)date{
    
   if ([_dateStr containsString:[self.dateFormatter5 stringFromDate:date]]) {
        return [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    }
    return nil;
}



-(NSDate *)minimumDateForCalendar:(FSCalendar *)calendar
{
        NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
        [dateFormat setDateFormat:@"yyyy-MM-dd"];
        NSDate *date = [dateFormat dateFromString:_dateStr];
    
    return date;
    
}



//- (NSDate *)maximumDateForCalendar:(FSCalendar *)calendar
//{
//    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
//    [dateFormat setDateFormat:@"yyyy-MM-dd"];
//    NSDate *date = [dateFormat dateFromString:_dateStr];
//
//    return date;
//
//}
//_calendar.appearance.weekdayTextColor = [UIColor redColor];
//_calendar.appearance.headerTitleColor = [UIColor redColor];
//_calendar.appearance.eventColor = [UIColor greenColor];
//_calendar.appearance.selectionColor = [UIColor blueColor];
//_calendar.appearance.todayColor = [UIColor orangeColor];
//_calendar.appearance.todaySelectionColor = [UIColor blackColor];


@end
