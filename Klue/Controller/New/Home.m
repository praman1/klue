//
//  Home.m
//  Klue
//
//  Created by volive solutions on 05/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "Home.h"
#import "PrefixHeader.pch"

@interface Home ()<UICollectionViewDelegate,UICollectionViewDataSource,UIScrollViewDelegate>
{
    CollectionViewCell *cell;
    NSString *whishlistString;
    NSString *languageString;
    NSMutableArray *testArray;
    NSString *currencyString;
    
    NSMutableArray *catDataArrayCount;
    NSMutableArray *bannerDataArrayCount;
    NSMutableArray *adsDataArrayCount;
    NSMutableArray *productDataArrayCount;
    
    NSMutableArray *categoryIdArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *categoryImageArray;
    
    NSMutableArray *bannerImageArray;
    NSMutableArray *bannerLinkArray;
    
    NSMutableArray *adImageArray;
    NSMutableArray *adsLinkArray;
    
    NSMutableArray *isWhishlistCheckArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productImageArray;
    NSMutableArray *productNameArray;
    
    NSMutableArray *productRegular_price_sarArray;
    NSMutableArray *productPrice_sarArray;
    
    NSMutableArray *productRegular_price_usdArray;
    NSMutableArray *productPrice_usdArray;
    
    NSMutableArray *productRegular_price_kwdArray;
    NSMutableArray *productPrice_kwdArray;
    
    NSMutableArray *productRegular_price_aedArray;
    NSMutableArray *productPrice_aedArray;

    NSMutableArray *productBrandNameArray ,* finalArr;
    NSObject*objectUserDefaults;

    
   
    
    int z,y;
}
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHeight;

@property (weak, nonatomic) IBOutlet UICollectionView *bannerCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *adsCollectionView;
@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectioview;
@property (weak, nonatomic) IBOutlet UICollectionView *arrivalCollectionview;
@property (weak, nonatomic) IBOutlet UILabel *arrivalLabel;
- (IBAction)viewAllBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn_Outlet;
@property (weak, nonatomic) IBOutlet UISearchBar *searchbar;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *slideMenuBtn;
- (IBAction)cartBtn_Action:(id)sender;

@property UIImageView *bannerImageView1;
@property UIImageView *downImageView;

@end

@implementation Home

- (void)viewDidLoad {
    [super viewDidLoad];

    [self myCustomDesign];
    objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(currencyUpdate) name:@"currencyUpdate" object:nil];
    finalArr = [NSMutableArray new];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageRefresh) name:@"refreshMenu1" object:nil];

    // Do any additional setup after loading the view.
}
-(void)languageRefresh
{
    [self viewDidLoad];
}

-(void)currencyUpdate
{
    [self viewDidLoad];
}

-(void)myCustomDesign{
    
    testArray = [NSMutableArray new];
    [[sharedclass sharedInstance]shadowEffects:_viewAllBtn_Outlet];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_slideMenuBtn setTarget: self.revealViewController];
        [_slideMenuBtn setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
        //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
        
    }
    [revealViewController panGestureRecognizer];
    [revealViewController tapGestureRecognizer];
    
    _viewAllBtn_Outlet.layer.cornerRadius = 4;
    _searchbar.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Search for products"];
    [_viewAllBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"VIEW ALL"] forState:UIControlStateNormal];
    _arrivalLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"New Arrival"];
    UIImage *img = [UIImage imageNamed:@"logoooooo.png"];
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 150, 25)];
    [imgView setImage:img];
    // setContent mode aspect fit
    [imgView setContentMode:UIViewContentModeScaleAspectFit];
    
    self.navigationItem.titleView = imgView;
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Categories"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Top Picks"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Whishlist"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:4] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Account"]];
    
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    
    [self homeServiceCall];
}

-(void)viewWillAppear:(BOOL)animated
{
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    [self homeServiceCall];
}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    //self.navigationController.navigationBar.hidden = YES;
//    self.tabBarController.tabBar.hidden = TRUE;
//}

-(void)homeServiceCall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    // http://volivesolutions.com/Klue/services/banner_cat_products?API-KEY=985869547&lang=en&user_id=3
    NSMutableDictionary * homePostDictionary = [[NSMutableDictionary alloc]init];
    
    [homePostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [homePostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [homePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"banner_cat_products?" withPostDict:homePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
  
            catDataArrayCount = [NSMutableArray new];
            bannerDataArrayCount = [NSMutableArray new];
            adsDataArrayCount = [NSMutableArray new];
            productDataArrayCount = [NSMutableArray new];
            
            categoryIdArray = [NSMutableArray new];
            categoryNameArray = [NSMutableArray new];
            categoryImageArray = [NSMutableArray new];
            
            bannerImageArray = [NSMutableArray new];
            bannerLinkArray = [NSMutableArray new];
            
            adImageArray = [NSMutableArray new];
            
            productIdArray = [NSMutableArray new];
            productImageArray = [NSMutableArray new];
            productNameArray = [NSMutableArray new];
            productBrandNameArray = [NSMutableArray new];
            isWhishlistCheckArray = [NSMutableArray new];
            
            productRegular_price_sarArray = [NSMutableArray new];
            productPrice_sarArray = [NSMutableArray new];
            
            productRegular_price_usdArray = [NSMutableArray new];
            productPrice_usdArray = [NSMutableArray new];
            
            productRegular_price_kwdArray = [NSMutableArray new];
            productPrice_kwdArray = [NSMutableArray new];
            
            productRegular_price_aedArray = [NSMutableArray new];
            productPrice_aedArray = [NSMutableArray new];
           
            
            
           // dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];

                catDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"categories"];
                bannerDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"banner"];
                adsDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"ads"];
                productDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"products"];
                
               // dispatch_async(dispatch_get_main_queue(), ^{
                    
                    if (catDataArrayCount.count > 0) {
                        for (int i = 0; catDataArrayCount.count > i; i++) {
                            [categoryIdArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cat_id"]];
                            [categoryNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cname"]];
                            [ categoryImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cat_image"]];
                        }
                        
                    }
               // });
                

               // dispatch_async(dispatch_get_main_queue(), ^{
                
                    if (bannerDataArrayCount.count > 0) {
                        for (int i = 0; bannerDataArrayCount.count > i; i++) {
                            [bannerImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"banner"]objectAtIndex:i] objectForKey:@"banner_image"]];
                            [bannerLinkArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"banner"]objectAtIndex:i] objectForKey:@"banner_link"]];
                        }
                    }
              //  });
                
             //  dispatch_async(dispatch_get_main_queue(), ^{
                if (adsDataArrayCount.count > 0) {
                    for (int i = 0; adsDataArrayCount.count > i; i++) {
                        [adImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"ads"]objectAtIndex:i] objectForKey:@"image"]];
                        //[adsLinkArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"banner"]objectAtIndex:i] objectForKey:@"banner_link"]];
                    }
                }
               // });
                
               // dispatch_async(dispatch_get_main_queue(), ^{
                    if (productDataArrayCount.count > 0) {
                        for (int i = 0; productDataArrayCount.count > i; i++) {
                            [productIdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"prod_id"]];
                            [productNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"pname"]];
                            [productBrandNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"brand_name"]];
                            [productImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"prod_image"]];
                            
                            [productPrice_sarArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            [productRegular_price_sarArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
                            
                            [ productPrice_usdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            [productRegular_price_usdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            [productRegular_price_kwdArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"price_aed"]];
                            [ productRegular_price_aedArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
                            
                            [isWhishlistCheckArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"products"]objectAtIndex:i] objectForKey:@"is_wish_list"]];
                        }
                    }
            if (productDataArrayCount.count>0) {
                self.collectionHeight.constant=456;
                self.scroll.scrollEnabled = YES;
               
            }else{
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.collectionHeight.constant=0;
                    self.scroll.scrollEnabled = NO;
                });
               
               
            }

                 dispatch_async(dispatch_get_main_queue(), ^{
                     [_bannerCollectionView reloadData];
                     [_adsCollectionView reloadData];
                     [_arrivalCollectionview reloadData];
                     [_categoriesCollectioview reloadData];
                     });
                
          
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];
    
}


#pragma mark CollectionViewDelegateMethods
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if(collectionView == _categoriesCollectioview)
    {
        
        CGFloat width = (CGFloat) (_categoriesCollectioview.frame.size.width/4);
        
        return CGSizeMake(width-10,112);
    }else if (collectionView == _arrivalCollectionview)
    {
        CGFloat width = (CGFloat)(_arrivalCollectionview.frame.size.width/2);
        return CGSizeMake(width-8, 225);
    }
    else if (collectionView == _bannerCollectionView)
    {
        CGFloat width = (CGFloat)(_bannerCollectionView.frame.size.width/1);
        return CGSizeMake(width-2, 135);
    }
    else if (collectionView == _adsCollectionView)
    {
        CGFloat width = (CGFloat)(_adsCollectionView.frame.size.width/1);
        return CGSizeMake(width-2, 135);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _categoriesCollectioview) {
 
        return categoryNameArray.count + 1 ;
        
    }
    else if (collectionView == _arrivalCollectionview){
    
        return productNameArray.count;
    }
    else if (collectionView == _bannerCollectionView)
    {
        return bannerImageArray.count;
    }
    else if (collectionView == _adsCollectionView)
    {
        return adImageArray.count;
    }
    else{
        return 0;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    
    if (collectionView == _categoriesCollectioview) {
        
        if (indexPath.row == 0) {

        CollectionViewCell *cell = [_categoriesCollectioview dequeueReusableCellWithReuseIdentifier:@"categoryCell1" forIndexPath:indexPath];
            cell.categoryNameLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"All Categories"];

             return cell;

        }else{
        
            CollectionViewCell * cell1 = [_categoriesCollectioview dequeueReusableCellWithReuseIdentifier:@"categoryCell" forIndexPath:indexPath];
            
            cell1.categoryImg.layer.cornerRadius = cell1.categoryImg.frame.size.width/2;
            
            cell1.categoryNameLbl.text = [categoryNameArray objectAtIndex:indexPath.row - 1 ];
            [cell1.categoryImg sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[categoryImageArray objectAtIndex:indexPath.row - 1 ]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
            cell1.userInteractionEnabled = YES;
            
             return cell1;
        }
       
        
    }
    
    else if(collectionView == _arrivalCollectionview){
        
        CollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"arrivalCell" forIndexPath:indexPath];
        
        [[sharedclass sharedInstance]shadowEffects:cell.cellView];
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
        cell.brandNameLabel.text = [productBrandNameArray objectAtIndex:indexPath.row];
        
        
        NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                      NSStrikethroughColorAttributeName:[UIColor blackColor]};
        NSAttributedString * attr;
        if ([currencyString isEqualToString:@"SAR"]) {
             attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",[productRegular_price_sarArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
        }
        
       else if ([currencyString isEqualToString:@"KWD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",[productRegular_price_kwdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
        }
       else if ([currencyString isEqualToString:@"AED"]) {
           attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",[productRegular_price_aedArray objectAtIndex:indexPath.row]] attributes:attribtues];
           cell.originalAmountLabel.attributedText = attr;
           cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
       }
       else if ([currencyString isEqualToString:@"USD"]) {
           attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",[productRegular_price_usdArray objectAtIndex:indexPath.row]] attributes:attribtues];
           cell.originalAmountLabel.attributedText = attr;
           cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
       }
        
        
        //cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
        [cell.favouriteBtn addTarget:self action:@selector(addOrRemoveWhishlist:) forControlEvents:UIControlEventTouchUpInside];
        whishlistString = [NSString stringWithFormat:@"%@",[isWhishlistCheckArray objectAtIndex:indexPath.row]];
        
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            
        }
        
        return cell;
        
    }
    else if (collectionView == _bannerCollectionView)
    {
        CollectionViewCell *bannerCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"bannerCell" forIndexPath:indexPath];
        //bannerCell.bannerImageView.image = [UIImage imageNamed:@"flower_img1.png"];
        [bannerCell.bannerImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[bannerImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];

        return bannerCell;
    }
    else if (collectionView == _adsCollectionView)
    {
        CollectionViewCell *adsCell = [collectionView dequeueReusableCellWithReuseIdentifier:@"adsCell" forIndexPath:indexPath];
       // adsCell.adImageView.image = [UIImage imageNamed:@"flower_img1.png"];
       [adsCell.adImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[adImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];

        return adsCell;
    }
    
    return cell;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
        //CollectionViewCell *cell =(CollectionViewCell*) [_categoriesCollectioview cellForItemAtIndexPath:indexPath];

     if (collectionView == _bannerCollectionView)
    {
        WebViewVC *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewVC"];
        webVC.webViewlink =[NSString stringWithFormat:@"%@",[bannerLinkArray objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:webVC animated:TRUE];
    }
    else if(collectionView == _arrivalCollectionview){
       
        ProductScreenVC *productInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductScreenVC"];
        productInfo.productIdString = [NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]];
        
        [self.navigationController pushViewController:productInfo animated:YES];
   
    }
     else if(collectionView == _categoriesCollectioview)
    {
        if(indexPath.row == 0)
        {
            CategoryTableVC *products = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryTableVC"];
             self.tabBarController.selectedIndex = 1;
            [self.navigationController pushViewController:products animated:TRUE];
        }
        else
        {
            NSString *checkcatIdStr = [categoryIdArray objectAtIndex:indexPath.row - 1];
            if ([checkcatIdStr isEqualToString:@"6"]) {
                NSLog(@"SubCatId Is %@",checkcatIdStr);
                OccasionsCatListVC *subCatList = [self.storyboard instantiateViewControllerWithIdentifier:@"OccasionsCatListVC"];
                subCatList.checkStr=@"BACK";
                subCatList.catIdString = [categoryIdArray objectAtIndex:indexPath.row - 1];
                subCatList.catNameString = [categoryNameArray objectAtIndex:indexPath.row-1];
                [self.navigationController pushViewController:subCatList animated:TRUE];
            }
            else{
                SubCategoryListVC *subCatList = [self.storyboard instantiateViewControllerWithIdentifier:@"SubCategoryListVC"];
                subCatList.checkStr=@"BACK";
                subCatList.catIdString = [categoryIdArray objectAtIndex:indexPath.row - 1];
                subCatList.catNameString = [categoryNameArray objectAtIndex:indexPath.row-1];
                [self.navigationController pushViewController:subCatList animated:TRUE];
            }
            
            
        }
       
    }

}

-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = (CollectionViewCell*)[_arrivalCollectionview cellForItemAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.cellView.layer.borderWidth = 2;
}

-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[_arrivalCollectionview cellForItemAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.cellView.layer.borderWidth = 2;
}


#pragma mark AddOrRemoveWhishlist

-(void)addOrRemoveWhishlist:(id)sender
{
    if (objectUserDefaults != nil) {
        CGPoint position = [sender convertPoint:CGPointZero toView:_arrivalCollectionview];
        NSIndexPath *indexPath = [_arrivalCollectionview indexPathForItemAtPoint:position];
        
        CollectionViewCell *cell = (CollectionViewCell*)[_arrivalCollectionview cellForItemAtIndexPath:indexPath];
        whishlistString = [NSString stringWithFormat:@"%@",[isWhishlistCheckArray objectAtIndex:indexPath.row]];
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            [self addWhishlistServicecall:sender];
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            [self removeWhishlistServicecall:sender];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
    
    }
}
#pragma  mark AddWhishlistServicecall
-(void)addWhishlistServicecall:(id)sender
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    //    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id
    CGPoint position = [sender convertPoint:CGPointZero toView:_arrivalCollectionview];
    NSIndexPath *indexPath = [_arrivalCollectionview indexPathForItemAtPoint:position];
    
    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_wishlist" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];
                    
                    
                    [self homeServiceCall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark RemoveWhishlistServicecall
-(void)removeWhishlistServicecall:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    //    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,prod_id
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_arrivalCollectionview];
    NSIndexPath *indexPath = [_arrivalCollectionview indexPathForItemAtPoint:position];
    
    NSMutableDictionary * removeWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [removeWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [removeWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:removeWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    // [SVProgressHUD dismiss];
                    
                    [self homeServiceCall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


- (IBAction)viewAllBTN:(id)sender {

    ProductsList *list = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductsList"];
    [self.navigationController pushViewController:list animated:TRUE];

    
}
- (IBAction)cartBtn_Action:(id)sender {
    if (objectUserDefaults != nil) {
        ShoppingCartVC *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartVC"];
        [self.navigationController pushViewController:cart animated:TRUE];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
            message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
   
}
@end
