//
//  OrderDetailVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "OrderDetailVC.h"
#import "PrefixHeader.pch"

@interface OrderDetailVC ()
{
    NSString *languageString;
    NSString *addressString;
    TableViewCell *cell;
    
    NSMutableArray *dataCountArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productCountArray;
    NSMutableArray *brandNameArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    NSMutableArray *originalPriceArray;
    NSMutableArray *quantityArray;
    
}

@property (strong, nonatomic) IBOutlet NSLayoutConstraint *detailTVHeight;

@property (strong, nonatomic) IBOutlet UILabel *CODChargesInfoLabel;
@property (strong, nonatomic) IBOutlet UILabel *CODChargesValueLabel;


- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *placedOnLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingAddressInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderDeliveryInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *paymentInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *productPriceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingChargesInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *shippingChargesValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *netPriceInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *netPriceValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardPaymentInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *creditCardValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderDetailsTableView;

@end

@implementation OrderDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customDesign];
    
 
    

    // Do any additional setup after loading the view.
}

-(void)customDesign
{
    _paymentInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Payment Information"];
    _shippingAddressInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Shipping Address"];
    _productPriceInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Product Price"];
    _shippingChargesInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Shipping Charges"];
    _netPriceInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Net Price"];
    _orderIdLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order ID"],_orderIdString];
    _placedOnLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On"],_placedDateString];
    [self orderDetailsServicecall];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order Details"];
}

#pragma  mark GetWhishlistServicecall
-(void)orderDetailsServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/order_details
    
   // API-KEY:985869547,lang:en,user_id,order_id
    NSMutableDictionary * myOrdersPostDictionary = [[NSMutableDictionary alloc]init];
    
    [myOrdersPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [myOrdersPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
     [myOrdersPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
   // [myOrdersPostDictionary setObject:@"12" forKey:@"user_id"];
    //[myOrdersPostDictionary setObject:[NSString stringWithFormat:@"%@",_productIdString ? _productIdString:@""] forKey:@"prod_id"];
    [myOrdersPostDictionary setObject:[NSString stringWithFormat:@"%@",_orderIdString ? _orderIdString :@""] forKey:@"order_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"order_details" withPostDict:myOrdersPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Order Details Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    
                    dataCountArray = [NSMutableArray new];
                    
                    productIdArray = [NSMutableArray new];
                    productCountArray = [NSMutableArray new];
                    brandNameArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    originalPriceArray = [NSMutableArray new];
                    quantityArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
                    
                    if (dataCountArray.count > 0) {
                        for (int i =0; dataCountArray.count > i; i++) {
                            

                            [productIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_name"]];
                            [productImageArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [brandNameArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                            [originalPriceArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                            [quantityArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"product_qty"]];
                            //[productCountArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]];
                            
                        }
   
                    }
                    if (dataCountArray.count < 2) {
                        _detailTVHeight.constant = 182;
                    }
                    else{
                        _detailTVHeight.constant = 312;
                    }
                   // _placedOnLabel.text =[NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Placed On"],[[dict objectForKey:@"data"]objectForKey:@"order_date"]];
                    //_orderIdLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order ID"],[[dict objectForKey:@"data"]objectForKey:@"order_id"]];
                    
                    
 
                   // _orderDeliveryInfoLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order wil be deliver by"],[[dict objectForKey:@"data"]objectForKey:@"estimate_date"]];
                    
                    _userNameLabel.text = [[dict objectForKey:@"shipping_details"]objectForKey:@"username"];
                    addressString = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@",[[dict objectForKey:@"shipping_details"]objectForKey:@"username"],[[dict objectForKey:@"shipping_details"]objectForKey:@"email"],[[dict objectForKey:@"shipping_details"]objectForKey:@"mobile"],[[dict objectForKey:@"shipping_details"]objectForKey:@"address"],[[dict objectForKey:@"shipping_details"]objectForKey:@"city"],[[dict objectForKey:@"shipping_details"]objectForKey:@"landmark"]];
                    
                    _addressLabel.text = [NSString stringWithFormat:@"%@",addressString];
                    _productPriceValueLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"sub_total"],_currencyType];
                    _shippingChargesValueLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"delivery_price"],_currencyType];
                    _CODChargesValueLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"cod_price"],_currencyType];
                    _netPriceValueLabel.text = [NSString stringWithFormat:@"%@ %@",[dict objectForKey:@"total"],_currencyType];
                    _creditCardValueLabel.text = [NSString stringWithFormat:@"%@ SAR",[dict objectForKey:@"total"]];
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_orderDetailsTableView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark TableviewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return dataCountArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_orderDetailsTableView dequeueReusableCellWithIdentifier:@"tableViewCell"];
    
    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"tableViewCell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.itemView];
    
    [cell.itemImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]] completed:nil];
    cell.itemNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
    cell.itemBrandLabel.text = [brandNameArray objectAtIndex:indexPath.row];
    cell.itemPriceLabel.text = [NSString stringWithFormat:@"%@ %@",[originalPriceArray objectAtIndex:indexPath.row],_currencyType];
    cell.itemQuantityLabel.text = [NSString stringWithFormat:@"X%@",[quantityArray objectAtIndex:indexPath.row]];
    cell.itemDeliveryDateLabel.text = [NSString stringWithFormat:@"%@:%@",[[sharedclass sharedInstance]languageSelectedStringForKey:@"Order wil be deliver by"],_estimatedDateString];
    cell.itemOrderStatusLabel.text = [NSString stringWithFormat:@"%@",_statusString];

    //cell.addressLabel.text = [NSString stringWithFormat:@"%@",[finalAddressArray objectAtIndex:indexPath.row]];
    
    return cell;
}


- (IBAction)btnBack:(id)sender {
        [self.navigationController popViewControllerAnimated:YES];
}
@end
