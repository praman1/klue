//
//  MoreVC.h
//  Klue
//
//  Created by vishal bhatt on 19/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MoreVC : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *homeBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *myOrdersBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *whishListBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *notificationbtn_Outlet;

@end
