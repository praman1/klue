//
//  OccasionsCatListVC.m
//  Klue
//
//  Created by volivesolutions on 13/06/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "OccasionsCatListVC.h"

@interface OccasionsCatListVC ()
{
    NSMutableArray *subCatDataArrayCount;
    NSMutableArray *subCatIdArray;
    NSMutableArray *subCatNameArray;
     NSString *languageString;
    TableViewCell *cell;
}
- (IBAction)backBtn_Action:(id)sender;
@property (strong, nonatomic) IBOutlet UITableView *occasionsCatListTableview;

@end

@implementation OccasionsCatListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:_catNameString];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self subCatListServicecall];
    // Do any additional setup after loading the view.
}

-(void)subCatListServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    // http://volivesolutions.com/Klue/services/sub_categories?API-KEY=985869547&lang=en&cat_id=7
    NSMutableDictionary * subCatPostDictionary = [[NSMutableDictionary alloc]init];
    
    [subCatPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [subCatPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [subCatPostDictionary setObject:[NSString stringWithFormat:@"%@",_catIdString ? _catIdString :@""] forKey:@"cat_id"];
  
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"sub_categories?" withPostDict:subCatPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            subCatNameArray = [NSMutableArray new];
            subCatIdArray = [NSMutableArray new];
           
            subCatDataArrayCount = [NSMutableArray new];
            
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                subCatDataArrayCount = [dataDictionary objectForKey:@"data"];
                
                if (subCatDataArrayCount.count > 0) {
                    for (int i = 0; subCatDataArrayCount.count > i; i++) {
                        [subCatIdArray addObject: [[[dataDictionary objectForKey:@"data"]objectAtIndex:i] objectForKey:@"sub_cat_id"]];
                        [subCatNameArray addObject: [[[dataDictionary objectForKey:@"data"]objectAtIndex:i] objectForKey:@"sub_cat_name"]];
                    }
                }
                dispatch_async(dispatch_get_main_queue(), ^{
                    [_occasionsCatListTableview reloadData];
                    
                });
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
        
    }];
    
    
}



-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return subCatNameArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_occasionsCatListTableview dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.cellView];
    
    cell.catNameLabel.text = [subCatNameArray objectAtIndex:indexPath.row];
    
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    SubCategoryListVC *subCatList = [self.storyboard instantiateViewControllerWithIdentifier:@"SubCategoryListVC"];
    subCatList.checkStr=@"BACK";
    subCatList.catIdString = [subCatIdArray objectAtIndex:indexPath.row];
    subCatList.catNameString = [subCatNameArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:subCatList animated:TRUE];
    //[_occasionsCatListTableview deselectRowAtIndexPath:indexPath animated:TRUE];
}

-(void)tableView:(UITableView *)tableView didHighlightRowAtIndexPath:(NSIndexPath *)indexPath{
    
    TableViewCell *cell = (TableViewCell*)[_occasionsCatListTableview cellForRowAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.cellView.layer.borderWidth = 2;
}

-(void)tableView:(UITableView *)tableView didUnhighlightRowAtIndexPath:(NSIndexPath *)indexPath
{
    TableViewCell *cell = (TableViewCell*)[_occasionsCatListTableview cellForRowAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.cellView.layer.borderWidth = 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 120;
}

- (IBAction)backBtn_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}
@end
