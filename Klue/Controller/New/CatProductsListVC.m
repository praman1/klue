//
//  CatProductsListVC.m
//  Klue
//
//  Created by volivesolutions on 12/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "CatProductsListVC.h"
#import "PrefixHeader.pch"

@interface CatProductsListVC ()<UICollectionViewDelegate,UICollectionViewDataSource>
{
    CollectionViewCell *cell;
    NSString *languageString;
    NSString *currencyString;
    
    NSMutableArray *dataCountArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productBrandNameArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    
    NSMutableArray *productRegular_price_sarArray;
    NSMutableArray *productPrice_sarArray;
    
    NSMutableArray *productRegular_price_usdArray;
    NSMutableArray *productPrice_usdArray;
    
    NSMutableArray *productRegular_price_kwdArray;
    NSMutableArray *productPrice_kwdArray;
    
    NSMutableArray *productRegular_price_aedArray;
    NSMutableArray *productPrice_aedArray;
    
    NSMutableArray *isWhistListCheckArray;
    NSMutableArray *sortCheckingArr;
    NSString *whishlistString;
    NSDictionary *passedDataDict;
    NSObject*objectUserDefaults;
   
    
    BOOL isFav;
}

@property (weak, nonatomic) IBOutlet UICollectionView *catCollectionView;
- (IBAction)backBTN:(id)sender;
- (IBAction)cartBTN:(id)sender;
- (IBAction)moreBTN:(id)sender;
- (IBAction)sortBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *sortBtn_Outlet;
- (IBAction)filtersBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *filtersBtn_Outlet;


@end

@implementation CatProductsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_filtersBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Filters"] forState:UIControlStateNormal];
    [_sortBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sort"] forState:UIControlStateNormal];
     objectUserDefaults = [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"];
    isFav = NO;
    UIBezierPath *maskPath1 = [UIBezierPath bezierPathWithRoundedRect:_filtersBtn_Outlet.bounds byRoundingCorners:( UIRectCornerBottomLeft  | UIRectCornerTopLeft ) cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer1 = [[CAShapeLayer alloc] init];
    maskLayer1.frame = self.view.bounds;
    maskLayer1.path  = maskPath1.CGPath;
    
    _filtersBtn_Outlet.layer.mask = maskLayer1;
    
    UIBezierPath *maskPath2 = [UIBezierPath bezierPathWithRoundedRect:_sortBtn_Outlet.bounds byRoundingCorners:( UIRectCornerBottomRight |UIRectCornerTopRight ) cornerRadii:CGSizeMake(6.0, 6.0)];
    
    CAShapeLayer *maskLayer2 = [[CAShapeLayer alloc] init];
    maskLayer2.frame = self.view.bounds;
    maskLayer2.path  = maskPath2.CGPath;
    
    _sortBtn_Outlet.layer.mask = maskLayer2;
    [self subCatProductsServicecall];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:_subCatNameString];
    sortCheckingArr=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0", nil];
    [[NSUserDefaults standardUserDefaults]setObject:_subCatIdString forKey:@"subCatId"];
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];

    [[NSNotificationCenter defaultCenter]
     addObserver:self selector:@selector(triggerAction:) name:@"filterData" object:nil];
    
}

-(void) triggerAction:(NSNotification*)notification
{
    
        passedDataDict = notification.userInfo;
    
        NSLog (@"Successfully received test notification! %@", passedDataDict);
   
    
    productIdArray = [NSMutableArray new];
    productNameArray = [NSMutableArray new];
    productImageArray = [NSMutableArray new];
    productBrandNameArray = [NSMutableArray new];
    
    productRegular_price_sarArray = [NSMutableArray new];
    productPrice_sarArray = [NSMutableArray new];
    
    productRegular_price_usdArray = [NSMutableArray new];
    productPrice_usdArray = [NSMutableArray new];
    
    productRegular_price_kwdArray = [NSMutableArray new];
    productPrice_kwdArray = [NSMutableArray new];
    
    productRegular_price_aedArray = [NSMutableArray new];
    productPrice_aedArray = [NSMutableArray new];
    
    isWhistListCheckArray = [NSMutableArray new];
    dataCountArray = [NSMutableArray new];
    
    dataCountArray = [passedDataDict objectForKey:@"data"];
    
    if (dataCountArray.count > 0) {
        for (int i = 0; dataCountArray.count > i; i++) {
            [productIdArray addObject:[[[passedDataDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
            [productNameArray addObject:[[[passedDataDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
            [productImageArray addObject:[[[passedDataDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
            [productBrandNameArray addObject:[[[passedDataDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
            
            [productPrice_sarArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
            [productRegular_price_sarArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
            
            [productPrice_usdArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
            [productRegular_price_usdArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
            
            [productPrice_kwdArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
            [productRegular_price_kwdArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
            
            [productPrice_aedArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];
            [ productRegular_price_aedArray addObject: [[[passedDataDict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
            
            [isWhistListCheckArray addObject:[[[passedDataDict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"is_wish_list"]];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [_catCollectionView reloadData];
    });
    
}

#pragma mark SubCatProductsServicecall
-(void)subCatProductsServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
   // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/products_list

    //API-KEY=985869547,lang,sub_cat_id,user_id

    NSMutableDictionary * subCatProductsPostDictionary = [[NSMutableDictionary alloc]init];
    
    [subCatProductsPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [subCatProductsPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [subCatProductsPostDictionary setObject:_subCatIdString forKey:@"sub_cat_id"];
    [subCatProductsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
 

    [[sharedclass sharedInstance]urlPerameterforPost:@"products_list" withPostDict:subCatProductsPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    productIdArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    productBrandNameArray = [NSMutableArray new];
                    
                    productRegular_price_sarArray = [NSMutableArray new];
                    productPrice_sarArray = [NSMutableArray new];
                    
                    productRegular_price_usdArray = [NSMutableArray new];
                    productPrice_usdArray = [NSMutableArray new];
                    
                    productRegular_price_kwdArray = [NSMutableArray new];
                    productPrice_kwdArray = [NSMutableArray new];
                    
                    productRegular_price_aedArray = [NSMutableArray new];
                    productPrice_aedArray = [NSMutableArray new];
                    
                    isWhistListCheckArray = [NSMutableArray new];
                    dataCountArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
     
                    if (dataCountArray.count > 0) {
                        for (int i = 0; dataCountArray.count > i; i++) {
                            [productIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
                            [productImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [productBrandNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                            
                            [productPrice_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            [productRegular_price_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            [productRegular_price_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            [productRegular_price_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];
                            [ productRegular_price_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
                            
                            [isWhistListCheckArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"is_wish_list"]];
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_catCollectionView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark - Table view data source
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _catCollectionView)
    {
        CGFloat width = (CGFloat)(_catCollectionView.frame.size.width/2);
        return CGSizeMake(width-5, 220);
    }
    return CGSizeZero;
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return productNameArray.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    if (collectionView == _catCollectionView) {
        cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
 
        [[sharedclass sharedInstance]shadowEffects:cell.cellView];
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
        cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
        cell.brandNameLabel.text = [productBrandNameArray objectAtIndex:indexPath.row];
       
        
        NSDictionary * attribtues = @{NSStrikethroughStyleAttributeName:@(NSUnderlineStyleSingle),
                                      NSStrikethroughColorAttributeName:[UIColor blackColor]};
        NSAttributedString * attr;
        if ([currencyString isEqualToString:@"SAR"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ SAR",[productRegular_price_sarArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
        }
        
        else if ([currencyString isEqualToString:@"KWD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ KWD",[productRegular_price_kwdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"AED"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ AED",[productRegular_price_aedArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
        }
        else if ([currencyString isEqualToString:@"USD"]) {
            attr = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@ USD",[productRegular_price_usdArray objectAtIndex:indexPath.row]] attributes:attribtues];
            cell.originalAmountLabel.attributedText = attr;
            cell.discountAmountLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
        }
        
        [cell.favouriteBtn addTarget:self action:@selector(addOrRemoveWhishlist:) forControlEvents:UIControlEventTouchUpInside];
        whishlistString = [NSString stringWithFormat:@"%@",[isWhistListCheckArray objectAtIndex:indexPath.row]];
        
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
           
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            
        }

        return cell;
        
    }
    
    return NULL;
    
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if(collectionView == _catCollectionView){

        ProductScreenVC *productInfo = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductScreenVC"];

        productInfo.productIdString = [productIdArray objectAtIndex:indexPath.row];
        [self.navigationController pushViewController:productInfo animated:YES];
    }

}

-(void)collectionView:(UICollectionView *)collectionView didHighlightItemAtIndexPath:(NSIndexPath *)indexPath{
    
    CollectionViewCell *cell = (CollectionViewCell*)[_catCollectionView cellForItemAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0] CGColor];
    cell.cellView.layer.borderWidth = 2;
}

-(void)collectionView:(UICollectionView *)collectionView didUnhighlightItemAtIndexPath:(NSIndexPath *)indexPath
{
    CollectionViewCell *cell = (CollectionViewCell*)[_catCollectionView cellForItemAtIndexPath:indexPath];
    cell.cellView.layer.borderColor = [[UIColor clearColor] CGColor];
    cell.cellView.layer.borderWidth = 2;
}

-(void)addOrRemoveWhishlist:(id)sender
{
    
    if (objectUserDefaults != nil) {
        CGPoint position = [sender convertPoint:CGPointZero toView:_catCollectionView];
        NSIndexPath *indexPath = [_catCollectionView indexPathForItemAtPoint:position];
        CollectionViewCell *cell = (CollectionViewCell*)[_catCollectionView cellForItemAtIndexPath:indexPath];
        
        whishlistString = [NSString stringWithFormat:@"%@",[isWhistListCheckArray objectAtIndex:indexPath.row]];
        if ([whishlistString isEqualToString:@"0"]) {
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistTrue"] forState:UIControlStateNormal];
            [self addWhishlistServicecall:sender];
        }
        else{
            [cell.favouriteBtn setImage:[UIImage imageNamed:@"whishlistFalse"] forState:UIControlStateNormal];
            [self removeWhishlistServicecall:sender];
        }
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Guest Login Alert!"]
                                        
                                                                           message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"You are a guest,please login to use this feature"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 [SVProgressHUD dismiss];
                                                                 LoginVC* login = [self.storyboard instantiateViewControllerWithIdentifier:@"LoginVC"];
                                                                 [self presentViewController:login animated:TRUE completion:nil];
                                                             }];
            UIAlertAction *cancelButton = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleDefault
                                                                 handler:nil];
            
            [alert addAction:okButton];
            [alert addAction:cancelButton];
            
            [self presentViewController:alert animated:YES completion:nil];
        });
        
    }
    
    
}

#pragma  mark AddWhishlistServicecall
-(void)addWhishlistServicecall:(id)sender
{
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/add_wishlist
    //API-KEY:985869547,lang,user_id,prod_id
    CGPoint position = [sender convertPoint:CGPointZero toView:_catCollectionView];
    NSIndexPath *indexPath = [_catCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * addWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [addWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"add_wishlist" withPostDict:addWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    //[SVProgressHUD dismiss];
                    
                    
                    [self subCatProductsServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


#pragma mark RemoveWhishlistServicecall
-(void)removeWhishlistServicecall:(id)sender
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
//    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
//    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
//    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    //http://volivesolutions.com/Klue/services/delete_favourite
    //API-KEY:985869547,lang,user_id,prod_id
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_catCollectionView];
    NSIndexPath *indexPath = [_catCollectionView indexPathForItemAtPoint:position];
    
    NSMutableDictionary * removeWhishlistPostDictionary = [[NSMutableDictionary alloc]init];
    
    [removeWhishlistPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [removeWhishlistPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [removeWhishlistPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_favourite" withPostDict:removeWhishlistPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                   // [SVProgressHUD dismiss];
                    
                    [self subCatProductsServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
           // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark SortServicecall
-(void)sortServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/sort_products
    //"API-KEY:985869547,lang:en ,sub_cat_id,low_price,high_price,new_arrival,user_id"
    
    
    
    NSMutableDictionary * sortPostDictionary = [[NSMutableDictionary alloc]init];
    
    [sortPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [sortPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [sortPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] ? [[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] :@"" forKey:@"user_id"];
    [sortPostDictionary setObject:_subCatIdString ? _subCatIdString :@"" forKey:@"sub_cat_id"];
    [sortPostDictionary setObject:[sortCheckingArr objectAtIndex:0] forKey:@"low_price"];
    [sortPostDictionary setObject:[sortCheckingArr objectAtIndex:1] forKey:@"high_price"];
    [sortPostDictionary setObject:[sortCheckingArr objectAtIndex:2] forKey:@"new_arrival"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"sort_products" withPostDict:sortPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Sort Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    productIdArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    productBrandNameArray = [NSMutableArray new];
                    
                    productRegular_price_sarArray = [NSMutableArray new];
                    productPrice_sarArray = [NSMutableArray new];
                    
                    productRegular_price_usdArray = [NSMutableArray new];
                    productPrice_usdArray = [NSMutableArray new];
                    
                    productRegular_price_kwdArray = [NSMutableArray new];
                    productPrice_kwdArray = [NSMutableArray new];
                    
                    productRegular_price_aedArray = [NSMutableArray new];
                    productPrice_aedArray = [NSMutableArray new];
                    
                    isWhistListCheckArray = [NSMutableArray new];
                    dataCountArray = [NSMutableArray new];
                    
                    dataCountArray = [dict objectForKey:@"data"];
                    
                    if (dataCountArray.count > 0) {
                        for (int i = 0; dataCountArray.count > i; i++) {
                            [productIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
                            [productImageArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [productBrandNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                           
                            [productPrice_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            [productRegular_price_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            [productRegular_price_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                            [productRegular_price_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_kwd"]];
                            
                            [productPrice_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];
                            [ productRegular_price_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"regular_price_aed"]];
                            
                            [isWhistListCheckArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"is_wish_list"]];
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_catCollectionView reloadData];
                    });
                    
                    
                    // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(void)sortClicked
{
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sort"] message:@"Select an option to sort" preferredStyle:UIAlertControllerStyleActionSheet];
    
    
    
    UIAlertAction *priceLH = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Price Low to High"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [sortCheckingArr replaceObjectAtIndex:0 withObject:@"1"];
                [sortCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
                [sortCheckingArr replaceObjectAtIndex:2 withObject:@"0"];
 
        [self sortServicecall];
        
    }];
    UIAlertAction *priceHL = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Price High to Low"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [sortCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
        [sortCheckingArr replaceObjectAtIndex:1 withObject:@"1"];
        [sortCheckingArr replaceObjectAtIndex:2 withObject:@"0"];

        [self sortServicecall];
        
        
    }];
    UIAlertAction *newArrival = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"New Arrivals"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [sortCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
        [sortCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
        [sortCheckingArr replaceObjectAtIndex:2 withObject:@"1"];
 
        [self sortServicecall];
        
        
    }];
    
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
    
    [alertController addAction:priceHL];
    [alertController addAction:priceLH];
    [alertController addAction:newArrival];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (IBAction)backBTN:(id)sender {
    
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)cartBTN:(id)sender {
    
    ShoppingCartVC *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartVC"];
    [self.navigationController pushViewController:cart animated:TRUE];
}

- (IBAction)moreBTN:(id)sender {
}

- (IBAction)sortBtn_Action:(id)sender {
    
    [self sortClicked];
    
}

- (IBAction)filtersBtn_Action:(id)sender {
    FilterVC *filters = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
   // filters.subcatIdStr = _subCatIdString;
   // [self.navigationController pushViewController:filters animated:TRUE];
    [self presentViewController:filters animated:TRUE completion:nil];
}
@end
