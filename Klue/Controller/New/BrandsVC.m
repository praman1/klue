//
//  BrandsVC.m
//  Klue
//
//  Created by volivesolutions on 25/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "BrandsVC.h"
#import "BrandCell.h"

@interface BrandsVC ()<UITableViewDelegate,UITableViewDataSource>
{
    BrandCell * brandCell;
    NSMutableArray *brandDataCountArray;
    NSMutableArray *brandNameArray;
    NSMutableArray *brandIdArray;
    NSString *languageString;
    
    NSMutableArray * ischeckArr;
    
    NSMutableArray * idsArr;
    
    
}
- (IBAction)backBtn_Action:(id)sender;
- (IBAction)closeBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn_Outlet;
- (IBAction)doneBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *doneBtn_Outlet;

@end

@implementation BrandsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self brandNameServicecall];
    
    idsArr = [NSMutableArray new];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [_doneBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Done"] forState:UIControlStateNormal];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Choose Brands"];
    
    // Do any additional setup after loading the view.
}

-(void)brandNameServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
        [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
        [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
        [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
        [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/brands_list
    //API-KEY:985869547l,ang:en
    
    
    
    NSMutableDictionary * brandListPostDictionary = [[NSMutableDictionary alloc]init];
    
    [brandListPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [brandListPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"brands_list" withPostDict:brandListPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Brands list Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                     [SVProgressHUD dismiss];
                    brandNameArray = [NSMutableArray new];
                    brandIdArray = [NSMutableArray new];
                    brandDataCountArray = [NSMutableArray new];
                    ischeckArr = [NSMutableArray new];
                    
                    
                    brandDataCountArray = [dict objectForKey:@"data"];
                    
                    if (brandDataCountArray.count > 0) {
                        for (int i = 0; brandDataCountArray.count > i; i++) {
                    
                        [brandNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]valueForKey:@"name"]];
                        [brandIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]valueForKey:@"id"]];
                            
                            [ischeckArr addObject:@"0"];
                           
                        }
                                }
                        dispatch_async(dispatch_get_main_queue(), ^{
                                [_brandTV reloadData];
                        });
                    
                    // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
             [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return brandNameArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    

    brandCell = [_brandTV dequeueReusableCellWithIdentifier:@"BrandCell"];
    if (brandCell == nil) {
        brandCell = [[BrandCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"BrandCell"];
    }
    brandCell.brandNameLbl.text = [brandNameArray objectAtIndex:indexPath.row];
    
    NSString * ischeck = [ischeckArr objectAtIndex:indexPath.row];
    NSString * selectStr = [brandIdArray objectAtIndex:indexPath.row];
    
    if ([ischeck isEqualToString:@"0"]) {
        
        brandCell.brandImg.image = [UIImage imageNamed:@"checkbox"];
   
    }else
    {
        brandCell.brandImg.image = [UIImage imageNamed:@"checkbox_checked"];
        
      
        
        [idsArr addObject:selectStr];
        
        NSString * finalStr = [idsArr componentsJoinedByString:@","];
        
        NSLog(@"my ids are %@",finalStr);
        [[NSUserDefaults standardUserDefaults]setObject:finalStr forKey:@"brandId"];
    }
    
    return brandCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
   NSString * ischeck = [ischeckArr objectAtIndex:indexPath.row];
    
    if ([ischeck isEqualToString:@"0"]) {
        
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"1"];
        idsArr = [NSMutableArray new];
        
    }else
    {
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"0"];
        idsArr = [NSMutableArray new];
    }

    
    [self.brandTV reloadData];
}

- (IBAction)backBtn_Action:(id)sender {
    [self.navigationController popViewControllerAnimated:TRUE];
}

- (IBAction)closeBtn_Action:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
- (IBAction)doneBtn_Action:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}
@end
