//
//  AddAddressVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "AddAddressVC.h"
#import "PrefixHeader.pch"

@interface AddAddressVC ()
{
    NSString *languageString;
    //NSString *shippingAddressId;
   
}
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBtn_Outlet;

- (IBAction)btnBack:(id)sender;

@property (weak, nonatomic) IBOutlet UIScrollView *addressScrollView;

@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *nameTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *addressTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *houseNoTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *landMarkTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *emailIdTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *mobileNumberTF;
@property (strong, nonatomic) IBOutlet SkyFloatingLabelTextField *cityTF;

@property (weak, nonatomic) IBOutlet UIButton *saveAddressBtn_Outlet;
- (IBAction)saveAddressBTN:(id)sender;
@property (strong, nonatomic) IBOutlet UILabel *cityLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseOrFlatNoLabel;
@property (weak, nonatomic) IBOutlet UILabel *landmarkLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailIdLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileNumberLabel;
@property (weak, nonatomic) IBOutlet UILabel *saveDeliveryLocationLabel;


@end

@implementation AddAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([_addressCheckString isEqualToString:@"fromAddressList"]) {
        _nameTF.text =     _nameString;
        _emailIdTF.text = _emailIdString;
        _mobileNumberTF.text = _mobileNumberString;
        _addressTF.text = _addressString;
        //_houseNoTF.text = _houseNoString;
        _cityTF.text = _cityNameString;
        _landMarkTF.text = _landMarkString;
    }
   

    
    //[self addAddressServicecall];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
}

#pragma mark Textfield Delegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _addressTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-100) animated:YES];
        
    }else if (textField == _houseNoTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _cityTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _landMarkTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }else if (textField == _emailIdTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }else if (textField == _mobileNumberTF) {
        
        [_addressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_nameTF resignFirstResponder];
    [_addressTF resignFirstResponder];
    [_houseNoTF resignFirstResponder];
    [_landMarkTF resignFirstResponder];
    [_emailIdTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    
    
    [_addressScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTF) {
        
        [_addressTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _addressTF) {
        
        [_houseNoTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _houseNoTF) {
        
        [_landMarkTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _landMarkTF) {
        
        [_emailIdTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailIdTF) {
        
        [_mobileNumberTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _mobileNumberTF) {
        
        [_mobileNumberTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}


- (IBAction)btnBack:(id)sender {
    if ([_comForm isEqualToString:@"Menu"]) {
        SWRevealViewController *revealViewController = self.revealViewController;
        if ( revealViewController )
        {
            
            [_backBtn_Outlet setTarget: self.revealViewController];
            [_backBtn_Outlet setAction: @selector( revealToggle: )];
            [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
            //revealViewController.rearViewRevealOverdraw= self.view.frame.size.width-10;
            
        }
        [revealViewController panGestureRecognizer];
        [revealViewController tapGestureRecognizer];
//        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
//        [self showViewController:home sender:self];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

-(void)addAddressServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    _cityNameString = [[NSUserDefaults standardUserDefaults]objectForKey:@"cityName"];
    

    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/shipping_addr

//    "API-KEY:985869547,lang:en,user_id,username,
//    email,address,city,shipping_id,landmark,mobile"
    
    NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addAddressPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];

    [addAddressPostDictionary setObject:_nameTF.text forKey:@"username"];
    [addAddressPostDictionary setObject:_addressTF.text forKey:@"address"];
    [addAddressPostDictionary setObject:_houseNoTF.text forKey:@"flat_no"];
    [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
    [addAddressPostDictionary setObject:_landMarkTF.text forKey:@"landmark"];
    [addAddressPostDictionary setObject:_emailIdTF.text forKey:@"email"];
    [addAddressPostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
 
    if (_shippingAddressId.length == 0) {
        
        [addAddressPostDictionary setObject:@"" forKey:@"shipping_id"];
    }else
    {
        [addAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",_shippingAddressId] forKey:@"shipping_id"];
    }

    [[sharedclass sharedInstance]urlPerameterforPost:@"shipping_addr" withPostDict:addAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Add Address Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    _shippingAddressId = [NSString stringWithFormat:@"%@",[dict objectForKey:@"shipping_id"]];
                    
//                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:^{
//                        [self.navigationController popViewControllerAnimated:TRUE];
//                    }];
                    UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                        
                        [self.navigationController popViewControllerAnimated:TRUE];
                       
                    }];
                    [alert addAction:ok];
                    [self presentViewController:alert animated:YES completion:nil];
                    

                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

- (IBAction)saveAddressBTN:(id)sender {
    
    [self addAddressServicecall];
}
@end
