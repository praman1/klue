//
//  SplashVC.m
//  Klue
//
//  Created by vishal bhatt on 15/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "SplashVC.h"
#import "PrefixHeader.pch"

@interface SplashVC (){
    NSString *languagString;
    NSString *currencyString;
    
}
@property (weak, nonatomic) IBOutlet UIButton *skipTheIntroBtn_Outlet;

@end

@implementation SplashVC

- (void)viewDidLoad {
    [super viewDidLoad];
    currencyString = [NSString stringWithFormat:@"SAR"];
    [[NSUserDefaults standardUserDefaults]setObject:currencyString forKey:@"currency"];
    NSLog(@"Currency String is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"currency"]);
    
    languagString = [NSString stringWithFormat:@"ar"];
    [[NSUserDefaults standardUserDefaults]setObject:languagString forKey:@"language"];
    NSLog(@"Language String is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"language"]);
    
    [_skipTheIntroBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"SKIP THE INTRO"] forState:UIControlStateNormal];
    
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] != NULL )
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                    bundle: nil];
                           SWRevealViewController *home = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Side"];
                          // self.window.rootViewController = home;
                           [self presentViewController:home animated:YES completion:nil];
                       });
        
    }

    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    if ([[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] != NULL )
    {
        dispatch_async(dispatch_get_main_queue(), ^
                       {
                           UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                                    bundle: nil];
                           SWRevealViewController *home = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Side"];
                           // self.window.rootViewController = home;
                           [self presentViewController:home animated:YES completion:nil];
                       });
        
    }
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)skipTheIntroBTN:(id)sender {
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
