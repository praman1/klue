//
//  OrderSummaryVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "OrderSummaryVC.h"

@interface OrderSummaryVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *addressDataArrayCount;
    NSMutableArray *finalAddressArray;
    NSMutableArray *shippingAddressIdArray;
    NSString *addressStr;
    NSString *languageString;
    NSString *shippingIdString;
    NSString *currencyString;
    NSString *cityNameString;
    NSString *usernameString;
    NSString *emailIdString;
    NSString *mobileNoString;
    NSMutableArray *userNameArray;
    TableViewCell *cell;
    
    NSMutableArray *nameArray;
    NSMutableArray *emailIdArray;
    NSMutableArray *mobileNumberArray;
    NSMutableArray *addressArray;
    NSMutableArray *houseNoArray;
    NSMutableArray *landMarkArray;
    NSMutableArray *cityNameArray;
    NSMutableArray * ischeckArr;
    BOOL ischecked;
}

@property (strong, nonatomic) IBOutlet UIView *offerView;
@property (strong, nonatomic) IBOutlet UIView *card1;
@property (strong, nonatomic) IBOutlet UIView *card2;
@property (strong, nonatomic) IBOutlet UIView *card3;
- (IBAction)btnBack:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *selectDeliveryLabel;
@property (weak, nonatomic) IBOutlet UILabel *offersLabel;
- (IBAction)addNewAddressBTN_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *addNewAddressBtn_Outlet;
- (IBAction)applyBTN_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *applyBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTotalLabel;
- (IBAction)proceedToPayBTN_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *proceedToPayBtn_Outlet;
@property (weak, nonatomic) IBOutlet UILabel *promoCodeValueLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTotalValueLabel;
@property (weak, nonatomic) IBOutlet UITableView *orderSummeryTableView;



@end

@implementation OrderSummaryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    ischecked = false;
    // Do any additional setup after loading the view.
    currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order Summery"];
    _selectDeliveryLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Select Delivery Address"];
    _offersLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Offers"];
    _promoCodeLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Promo Code"];
    _orderTotalLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Order Total"];
    [_addNewAddressBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Add New Address"] forState:UIControlStateNormal];
    [_applyBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Apply"] forState:UIControlStateNormal];
    [_proceedToPayBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Proceed To Pay"] forState:UIControlStateNormal];
    
   // if ([currencyString isEqualToString:@"SAR"]) {
        _orderTotalValueLabel.text = [NSString stringWithFormat:@"%@",_subTotalString];
   // }
//    else if ([currencyString isEqualToString:@"KWD"]) {
//        _orderTotalValueLabel.text = [NSString stringWithFormat:@"%@ KWD",_subTotalString];
//    }
//    else if ([currencyString isEqualToString:@"AED"]) {
//        _orderTotalValueLabel.text = [NSString stringWithFormat:@"%@ AED",_subTotalString];
//    }
//    else if ([currencyString isEqualToString:@"USD"]) {
//        _orderTotalValueLabel.text = [NSString stringWithFormat:@"%@ USD",_subTotalString];
//    }
    
    
    
    
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    
    self.offerView.layer.borderColor = [UIColor colorWithRed:180.0/255.0 green:155.0/255.0 blue:108.0/255.0 alpha:1.0].CGColor;
    

    [[sharedclass sharedInstance]shadowEffects:_addNewAddressBtn_Outlet];
    [[sharedclass sharedInstance]shadowEffects:_proceedToPayBtn_Outlet];

    

    
    [_card2.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_card2.layer setShadowOffset:CGSizeMake(0, 0)];
    [_card2.layer setShadowRadius:3.0];
    [_card2.layer setShadowOpacity:0.3];
    _card2.layer.masksToBounds = NO;
    
    [_card3.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_card3.layer setShadowOffset:CGSizeMake(0, 0)];
    [_card3.layer setShadowRadius:3.0];
    [_card3.layer setShadowOpacity:0.3];
    _card3.layer.masksToBounds = NO;
    [self getShippingAddressServicecall];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getShippingAddressServicecall];
   // self.navigationController.navigationBar.hidden = NO;
}

-(void)viewWillDisappear:(BOOL)animated
{
        //self.navigationController.navigationBar.hidden = YES;
    
}

#pragma mark GetShippingAddressServicecall
-(void)getShippingAddressServicecall
{
    //http://volivesolutions.com/Klue/services/get_shipping_addr
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //API-KEY:985869547,lang:en,user_id
    
    NSMutableDictionary * getAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [getAddressPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [getAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [getAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"get_shipping_addr" withPostDict:getAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Get Address Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    addressDataArrayCount = [NSMutableArray new];
                    finalAddressArray = [NSMutableArray new];
                    shippingAddressIdArray = [NSMutableArray new];
                    userNameArray = [NSMutableArray new];
                    
                    nameArray = [NSMutableArray new];
                    emailIdArray = [NSMutableArray new];
                    mobileNumberArray = [NSMutableArray new];
                    addressArray = [NSMutableArray new];
                    landMarkArray = [NSMutableArray new];
                    cityNameArray = [NSMutableArray new];
                    ischeckArr = [NSMutableArray new];
                    
                    addressDataArrayCount = [dict objectForKey:@"data"];
                    
                    if (addressDataArrayCount.count > 0) {
                        
                        for (int i =0; addressDataArrayCount.count > i; i++) {
                            
                            [shippingAddressIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"shipping_id"]]];
                            [userNameArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"username"]];
                            
                            [nameArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"username"]]];
                            [emailIdArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"email"]]];
                            [mobileNumberArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"mobile"]]];
                            [addressArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"address"]]];
                            //[houseNoArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"]]];
                            [cityNameArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"]]];
                            [landMarkArray addObject:[NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"landmark"]]];
    
                            addressStr = [NSString stringWithFormat:@"%@,%@,%@,%@,%@,%@,%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"username"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"email"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"mobile"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"address"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"city"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"landmark"],[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"country"]];
                            
                            [finalAddressArray addObject:addressStr];
                            
                            [ischeckArr addObject:@"NO"];
                            
                        }
                        
                        NSLog(@"check array is %@",ischeckArr);
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_orderSummeryTableView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
    
}

#pragma mark DeleteAddressServicecall
-(void)deleteAddressServicecall:(id)sender
{
    //http://volivesolutions.com/Klue/services/del_shipping_addr
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_orderSummeryTableView];
    NSIndexPath *indexPath = [_orderSummeryTableView indexPathForRowAtPoint:position];
    //API-KEY:985869547,lang:en,shipping_id,user_id
    NSMutableDictionary * deleteAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [deleteAddressPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [deleteAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [deleteAddressPostDictionary setObject:[NSString stringWithFormat:@"%@",[shippingAddressIdArray objectAtIndex:indexPath.row]] forKey:@"shipping_id"];
    [deleteAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"del_shipping_addr" withPostDict:deleteAddressPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Get Address Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    [self getShippingAddressServicecall];
                    
                }
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
    
    
}
#pragma mark TableviewDelegates
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return finalAddressArray.count;
    //return 3;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_orderSummeryTableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.orderView];
    [cell.radioBtn_Outlet addTarget:self action:@selector(selectAddress:) forControlEvents:UIControlEventTouchUpInside];
    [cell.editAddressBtn_Outlet addTarget:self action:@selector(editAddress:) forControlEvents:UIControlEventTouchUpInside];
    cell.userNameLabel.text =[NSString stringWithFormat:@"%@",[userNameArray objectAtIndex:indexPath.row]];
    cell.addressLabel.text = [NSString stringWithFormat:@"%@",[finalAddressArray objectAtIndex:indexPath.row]];
    [cell.deleteAddressBtn_Outlet addTarget:self action:@selector(deleteAddressServicecall:) forControlEvents:UIControlEventTouchUpInside];
    
    NSString * ischeck = [ischeckArr objectAtIndex:indexPath.row];

    if ([ischeck isEqualToString:@"NO"]) {

        [cell.radioBtn_Outlet setImage:[UIImage imageNamed:@"radio_uncheck"] forState:UIControlStateNormal];
        

    }else
    {
        [cell.radioBtn_Outlet setImage:[UIImage imageNamed:@"radio_check"] forState:UIControlStateNormal];
       
       [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"NO"];
    }

    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    NSString * ischeck = [ischeckArr objectAtIndex:indexPath.row];
    
    if ([ischeck isEqualToString:@"NO"]) {
        
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"YES"];
       
    }else
    {
        
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"NO"];
        
    }
        shippingIdString = [NSString stringWithFormat:@"%@",[shippingAddressIdArray objectAtIndex:indexPath.row]];
    cityNameString = [NSString stringWithFormat:@"%@",[cityNameArray objectAtIndex:indexPath.row]];
    
     dispatch_async(dispatch_get_main_queue(), ^{
     [_orderSummeryTableView reloadData];
     });


}
-(void)selectAddress:(id)sender
{
    
//    ischecked = false;
//
    CGPoint position = [sender convertPoint:CGPointZero toView:_orderSummeryTableView];
    NSIndexPath *indexPath = [_orderSummeryTableView indexPathForRowAtPoint:position];
    //TableViewCell *cell = (TableViewCell*)[_orderSummeryTableView cellForRowAtIndexPath:indexPath];
    NSString * ischeck = [ischeckArr objectAtIndex:indexPath.row];

    if ([ischeck isEqualToString:@"NO"]) {
        
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"YES"];
        
    }else
    {
        
        [ischeckArr replaceObjectAtIndex:indexPath.row withObject:@"NO"];
        
    }
    shippingIdString = [NSString stringWithFormat:@"%@",[shippingAddressIdArray objectAtIndex:indexPath.row]];
    cityNameString = [NSString stringWithFormat:@"%@",[cityNameArray objectAtIndex:indexPath.row]];
    
    usernameString = [NSString stringWithFormat:@"%@",[userNameArray objectAtIndex:indexPath.row]];
    [[NSUserDefaults standardUserDefaults]setObject:usernameString forKey:@"userName"];
    emailIdString = [NSString stringWithFormat:@"%@",[emailIdArray objectAtIndex:indexPath.row]];
    [[NSUserDefaults standardUserDefaults]setObject:emailIdString forKey:@"emailId"];
    mobileNoString = [NSString stringWithFormat:@"%@",[mobileNumberArray objectAtIndex:indexPath.row]];
    [[NSUserDefaults standardUserDefaults]setObject:mobileNoString forKey:@"mobileNo"];
    [_orderSummeryTableView reloadData];
}

-(void)editAddress:(id)sender
{
    CGPoint position = [sender convertPoint:CGPointZero toView:_orderSummeryTableView];
    NSIndexPath *indexPath = [_orderSummeryTableView indexPathForRowAtPoint:position];
    
    AddAddressVC *addAddress = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAddressVC"];
    addAddress.shippingAddressId = [shippingAddressIdArray objectAtIndex:indexPath.row];
    addAddress.nameString = [nameArray objectAtIndex:indexPath.row];
    addAddress.emailIdString = [emailIdArray objectAtIndex:indexPath.row];
    addAddress.mobileNumberString = [mobileNumberArray objectAtIndex:indexPath.row];
    addAddress.addressString = [addressArray objectAtIndex:indexPath.row];
    // addAddress.houseNoString = [shippingAddressIdArray objectAtIndex:indexPath.row];
    addAddress.landMarkString = [landMarkArray objectAtIndex:indexPath.row];
    
    [self.navigationController pushViewController:addAddress animated:TRUE];
    
}
//-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 172;
//}


- (IBAction)btnBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)addNewAddressBTN_Action:(id)sender {
    
    AddAddressVC *address = [self.storyboard instantiateViewControllerWithIdentifier:@"AddAddressVC"];
    [self.navigationController pushViewController:address animated:TRUE];
}

- (IBAction)applyBTN_Action:(id)sender {
}

- (IBAction)proceedToPayBTN_Action:(id)sender {
    
    if (shippingIdString.length > 0) {
        
        ShippingMethodsViewController *methods = [self.storyboard instantiateViewControllerWithIdentifier:@"ShippingMethodsViewController"];
        methods.shippingString = [NSString stringWithFormat:@"%@",shippingIdString];
        methods.total = [NSString stringWithFormat:@"%@",_subTotalString];
        methods.imageData = _image;
        methods.messageTextString = _messageString;
        methods.notetextString = _noteString;
        methods.cityNameString = cityNameString;
        [self.navigationController pushViewController:methods animated:TRUE];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please select an address for shipping"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
   
    
   
}

@end
