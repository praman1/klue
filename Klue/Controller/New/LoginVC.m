//
//  LoginVC.m
//  Klue
//
//  Created by vishal bhatt on 15/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "LoginVC.h"
#import "PrefixHeader.pch"

@interface LoginVC ()
{
    NSString *languageString;
    NSString *tokenString;
    NSString *emailIdString;
     UIAlertController *alertController;
}
- (IBAction)btnBack:(id)sender;

@end

@implementation LoginVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    _emailTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
    _passwordTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"password"];
    [self customDesign];
//    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(dismiss)];
//    [self.view addGestureRecognizer:tap];
    
    UISwipeGestureRecognizer *swipeRecognizer = [[UISwipeGestureRecognizer alloc]
                                                 initWithTarget:self action:@selector(swipeDown:)];
    swipeRecognizer.direction = UISwipeGestureRecognizerDirectionDown;
    [self.view addGestureRecognizer:swipeRecognizer];
    
}
- (void)swipeDown:(UIGestureRecognizer *)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
//-(void)dismiss{
//
//    [self dismissViewControllerAnimated:TRUE completion:nil];
//}

-(void)customDesign{
    
    [[sharedclass sharedInstance]shadowEffects:_signInBtn_Outlet];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Login"];
//    if([languageString isEqualToString:@"ar"]){
//        
//        _emailTF.textAlignment = NSTextAlignmentRight;
//        _passwordTF.textAlignment = NSTextAlignmentRight;
//        
//    } else  {
//        
//        _emailTF.textAlignment = NSTextAlignmentLeft;
//        _passwordTF.textAlignment = NSTextAlignmentLeft;
//    }
    _helloThereLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Hello there!"];
    _welcomeBackLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Welcome Back"];
    _emailTF.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Email/Mobile No"];
    _passwordTF.placeholder = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Password"];
    [_forgotPasswordBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Forgot your password?"] forState:UIControlStateNormal];
    [_signInBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Sign In"] forState:UIControlStateNormal];
    [_termsBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Terms"] forState:UIControlStateNormal];
    [_dontHaveAccountBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Don't have an account? Register"] forState:UIControlStateNormal];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [self.view addGestureRecognizer:tap];

}

//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//}
//-(void)viewWillDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = YES;
//}

#pragma mark TextFieldDelegates
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _emailTF) {
 
        [_loginScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _passwordTF) {
   
        [_loginScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-140) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    
    [_loginScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _emailTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_passwordTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    return returnValue;
}

//http://volivesolutions.com/Klue/Api/auth
//email,password

#pragma mark loginServiceCall
-(void)loginServiceCall
{
//http://volivesolutions.com/Klue/services/login

   // API-KEY,email,password,lang,device_type,device_token

    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary * loginPostDictionary = [[NSMutableDictionary alloc]init];
    
    [loginPostDictionary setObject:_emailTF.text forKey:@"email"];
    [loginPostDictionary setObject:_passwordTF.text forKey:@"password"];
   // [loginPostDictionary setObject:tokenString ? tokenString :@"" forKey:@"token"];
    [loginPostDictionary setObject:@"123" forKey:@"device_token"];
    [loginPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [loginPostDictionary setObject:Device_Type forKey:@"device_type"];
    [loginPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"login" withPostDict:loginPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:_emailTF.text forKey:@"emailId"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                    [[NSUserDefaults standardUserDefaults]setObject:_passwordTF.text forKey:@"password"];
                    [[NSUserDefaults standardUserDefaults]synchronize];
                   
                   // NSString *basePath = [NSString stringWithFormat:@"%@",[dict objectForKey:@"base_path"]];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"user_id"] forKey:@"userId"];
   
                    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"image"] forKey:@"profileImage"];
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"fname"] forKey:@"userName"];
                    
                    NSLog(@"User Name Is %@",[[NSUserDefaults standardUserDefaults]objectForKey:@"userName"]);
                    
                    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"email"] forKey:@"emailId"];
                    [[NSUserDefaults standardUserDefaults]setObject:[[dict objectForKey:@"data"]objectForKey:@"mobile"] forKey:@"mobileNo"];
                    //[[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];

//                            SWRevealViewController *reveal = [self.storyboard instantiateViewControllerWithIdentifier:@"Side"];
//
//                   [self.navigationController presentViewController:reveal animated:TRUE completion:nil];
                    
                    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
                                                                             bundle: nil];
                    SWRevealViewController *reveal = (SWRevealViewController*)[mainStoryboard instantiateViewControllerWithIdentifier: @"Side"];
                    [self.navigationController presentViewController:reveal animated:TRUE completion:nil];
                  //  self.window.rootViewController = home;
                        
                    }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark Forgot Password Servicecall
-(void)forgotPasswordServicecall
{
    //http://volivesolutions.com/Klue/services/forgot_password
    
    // API-KEY:985869547,lang:en,email
    
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];

    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    NSMutableDictionary * forgotPasswordPostDictionary = [[NSMutableDictionary alloc]init];
  
    [forgotPasswordPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [forgotPasswordPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [forgotPasswordPostDictionary setObject:[NSString stringWithFormat:@"%@",emailIdString ? emailIdString :@""] forKey:@"email"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"forgot_password" withPostDict:forgotPasswordPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];

                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

- (IBAction)btnBack:(id)sender {
    
 [self dismissViewControllerAnimated:YES completion:nil];
    
}

- (IBAction)forgotPassword_BTN:(id)sender {
    
    alertController = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter Email Id!"]
                                                          message:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter your registered email id"]
                                                   preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =[[sharedclass sharedInstance]languageSelectedStringForKey:@"Enter Email Id"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
    
    [alertController addAction:[UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSArray * textfields = alertController.textFields;
        
        UITextField * namefield = textfields[0];
        
        //_emailID.text = emailString;
        NSLog(@"jfhgjdjgd %@",namefield);
        
        emailIdString=namefield.text;
        [self forgotPasswordServicecall];
        
}]];
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}

- (IBAction)signIn_BTN:(id)sender {
    if(_emailTF.text.length != 0 && _passwordTF.text.length !=0) {
        
        if ([[sharedclass sharedInstance]isNetworkAvalible] == YES) {
            
            [self loginServiceCall];
        }
        else{
            NoInternetVCViewController *noInternet = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVCViewController"];
            [self presentViewController:noInternet animated:TRUE completion:nil];
            
            // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please check your internet connection"] onViewController:self completion:nil];
        }
        
    }else
    {
        [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Please enter login details"] onViewController:self completion:nil];
    }
    
    
}

//- (void)presentModalViewController:(UIViewController *)modalViewController animated:(BOOL)animated
//{
//    NoInternetVCViewController *noInternet = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVCViewController"];
//}
- (IBAction)termsBTN:(id)sender {
    
}


- (IBAction)regusterBtn_Action:(id)sender {
    
    RegisterVC *registerVC =[self.storyboard instantiateViewControllerWithIdentifier:@"RegisterVC"];
    registerVC.checkString = @"fromLogin";
    [self.navigationController pushViewController:registerVC animated:TRUE];
}

@end
