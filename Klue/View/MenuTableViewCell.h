//
//  MenuTableViewCell.h
//  Klue
//
//  Created by vishal bhatt on 19/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblmenu;

@end
