//
//  customCollectionView.h
//  Adventure
//
//  Created by Suman Volive on 3/20/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol indexpathGetting <NSObject>

- (void)parentIndexPath:(NSIndexPath*)parentIndexpath;

@end

@interface customCollectionView : UICollectionView
@property (nonatomic, strong) NSIndexPath* parentIndexpath;
@property (nonatomic, weak) id <indexpathGetting> delegate1;
@end
