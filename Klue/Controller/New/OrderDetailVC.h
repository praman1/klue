//
//  OrderDetailVC.h
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailVC : UIViewController
@property NSString *productIdString;
@property NSString *orderIdString;
@property NSString *placedDateString;
@property NSString *estimatedDateString;
@property NSString *currencyType,*statusString;

@end
