//
//  SubCategoryTableVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "SubCategoryTableVC.h"
#import "HomeTableVC.h"
@interface SubCategoryTableVC ()
@property (strong, nonatomic) IBOutlet UIView *NAV1;
@property (strong, nonatomic) IBOutlet UIView *NAV2;
@property (strong, nonatomic) IBOutlet UIView *NAV3;
@property (strong, nonatomic) IBOutlet UIView *NAV4;
- (IBAction)btnBack:(id)sender;

@end

@implementation SubCategoryTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    
    [_NAV1.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_NAV1.layer setShadowOffset:CGSizeMake(0, 0)];
    [_NAV1.layer setShadowRadius:3.0];
    [_NAV1.layer setShadowOpacity:0.3];
    _NAV1.layer.masksToBounds = NO;
    
    
    [_NAV2.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_NAV2.layer setShadowOffset:CGSizeMake(0, 0)];
    [_NAV2.layer setShadowRadius:3.0];
    [_NAV2.layer setShadowOpacity:0.3];
    _NAV2.layer.masksToBounds = NO;
    
    [_NAV3.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_NAV3.layer setShadowOffset:CGSizeMake(0, 0)];
    [_NAV3.layer setShadowRadius:3.0];
    [_NAV3.layer setShadowOpacity:0.3];
    _NAV3.layer.masksToBounds = NO;
    
    [_NAV4.layer setShadowColor:[[UIColor blackColor] CGColor]];
    [_NAV4.layer setShadowOffset:CGSizeMake(0, 0)];
    [_NAV4.layer setShadowRadius:3.0];
    [_NAV4.layer setShadowOpacity:0.3];
    _NAV4.layer.masksToBounds = NO;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Incomplete implementation, return the number of sections
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete implementation, return the number of rows
    return 1;
}

/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:<#@"reuseIdentifier"#> forIndexPath:indexPath];
    
    // Configure the cell...
    
    return cell;
}
*/

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)btnBack:(id)sender {
    if ([_comForm isEqualToString:@"Menu"]) {
        Home *home = [self.storyboard instantiateViewControllerWithIdentifier:@"Home"];
        [self showViewController:home sender:self];
    }else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
