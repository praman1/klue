//
//  CityListVC.m
//  Klue
//
//  Created by volive solutions on 10/04/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "CityListVC.h"
#import "PrefixHeader.pch"


@interface CityListVC ()<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *cityListArray;
    TableViewCell *cityCell;
    NSString *cityString;
    NSString *languageString;
    NSMutableArray *cityListArrayCount;
}

@property (weak, nonatomic) IBOutlet UITableView *cityListTableView;
- (IBAction)tapGesture:(id)sender;
- (IBAction)closeBtn_Action:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *closeBtn_Outlet;

@end

@implementation CityListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_closeBtn_Outlet.layer.cornerRadius = _closeBtn_Outlet.layer.frame.size.width/2;
    _closeBtn_Outlet.layer.cornerRadius = 5;
    [[sharedclass sharedInstance]shadowEffects:_cityListTableView];
    [[sharedclass sharedInstance]shadowEffects:_closeBtn_Outlet];
  
    [self cityListServicecall];
}

-(void)dismiss{
    
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

#pragma mark CityListServicecall
-(void)cityListServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    //    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    //    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    //    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/cities_list
    //API-KEY:985869547l,ang:en
    
    NSMutableDictionary * cityListPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cityListPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [cityListPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"cities_list" withPostDict:cityListPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"CityList Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
  
                if ([status isEqualToString:@"1"])
                {
                    cityListArrayCount = [NSMutableArray new];
                    cityListArray = [NSMutableArray new];
                    
                    cityListArrayCount = [dict objectForKey:@"data"];
                    
                    if (cityListArrayCount.count > 0) {
                        for (int i = 0; cityListArrayCount.count > i; i++) {
                            
                            [cityListArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]valueForKey:@"city_name"]];
                            //[cityListArray addObject:[[cityListArrayCount objectAtIndex:i]objectForKey:@"city_name"]];
                        }
                    }
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_cityListTableView reloadData];
                            });
                    // [SVProgressHUD dismiss];
                   // [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
            });
        }
        else{
            // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return cityListArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    cityCell = [_cityListTableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cityCell == nil)
    {
        
        cityCell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    cityCell.textLabel.textColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    cityCell.textLabel.textAlignment =NSTextAlignmentCenter;
    cityCell.textLabel.text = [cityListArray objectAtIndex:indexPath.row];
    
    return cityCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    cityString = [cityListArray objectAtIndex:indexPath.row];
    [[NSUserDefaults standardUserDefaults]setObject:cityString forKey:@"cityName"];
   
    [[NSNotificationCenter defaultCenter]postNotificationName:@"data" object:nil];
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)tapGesture:(id)sender {
    [self dismissViewControllerAnimated:TRUE completion:nil];
}

- (IBAction)closeBtn_Action:(id)sender {
    [self dismissViewControllerAnimated:FALSE completion:nil];
}

@end
