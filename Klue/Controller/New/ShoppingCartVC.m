//
//  ShoppingCartVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "ShoppingCartVC.h"
#import "PrefixHeader.pch"

@interface ShoppingCartVC ()<UITableViewDelegate,UITableViewDataSource>
{
    TableViewCell *cell;
    int value;
    NSString *languageString;
    NSString *subTotalString;
    NSString *quantityStr;
    NSString *currencyString;
    
    NSMutableArray *productPriceArray;
    NSMutableArray *dataCountArray;
    NSMutableArray *productIdArray;
    NSMutableArray *productCountArray;
    NSMutableArray *brandNameArray;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    
    NSMutableArray *productRegular_price_sarArray;
    NSMutableArray *productPrice_sarArray;
    
    NSMutableArray *productRegular_price_usdArray;
    NSMutableArray *productPrice_usdArray;
    
    NSMutableArray *productRegular_price_kwdArray;
    NSMutableArray *productPrice_kwdArray;
    
    NSMutableArray *productRegular_price_aedArray;
    NSMutableArray *productPrice_aedArray;
    
    UIRefreshControl *refreshControl;
}

@property (weak, nonatomic) IBOutlet UITableView *shippingCartTableView;
@property (weak, nonatomic) IBOutlet UILabel *subtotalInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalValueLabel;
@property (weak, nonatomic) IBOutlet UIButton *continueBtn_Outlet;
- (IBAction)continueBTN:(id)sender;
- (IBAction)backBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIView *continueView;

@end

@implementation ShoppingCartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    refreshControl = [[UIRefreshControl alloc]init];
    //refreshControl.backgroundColor = [UIColor blackColor];
    refreshControl.tintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    [_shippingCartTableView addSubview:refreshControl];
    [refreshControl addTarget:self action:@selector(refreshTable) forControlEvents:UIControlEventValueChanged];
    
    
    // Do any additional setup after loading the view.
     currencyString = [[NSUserDefaults standardUserDefaults]objectForKey:@"currency"];
    [[sharedclass sharedInstance]shadowEffects:_continueView];[self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    _subtotalInfoLabel.text = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Sub Total"];
    self.title = [[sharedclass sharedInstance]languageSelectedStringForKey:@"Shopping Cart"];
    self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    [[sharedclass sharedInstance]shadowEffects:_continueBtn_Outlet];
    [_continueBtn_Outlet setTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Continue"] forState:UIControlStateNormal];
    [self getCartServicecall];
 
}

-(void)refreshTable
{
    dispatch_async(dispatch_get_main_queue(), ^{
        [refreshControl endRefreshing];
    });
    
    [self getCartServicecall];
}
//-(void)viewWillAppear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = NO;
//}
//
//-(void)viewWillDisappear:(BOOL)animated
//{
//    self.navigationController.navigationBar.hidden = YES;
//}
#pragma mark GetCartServicecall
-(void)getCartServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    //http://volivesolutions.com/Klue/services/getcart
    //API-KEY:985869547,lang,user_id,currency_type

    NSMutableDictionary * getCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [getCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [getCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [getCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [getCartPostDictionary setObject:[NSString stringWithFormat:@"%@",currencyString ? currencyString :@""] forKey:@"currency_type"];

    [[sharedclass sharedInstance]urlPerameterforPost:@"getcart" withPostDict:getCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Cart Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    [SVProgressHUD dismiss];
                    
                    dataCountArray = [NSMutableArray new];
                    productCountArray = [NSMutableArray new];
                    brandNameArray = [NSMutableArray new];
                    productNameArray = [NSMutableArray new];
                    productImageArray = [NSMutableArray new];
                    productPriceArray = [NSMutableArray new];
                    
                    productRegular_price_sarArray = [NSMutableArray new];
                    productPrice_sarArray = [NSMutableArray new];
                    
                    productRegular_price_usdArray = [NSMutableArray new];
                    productPrice_usdArray = [NSMutableArray new];
                    
                    productRegular_price_kwdArray = [NSMutableArray new];
                    productPrice_kwdArray = [NSMutableArray new];
                    
                    productRegular_price_aedArray = [NSMutableArray new];
                    productPrice_aedArray = [NSMutableArray new];
                    productIdArray = [NSMutableArray new];

                    dataCountArray = [dict objectForKey:@"data"];
                    
                    if (dataCountArray.count > 0) {
                        for (int i =0; dataCountArray.count > i; i++) {
                            
                            [productIdArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_id"]];
                            [productNameArray addObject:[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"pname"]];
                            [productImageArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"prod_image"]];
                            [brandNameArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"brand_name"]];
                            
                            //[productPriceArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price"]];
                            
                            
                            
                            
                            [productPrice_sarArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_sar"]];
                            
                            [productPrice_usdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_usd"]];
                            
                            [productPrice_kwdArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_kwd"]];
                        
                            [productPrice_aedArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i] objectForKey:@"price_aed"]];
                        
                            
                            [productCountArray addObject: [[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]];
                            quantityStr = [NSString stringWithFormat:@"%@",[[[dict objectForKey:@"data"]objectAtIndex:i]objectForKey:@"quantity"]];
                        }
                        
                        if ([quantityStr isEqualToString:@"0"]) {
                            
                            cell.quantityMinusBtn_Outlet.userInteractionEnabled = NO;
                        }
                        else {
                            
                            cell.quantityMinusBtn_Outlet.userInteractionEnabled = YES;
                        }
                        //subTotalString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"sub_total"]];
                        
                    }
                    subTotalString = [NSString stringWithFormat:@"%@",[dict objectForKey:@"sub_total"]];
                    if ([subTotalString length ] == 0) {
                        if ([currencyString isEqualToString:@"SAR"]) {
                            _totalValueLabel.text = @"0.0 SAR";
                        }
                        else if ([currencyString isEqualToString:@"KWD"]) {
                            _totalValueLabel.text = @"0.0 KWD";
                        }
                        else if ([currencyString isEqualToString:@"AED"]) {
                            _totalValueLabel.text = @"0.0 AED";
                        }
                        else if ([currencyString isEqualToString:@"USD"]) {
                            _totalValueLabel.text = @"0.0 USD";
                        }
                        
                    }
                    else{
                        //if ([currencyString isEqualToString:@"SAR"]) {
                            _totalValueLabel.text =[NSString stringWithFormat:@"%@",subTotalString];
                       // }
                        //else if ([currencyString isEqualToString:@"KWD"]) {
                           // _totalValueLabel.text =[NSString stringWithFormat:@"%@ KWD",subTotalString];
                       // }
                        //else if ([currencyString isEqualToString:@"AED"]) {
                            //_totalValueLabel.text =[NSString stringWithFormat:@"%@ AED",subTotalString];
                       // }
                        //else if ([currencyString isEqualToString:@"USD"]) {
                           // _totalValueLabel.text =[NSString stringWithFormat:@"%@ USD",subTotalString];
                       // }
                        
                    }
                    
                    
//                    _totalValueLabel.text = [NSString stringWithFormat:@"%@ SAR",[dict objectForKey:@"sub_total"] ? [dict objectForKey:@"sub_total"] : @""];
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [_shippingCartTableView reloadData];
                    });
                }
                
                else
                {
                    [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}




#pragma mark TableViewDelegates

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productNameArray.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    cell = [_shippingCartTableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (cell == nil) {
        
        cell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    [[sharedclass sharedInstance]shadowEffects:cell.orderView];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[productImageArray objectAtIndex:indexPath.row]]] completed:nil];
    cell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
    cell.brandLabel.text = [brandNameArray objectAtIndex:indexPath.row];
   
    if ([currencyString isEqualToString:@"SAR"]) {
       
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ SAR", [productPrice_sarArray objectAtIndex:indexPath.row]];
    }
    
    else if ([currencyString isEqualToString:@"KWD"]) {
 
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ KWD", [productPrice_kwdArray objectAtIndex:indexPath.row]];
    }
    else if ([currencyString isEqualToString:@"AED"]) {
       
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ AED", [productPrice_aedArray objectAtIndex:indexPath.row]];
    }
    else if ([currencyString isEqualToString:@"USD"]) {
       
        cell.discountPriceLabel.text = [NSString stringWithFormat:@"%@ USD", [productPrice_usdArray objectAtIndex:indexPath.row]];
    }
    
    cell.maiQuantityLabel.text = [NSString stringWithFormat:@"%@",[productCountArray objectAtIndex:indexPath.row]];
    cell.quantityLabel.text = [NSString stringWithFormat:@"x%@",[productCountArray objectAtIndex:indexPath.row]];
    [cell.deleteBtn addTarget:self action:@selector(deleteCart:) forControlEvents:UIControlEventTouchUpInside];
    [cell.quantityPlusBtn_Outlet addTarget:self action:@selector(addQuantity:) forControlEvents:UIControlEventTouchUpInside];
    [cell.quantityMinusBtn_Outlet addTarget:self action:@selector(minusQuantity:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

#pragma mark DeleteCartServicecall
-(void)deleteCart:(id)sender
{
    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    //    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    //    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
    //    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //http://volivesolutions.com/Klue/services/delete_cart

    //API-KEY:985869547,lang,user_id,prod_id
    
 
    
    NSMutableDictionary * deleteCartDictionary = [[NSMutableDictionary alloc]init];
    
    [deleteCartDictionary setObject:API_KEY forKey:@"API-KEY"];
    [deleteCartDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [deleteCartDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
    [deleteCartDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"delete_cart" withPostDict:deleteCartDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Remove Whishlist Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                    // [SVProgressHUD dismiss];
                    
                    [self getCartServicecall];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
    
}

#pragma mark QuantityIncrementServicecall
-(void)quantityIncrementServicecall:(id)sender
{
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
   // TableViewCell *cell = (TableViewCell*)[_shippingCartTableView cellForRowAtIndexPath:indexPath];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
   
   
    //API-KEY:985869547,lang:en,user_id:,prod_id:

    //http://volivesolutions.com/Klue/services/addcart
    NSMutableDictionary * addCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    [addCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
   // [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",cell.maiQuantityLabel.text] forKey:@"quantity"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"addcart" withPostDict:addCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"quantity Increment Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                   // [SVProgressHUD dismiss];
                    
                    //[[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                    [self getCartServicecall];
                }
                
                else
                {
                   // [SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
            //[SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

#pragma mark QuantityDecrementServicecall
-(void)quantityDecrementServicecall:(id)sender
{
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
    // TableViewCell *cell = (TableViewCell*)[_shippingCartTableView cellForRowAtIndexPath:indexPath];
    
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    // tokenString = [[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"];
    
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
//    [SVProgressHUD setBackgroundColor:[UIColor lightGrayColor]];
//    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    // API-KEY=985869547,lang,prod_id,user_id,quantity
    
    //http://volivesolutions.com/Klue/services/addcart_dec
    NSMutableDictionary * addCartPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addCartPostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",[productIdArray objectAtIndex:indexPath.row]] forKey:@"prod_id"];
    [addCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"userId"] forKey:@"user_id"];
     [addCartPostDictionary setObject:[NSString stringWithFormat:@"%@",cell.maiQuantityLabel.text] forKey:@"quantity"];
    
    [[sharedclass sharedInstance]urlPerameterforPost:@"addcart_dec" withPostDict:addCartPostDictionary andReturnwith:^(NSDictionary *dict, NSArray *arr, BOOL success, NSString *responseString) {
        if (dict) {
            NSLog(@"Quantity Decrement Data From Server %@", dict);
            NSString * status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
            // message= [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if ([status isEqualToString:@"1"])
                {
                   // [SVProgressHUD dismiss];
                    
                    //[[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Success"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                    [self getCartServicecall];
                }
                
                else
                {
                    //[SVProgressHUD dismiss];
                    [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[sharedclass sharedInstance] languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
                    
                }
            });
            
        }
        else{
           // [SVProgressHUD dismiss];
            dispatch_async(dispatch_get_main_queue(), ^{
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] withMessage:[[ sharedclass sharedInstance]languageSelectedStringForKey:[dict objectForKey:@"message"]] onViewController:self completion:nil];
            });
        }
    }];
}

-(void)addQuantity:(id)sender
{
    
    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];
    
    
    value = [[productCountArray objectAtIndex:indexPath.row] intValue];
    
    
    value ++;
    cell.maiQuantityLabel.text = [NSString stringWithFormat:@"%d",value];

    [self quantityIncrementServicecall:sender];

    
}

-(void)minusQuantity:(id)sender
{
    CGPoint position = [sender convertPoint:CGPointZero toView:_shippingCartTableView];
    NSIndexPath *indexPath = [_shippingCartTableView indexPathForRowAtPoint:position];

    value = [[productCountArray objectAtIndex:indexPath.row] intValue];
    
    value --;
    cell.maiQuantityLabel.text = [NSString stringWithFormat:@"%d",value];
    [self quantityDecrementServicecall:sender];
}


-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 150;
}


- (IBAction)continueBTN:(id)sender
{
    
    if (dataCountArray.count > 0) {
        
        GiftProductsListVC *gifts = [self.storyboard instantiateViewControllerWithIdentifier:@"GiftProductsListVC"];
        gifts.totalString = [NSString stringWithFormat:@"%@",subTotalString];
        [self.navigationController pushViewController:gifts animated:TRUE];
    }
    else{
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Message"] message:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Your cart is empty Please,add items to cart to continue"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *ok = [UIAlertAction actionWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:nil];
        [alert addAction:ok];
        [self presentViewController:alert animated:YES completion:nil];
    }
   

}

- (IBAction)backBTN:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
