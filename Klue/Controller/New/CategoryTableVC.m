//
//  CategoryTableVC.m
//  Klue
//
//  Created by vishal bhatt on 16/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import "CategoryTableVC.h"
#import "PrefixHeader.pch"

@interface CategoryTableVC ()<UITableViewDelegate,UITableViewDataSource>
{
    TableViewCell *catCell;
    NSString *languageString;
    
    NSMutableArray *catDataArrayCount;
    NSMutableArray *categoryIdArray;
    NSMutableArray *categoryNameArray;
    NSMutableArray *categoryImageArray;
}
@property (weak, nonatomic) IBOutlet UITableView *categoryListTableView;
@property (weak, nonatomic) IBOutlet UILabel *catNameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *catImageView;
- (IBAction)cartBtn_Action:(id)sender;
- (IBAction)moreBtn_Action:(id)sender;

@end

@implementation CategoryTableVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(languageRefresh) name:@"refreshMenu1" object:nil];
    
    [self customDesign];
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)languageRefresh
{
    [self viewDidLoad];
}

-(void)customDesign
{
    [self allCategoriesServicecall];
 
    [self.navigationController.navigationBar setTitleTextAttributes:
     @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.navigationController.navigationBar.shadowImage = [[UIImage alloc] init];
    
    _menuButton.target = self.revealViewController;
    _menuButton.action = @selector(revealToggle:);
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Categories"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Top Picks"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Whishlist"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:4] setTitle:[[sharedclass sharedInstance] languageSelectedStringForKey:@"Account"]];
}



#pragma mark AllCategoriesServicecall
-(void)allCategoriesServicecall
{
    languageString = [[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:189.0/255.0 green:154.0/255.0 blue:88.0/255.0 alpha:1.0]];
    [SVProgressHUD showWithStatus:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    [SVProgressHUD setBackgroundColor:[UIColor whiteColor]];
    [SVProgressHUD setDefaultAnimationType:SVProgressHUDAnimationTypeNative];
    [SVProgressHUD setDefaultMaskType:SVProgressHUDMaskTypeBlack];
    
    // http://volivesolutions.com/Klue/services/banner_cat_products?API-KEY=985869547&lang=en
    NSMutableDictionary * homePostDictionary = [[NSMutableDictionary alloc]init];
    
    [homePostDictionary setObject:API_KEY forKey:@"API-KEY"];
    [homePostDictionary setObject:[NSString stringWithFormat:@"%@",languageString ? languageString :@""] forKey:@"lang"];
    
    [[sharedclass sharedInstance]fetchResponseforParameter:@"banner_cat_products?" withPostDict:homePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"Data From Server%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            catDataArrayCount = [NSMutableArray new];
            categoryIdArray = [NSMutableArray new];
            categoryNameArray = [NSMutableArray new];
            categoryImageArray = [NSMutableArray new];
 
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
  
                catDataArrayCount = [[dataDictionary objectForKey:@"data"] objectForKey:@"categories"];

                if (catDataArrayCount.count > 0) {
                    for (int i = 0; catDataArrayCount.count > i; i++) {
                        [categoryIdArray addObject:[[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cat_id"]];
                        [categoryNameArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cname"]];
                        [ categoryImageArray addObject: [[[[dataDictionary objectForKey:@"data"]objectForKey:@"categories"]objectAtIndex:i] objectForKey:@"cat_image"]];
                    }
                    
                }

                dispatch_async(dispatch_get_main_queue(), ^{
 
                    [_categoryListTableView reloadData];
                });
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
                [[sharedclass sharedInstance]showAlertWithTitle:[[sharedclass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}

#pragma mark - Table view data source


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return categoryNameArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    catCell = [_categoryListTableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (catCell == nil) {
        catCell = [[TableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    catCell.catNameLabel.text = [categoryNameArray objectAtIndex:indexPath.row];
    [catCell.catImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASE_PATH,[categoryImageArray objectAtIndex:indexPath.row]]] completed:nil];
    
 
    return catCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    SubCategoryListVC *subCatList = [self.storyboard instantiateViewControllerWithIdentifier:@"SubCategoryListVC"];
    subCatList.checkStr = @"BACK";
    subCatList.catIdString = [categoryIdArray objectAtIndex:indexPath.row];
    subCatList.catNameString = [categoryNameArray objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:subCatList animated:TRUE];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100;
}

/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)cartBtn_Action:(id)sender {
    
    ShoppingCartVC *cart = [self.storyboard instantiateViewControllerWithIdentifier:@"ShoppingCartVC"];
    [self.navigationController pushViewController:cart animated:TRUE];
}

- (IBAction)moreBtn_Action:(id)sender {
}
@end
