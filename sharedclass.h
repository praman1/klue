//
//  sharedclass.h
//  TO GO
//
//  Created by Volive solutions on 11/2/17.
//  Copyright © 2017 Volive solutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SVProgressHUD.h"
#import <Reachability/Reachability.h>

@interface sharedclass : NSObject
-(void)textfieldAsLine:(UITextField *)myTextfield lineColor:(UIColor *)lineColor myView:(UIView *)view;

typedef void (^ KlueGet) ( NSData * dataFromJson );
typedef void(^KluePost) (NSDictionary*  dict, NSArray*  arr, BOOL success, NSString * responseString);
+(sharedclass *)sharedInstance;

@property (strong ,nonatomic) NSDictionary *json;

-(void)showprogressfor:(NSString *)str;
//-(void)alertforMessage:(UIViewController *)ve;
-(void)showprogressforSucsess:(NSString *)str;
-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;
-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(KlueGet)completionHandler;
-(void)urlPerameterforPost:(NSString* )paramStr withPostDict:(NSDictionary* )postDict andReturnwith:(KluePost)completionHandler;
-(NSString*)languageSelectedStringForKey:(NSString*) key;
-(void)showalrtfor:(NSString* )str inViewCtrl:(UIViewController* )vc;
-(BOOL)isNetworkAvalible;
-(void)shadowEffects:(UIView *)myView;

//-(void)alertforMessage:(UIViewController *)ve :(NSString *)imgname :(NSString *)text :(NSString *)buttonTitle;
@end
