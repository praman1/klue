//
//  BrandCell.h
//  Klue
//
//  Created by volivesolutions on 25/05/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrandCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *brandNameLbl;
@property (weak, nonatomic) IBOutlet UIImageView *brandImg;

@end
