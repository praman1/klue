//
//  LoginVC.h
//  Klue
//
//  Created by vishal bhatt on 15/02/18.
//  Copyright © 2018 com.igex. All rights reserved.
//

#import <UIKit/UIKit.h>
@import SkyFloatingLabelTextField;

@interface LoginVC : UIViewController
@property (weak, nonatomic) IBOutlet UILabel *helloThereLabel;
@property (weak, nonatomic) IBOutlet UILabel *welcomeBackLabel;
- (IBAction)forgotPassword_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *forgotPasswordBtn_Outlet;
@property (weak, nonatomic) IBOutlet UIButton *signInBtn_Outlet;
- (IBAction)signIn_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *emailTF;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *passwordTF;
@property (weak, nonatomic) IBOutlet UIScrollView *loginScrollView;
@property (weak, nonatomic) IBOutlet UIButton *termsBtn_Outlet;
- (IBAction)termsBTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *dontHaveAccountBtn_Outlet;
- (IBAction)regusterBtn_Action:(id)sender;

@end
